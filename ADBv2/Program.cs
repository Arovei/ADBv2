﻿using Discord;
using Discord.Commands;
using Discord.Interactions;
using Discord.WebSocket;
using Interactivity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using ADBv2.Services;

namespace ADBv2
{
    class Program
    {
        public static void Main(string[] args)
            => new Program().StartAsync().GetAwaiter().GetResult();

        private IConfigurationRoot _config;

        public async Task StartAsync()
        {
            /*
            var builder = new ConfigurationBuilder()    // Begin building the configuration file
                .SetBasePath(AppContext.BaseDirectory)  // Specify the location of the config
                .AddJsonFile("_configuration.json");    // Add the configuration file
            */
            new ConfigService();
            _config = ConfigService.ConfigInfo;         // Build the configuration file
            var socketConfig = new DiscordSocketConfig()
            {
                GatewayIntents = GatewayIntents.AllUnprivileged | GatewayIntents.MessageContent,
                LogLevel = LogSeverity.Verbose,
                MessageCacheSize = 1000     // Tell Discord.Net to cache 1000 messages per channel                
            };
            var _discord = new DiscordSocketClient(socketConfig);     // Add the discord client to the service provider
            

            var services = new ServiceCollection()      // Begin building the service provider
                .AddSingleton(_discord)
                .AddSingleton(new CommandService(new CommandServiceConfig     // Add the command service to the service provider
                {
                    DefaultRunMode = Discord.Commands.RunMode.Async,     // Force all commands to run async
                    LogLevel = LogSeverity.Verbose
                }))
                .AddSingleton<CommandHandler>()     // Add remaining services to the provider
                .AddSingleton<InteractivityService>()
                //.AddSingleton(new InteractivityConfig { DefaultTimeout = TimeSpan.FromSeconds(20) }) // You can optionally add a custom config
                .AddSingleton(x => new InteractionService(x.GetRequiredService<DiscordSocketClient>()))
                //.AddSingleton(new InteractiveService(_discord))
                .AddSingleton<LoggingService>()
                .AddSingleton<StartupService>()
                //.AddSingleton<AudioService>()
                .AddSingleton<TimerService>()                
                .AddSingleton<WclService>()
                .AddSingleton<Random>()             // You get better random with a single instance than by creating a new one every time you need it
                .AddSingleton(_config);

            var provider = services.BuildServiceProvider();     // Create the service provider

            provider.GetRequiredService<LoggingService>();      // Initialize the logging service, startup service, and command handler
            await provider.GetRequiredService<StartupService>().StartAsync();
            provider.GetRequiredService<CommandHandler>();
            provider.GetRequiredService<WclService>();
            provider.GetRequiredService<TimerService>();
            //WclService WclCheck = new WclService();
            TimerService.RoleTimer(_discord);
            
            await Task.Delay(-1);     // Prevent the application from closing
        }
    }
}
