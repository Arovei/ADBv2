﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Interactions;
using Interactivity;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System;
using System.Reflection;
using ADBv2.Data;
using ADBv2.Modules;

namespace ADBv2.Services
{
    class CommandHandler
    {
        private readonly InteractivityService _interactivity;
        private readonly DiscordSocketClient _discord;
        //private readonly InteractionService _commands;
        private readonly CommandService _commands;
        private readonly IConfigurationRoot _config;
        private readonly IServiceProvider _provider;
        private readonly IServiceProvider _services;

        // DiscordSocketClient, CommandService, IConfigurationRoot, and IServiceProvider are injected automatically from the IServiceProvider
        public CommandHandler(
            InteractivityService interactivity,
            DiscordSocketClient discord,
            //InteractionService commands,
            CommandService commands,
            IConfigurationRoot config,
            IServiceProvider provider)
            //IServiceProvider services)
        {
            _interactivity = interactivity;
            _discord = discord;
            _commands = commands;
            _config = config;
            _provider = provider;
            //_services = services;

            _discord.MessageReceived += OnMessageReceivedAsync;

        }
        /*
        public async Task InitializeAsync()
        {
            // add the public modules that inherit InteractionModuleBase<T> to the InteractionService
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _services);

            // process the InteractionCreated payloads to execute Interactions commands
            _discord.InteractionCreated += HandleInteraction;

            // process the command execution results 
            _commands.SlashCommandExecuted += SlashCommandExecuted;
            _commands.ContextCommandExecuted += ContextCommandExecuted;
            _commands.ComponentCommandExecuted += ComponentCommandExecuted;
        }
        private async Task HandleInteraction(SocketInteraction arg)
        {
            try
            {
                // create an execution context that matches the generic type parameter of your InteractionModuleBase<T> modules
                var ctx = new SocketInteractionContext(_discord, arg);
                await _commands.ExecuteCommandAsync(ctx, _services);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                // if a Slash Command execution fails it is most likely that the original interaction acknowledgement will persist. It is a good idea to delete the original
                // response, or at least let the user know that something went wrong during the command execution.
                if (arg.Type == InteractionType.ApplicationCommand)
                {
                    await arg.GetOriginalResponseAsync().ContinueWith(async (msg) => await msg.Result.DeleteAsync());
                }
            }
        }        
        private Task ComponentCommandExecuted(ComponentCommandInfo arg1, Discord.IInteractionContext arg2, IResult arg3)
        {
            if (!arg3.IsSuccess)
            {
                switch (arg3.Error)
                {
                    case InteractionCommandError.UnmetPrecondition:
                        // implement
                        break;
                    case InteractionCommandError.UnknownCommand:
                        // implement
                        break;
                    case InteractionCommandError.BadArgs:
                        // implement
                        break;
                    case InteractionCommandError.Exception:
                        // implement
                        break;
                    case InteractionCommandError.Unsuccessful:
                        // implement
                        break;
                    default:
                        break;
                }
            }
            return Task.CompletedTask;
        }
        private Task ContextCommandExecuted(ContextCommandInfo arg1, Discord.IInteractionContext arg2, IResult arg3)
        {
            if (!arg3.IsSuccess)
            {
                switch (arg3.Error)
                {
                    case InteractionCommandError.UnmetPrecondition:
                        // implement
                        break;
                    case InteractionCommandError.UnknownCommand:
                        // implement
                        break;
                    case InteractionCommandError.BadArgs:
                        // implement
                        break;
                    case InteractionCommandError.Exception:
                        // implement
                        break;
                    case InteractionCommandError.Unsuccessful:
                        // implement
                        break;
                    default:
                        break;
                }
            }
            return Task.CompletedTask;
        }
        private Task SlashCommandExecuted(SlashCommandInfo arg1, Discord.IInteractionContext arg2, IResult arg3)
        {
            if (!arg3.IsSuccess)
            {
                switch (arg3.Error)
                {
                    case InteractionCommandError.UnmetPrecondition:
                        // implement
                        break;
                    case InteractionCommandError.UnknownCommand:
                        // implement
                        break;
                    case InteractionCommandError.BadArgs:
                        // implement
                        break;
                    case InteractionCommandError.Exception:
                        // implement
                        break;
                    case InteractionCommandError.Unsuccessful:
                        // implement
                        break;
                    default:
                        break;
                }
            }
            return Task.CompletedTask;
        }
        */
        private async Task OnMessageReceivedAsync(SocketMessage s)
        {
            //Console.WriteLine(s.ToString());
            var msg = s as SocketUserMessage;     // Ensure the message is from a user/bot
            if (msg == null) return;
            if (msg.Author.Id == _discord.CurrentUser.Id) return;     // Ignore self when checking commands

            var context = new SocketCommandContext(_discord, msg);     // Create the command context

            int argPos = 0;     // Check if the message has a valid command prefix
            if (msg.HasStringPrefix(_config["prefix"], ref argPos) || msg.HasMentionPrefix(_discord.CurrentUser, ref argPos))
            {
                //var result = await _commands.ExecuteAsync(context, argPos, _provider);     // Execute the command
                var result = await _commands.ExecuteAsync(context, argPos, _provider);     // Execute the command

                if (!result.IsSuccess)     // If not successful, reply with the error.
                {
                    //await context.Channel.SendMessageAsync("");
                    if (result.Error != CommandError.UnknownCommand) { Console.WriteLine(result.ToString()); }
                }
            }
            else if (msg.MentionedRoles.Count > 0)
            {
                //This section for RolePing disabling timer (TimezoneModule.cs)
                Console.WriteLine($"[{DateTime.Now}] {msg.MentionedRoles.Count} role(s) mentioned in {context.Guild.Name}, checking if ping disable should apply.");
                await TimezoneModule.RolePing(context);
            }
        }
        

    }
}
