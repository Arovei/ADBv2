﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ADBv2.Services
{
    public class ConfigService
    {
        private static IConfigurationRoot _config;
        private string botOwner;
        private List<string> authUsers;
        private string googleApi;
        private string wunderApi;
        private string darkskyApi;
        private string mapquestApi;
        private string mapboxApi;
        private string oxfordApi;
        private string oxfordKey;
        private string scree;
        private string logChannel;
        private string wclApi;
        private string pirateApi;
        private string openweatherApi;
        private string rolePingDisable;

        public ConfigService()
        {
            var builder = new ConfigurationBuilder()    // Begin building the configuration file
                .SetBasePath(AppContext.BaseDirectory)  // Specify the location of the config
                .AddJsonFile("_configuration.json");    // Add the configuration file
            _config = builder.Build();                  // Build the configuration file

            botOwner = _config["owner"];
            authUsers = _config["authusers"].Split(',').ToList();
            scree = _config["scree"];
            googleApi = _config["tokens:googleapi"];
            wunderApi = _config["tokens:wundergroundapi"];
            darkskyApi = _config["tokens:darkskyapi"];
            mapquestApi = _config["tokens:mapquestapi"];
            mapboxApi = _config["tokens:mapboxapi"];
            oxfordApi = _config["tokens:oxfordid"];
            oxfordKey = _config["tokens:oxfordkey"];
            logChannel = _config["logch"];
            wclApi = _config["tokens:wclapi"];
            pirateApi = _config["tokens:pirateapi"];
            openweatherApi = _config["tokens:openweatherapi"];
            rolePingDisable = _config["rolePingDisableGID"];
        }

        public string GoogleApi { get { return googleApi; } }
        //public string WunderApi { get { return wunderApi; } }
        public string DarkskyApi { get { return darkskyApi; } }
        public string MapquestApi { get { return mapquestApi; } }
        public string MapboxApi { get { return mapboxApi; } }
        public string OxfordApi { get { return oxfordApi; } }
        public string OxfordKey { get { return oxfordKey; } }
        public string BotOwner { get { return botOwner; } }
        public string LogChannel { get { return logChannel; } }
        public List<string> AuthUsers { get { return authUsers; } }
        public static IConfigurationRoot ConfigInfo { get { return _config; } }
        public string Scree { get { return scree; } }
        public string WclApi { get { return wclApi; } }
        public string PirateApi { get { return pirateApi; } }
        public string OpenWeatherAPI { get { return openweatherApi; } }
        public string RolePingDisable { get { return rolePingDisable; } }
    }
}
