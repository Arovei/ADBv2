﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using System;
using System.Reflection;
using System.Threading.Tasks;
using ADBv2.Data;
using ADBv2.Modules;
using System.Timers;

namespace ADBv2.Services
{
    public class TimerService : ModuleBase<SocketCommandContext>
    {
        private readonly DiscordSocketClient _discord;

        public TimerService(DiscordSocketClient discord)
        {
            _discord = discord;
        }

        //const ulong serverId = 430830588652683275; // Server ID [DRQuest]
        //const ulong channelId = 430836860328083457; // Channel ID [Upcoming Quests]

        const ulong serverId = 151439658658562059; // Server ID [Aro]
        const ulong channelId = 270086559473074176; // Channel ID [testwork]

        private SocketChannel c;

        private static System.Timers.Timer aTimer;

        public void CheckTime()
        {
            c = LoadChannel();

            // Valid time - 0:00 to 23:59
            TimeSpan announceTime = TimeSpan.Parse("3:00");
            TimeSpan difference = TimeSpan.Zero;

            // If the designated time has already passed
            if (DateTime.Now.TimeOfDay < announceTime)
            {
                Console.WriteLine("ToD < AT");
                difference = announceTime - DateTime.Now.TimeOfDay;
            }
            if (DateTime.Now.TimeOfDay > announceTime)
            {
                Console.WriteLine("ToD > AT");
                difference = DateTime.Now.TimeOfDay - announceTime;
                difference += TimeSpan.FromHours(24);                
            }
            aTimer = new System.Timers.Timer(difference.TotalMilliseconds);
            aTimer.Elapsed += PostSched;

            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        public SocketChannel LoadChannel()
        {
            return _discord.GetGuild(serverId).GetChannel(channelId);
        }

        public void PostSched(Object source, System.Timers.ElapsedEventArgs e)
        {

        }

        public static void RoleTimer(DiscordSocketClient _discord)
        {
            Console.WriteLine($"[{DateTime.Now}] Timer engaged.");
            var myTimer = new Timer(5 * 60 * 1000); //calculate five minutes in milliseconds
            myTimer.Elapsed += delegate { CheckRoleTimer(_discord); };
            myTimer.Start();
        }
        private static void CheckRoleTimer(DiscordSocketClient _discord)
        {
            Console.WriteLine($"[{DateTime.Now}] Timer checking Ping status...");
            //Check if the timer is up on the roles, then call a method in timezonemodule
            TimezoneModule.CheckPingCooldown(_discord);
        }
    }
}
