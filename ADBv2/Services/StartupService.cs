﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Interactivity;
using Microsoft.Extensions.Configuration;
using System;
using System.Reflection;
using System.Threading.Tasks;
using ADBv2.Data;
using ADBv2.Modules;

namespace ADBv2.Services
{
    class StartupService
    {
        private readonly InteractivityService _interactivity;
        private readonly DiscordSocketClient _discord;
        private readonly CommandService _commands;
        private readonly IConfigurationRoot _config;
        private readonly IServiceProvider _service;

        // DiscordSocketClient, CommandService, and IConfigurationRoot are injected automatically from the IServiceProvider
        public StartupService(
            InteractivityService interactivity,
            DiscordSocketClient discord,
            CommandService commands,
            IConfigurationRoot config,
            IServiceProvider service)
        {
            _interactivity = interactivity;
            _config = config;
            _discord = discord;
            _commands = commands;
            _service = service;
        }

        public async Task StartAsync()
        {            
            string discordToken = _config["tokens:discord"];     // Get the discord token from the config file
            if (string.IsNullOrWhiteSpace(discordToken))
                throw new Exception("Please enter your bot's token into the `_configuration.json` file found in the applications root directory.");

            await _discord.LoginAsync(TokenType.Bot, discordToken);     // Login to discord
            await _discord.StartAsync();                                // Connect to the websocket

            // Delete the timer file
            ReportFile.TimerWipe();
            // Set Playing message
            await _discord.SetGameAsync("!help");

            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _service);     // Load commands and modules into the command service
        }
    }
}
