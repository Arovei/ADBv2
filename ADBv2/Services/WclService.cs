﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Collections.Generic;
using ADBv2.Data;
using ADBv2.Modules;


namespace ADBv2.Services
{
    public class WclService : ModuleBase<SocketCommandContext>
    {
        private readonly DiscordSocketClient _discord;

        public WclService(DiscordSocketClient discord)
        {
            _discord = discord;
            System.Threading.Thread.Sleep(10000);
            // Re-enable load(); to start the service again
            //load();
        }
        /*
        public void wait(int milliseconds)
        {
            System.Timers.Timer timer1 = new System.Timers.Timer();
            if (milliseconds == 0 || milliseconds < 0) return;
            //Console.WriteLine("start wait timer");
            timer1.Interval = milliseconds;
            timer1.Enabled = true;
            timer1.Start();
            timer1.Elapsed += (s, e) =>
            {
                timer1.Enabled = false;
                timer1.Stop();
                //Console.WriteLine("stop wait timer");
            };
        }*/

        //const ulong serverId = 430830588652683275; // Server ID [DRQuest]
        //const ulong channelId = 430836860328083457; // Channel ID [Upcoming Quests]

        const ulong serverId = 619047930724220948; // Server ID [Friends]
        const ulong channelId = 690092268933218344; // Channel ID [raid-logs]

        private SocketChannel c;

        private static System.Timers.Timer aTimer;

        public void CheckTime()
        {

        }

        public SocketChannel LoadChannel()
        {
            return _discord.GetGuild(serverId).GetChannel(channelId);
        }

        //private Channel channel;
        private System.Threading.Timer timer;

        public void load()
        {
            timer = new System.Threading.Timer(send, null, 0, (1000 * 60 * 60) / 4); // 15 minute interval
            //timer = new System.Threading.Timer(send, null, 0, (1000 * 60)); // 1 minute interval
            Console.WriteLine("Timer initalized");
        }

        void send(object state)
        {
            var channel = LoadChannel() as SocketTextChannel;
            //channel.SendMessage("your message");
            WclPost x = new WclPost();
            var temp = x.postReports(channel);
        }
    }

    public class WclPost
    {
        public async Task postReports(SocketTextChannel reportChannel)
        {
            WclReport x = new WclReport();
            var reportList = x.wcl();
            List<string> reportId = new List<string>();
            List<string> title = new List<string>();
            List<string> owner = new List<string>();
            List<long> startTime = new List<long>();
            List<long> endTime = new List<long>();
            List<long> zone = new List<long>();

            var raidIDs = ReportFile.RaidReportIds("get", null);
            reportList.Result.Reverse();

            foreach (var item in reportList.Result)
            {
                if (!raidIDs.Contains(item.Id))
                {
                    reportId.Add(item.Id);
                    var result = ReportFile.RaidReportIds("add", item.Id);
                    title.Add(item.Title);
                    owner.Add(item.Owner);
                    startTime.Add(item.Start);
                    endTime.Add(item.End);
                    zone.Add(item.Zone);
                }                
            }

            for (int i = 0; i < reportList.Result.Count; i++)
            {
                string zoneName = "";
                switch (zone[i])
                {
                    case 1000:
                        zoneName = "Molten Core";
                        break;
                    case 1001:
                        zoneName = "Onyxia";
                        break;
                    case 1002:
                        zoneName = "Blackwing Lair";
                        break;
                    case 1003:
                        zoneName = "Zul'Gurub";
                        break;
                    case 1004:
                        zoneName = "Ruins of Ahn'Qiraj";
                        break;
                    case 1005:
                        zoneName = "Temple of Ahn'Qiraj";
                        break;
                    case 1006:
                        zoneName = "Naxxramas";
                        break;
                    default:
                        zoneName = "Unspecified";
                        break;
                }
                var convertedStartTime = DateTimeOffset.FromUnixTimeMilliseconds(startTime[i]);
                var convertedEndTime = DateTimeOffset.FromUnixTimeMilliseconds(endTime[i]);
                var builder = new EmbedBuilder()
                .WithTitle(title[i])
                .WithDescription(zoneName + " log uploaded by " + owner[i])
                .WithUrl("https://classic.warcraftlogs.com/reports/" + reportId[i])
                .WithColor(new Color(0x9013FE))
                //.WithThumbnailUrl("https://i.imgur.com/lZU0dam.jpg")
                .WithAuthor(author => {
                    author
                        .WithName("WarcraftLogs: Friends Guild [Anathema US]")
                        .WithUrl("https://classic.warcraftlogs.com/guild/reports-list/505695?page=1")
                        .WithIconUrl("https://dmszsuqyoe6y6.cloudfront.net/img/warcraft/favicon.png");
                })
            .AddField("Start Time [EST]", convertedStartTime.LocalDateTime, inline: true)
            .AddField("End Time [EST]", convertedEndTime.LocalDateTime, inline: true);
                var embed = builder.Build();
                await reportChannel.SendMessageAsync(
                    "",
                    embed: embed)
                    .ConfigureAwait(false);
                System.Threading.Thread.Sleep(5000);
            }            
        }
    }
}