﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using ADBv2.Services;

namespace ADBv2.Data
{
    /// <summary>
    /// CS file currently not in use. See ConfigServices.cs
    /// </summary>
    public class Credentials
    {
        private IConfigurationRoot config;
        private string botOwner;
        private List<string> authUsers;

        public Credentials()
        {
            var builder = new ConfigurationBuilder()    // Begin building the configuration file
                    .SetBasePath(AppContext.BaseDirectory)  // Specify the location of the config
                    .AddJsonFile("_configuration.json");    // Add the configuration file
            config = builder.Build();                  // Build the configuration file

            botOwner = config["ownerID"];
            authUsers = config["authusers"].Split(',').ToList();
        }

        public string BotOwner { get { return botOwner; } }
        public List<string> AuthUsers { get { return authUsers; } }
    }
}
