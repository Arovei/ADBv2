﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADBv2.Services;

namespace ADBv2.Data
{
    class WeatherResult
    {
        private const string BaseUrl = "https://api.openweathermap.org/data/2.5/onecall";

        private HttpClient GetHttpClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(BaseUrl);
            return client;
        }

        private async Task<WeatherObj> Conditions(string lat, string lon)
        {
            string CallUrl = "";
            string latlong = $"?lat={lat}&lon={lon}&appid=";
            ConfigService cred = new ConfigService();
            // If the API key is filled in, go for it. If blank, stop the search.
            if (cred.OpenWeatherAPI != "") { CallUrl = BaseUrl + latlong + cred.OpenWeatherAPI; }
            else { return null; }

            Console.WriteLine("Conditions: REQUEST HTTP ADDR: " + CallUrl);

            using (var client = GetHttpClient())
            {
                var json = await client.GetStringAsync(CallUrl);
                var data = WeatherObj.FromJson(json);
                return data;
            }
        }

        private async Task<WeatherLocObj> GetLocInfo(string queryStr)
        {
            string CallUrl = "http://autocomplete.Pirateground.com/aq?query=" + queryStr;

            Console.WriteLine("GetLocInfo: REQUEST HTTP ADDR: " + CallUrl);

            var client = new HttpClient();
            client.BaseAddress = new Uri(CallUrl);

            var json = await client.GetStringAsync(CallUrl);
            var data = WeatherLocObj.FromJson(json);
            return data;
        }

        public async Task<WeatherObj> QueryByLatLong(string lat, string lon)
        {
            //locQuery = latitude,longitude
            //var queryContent = string.Format(Uri.EscapeDataString(locQuery));
            var result = await Conditions(lat, lon);
            return result;
        }

        /*
        public async Task<WeatherObj> QueryByTerm(string queryStr)
        {
            //var queryContent = string.Format(Uri.EscapeDataString(queryStr));
            var result = await Conditions(queryStr);
            return result;
        }

        public async Task<WeatherLocObj> GetLocation(string queryStr)
        {
            var queryContent = string.Format(Uri.EscapeDataString(queryStr));
            var locResult = await GetLocInfo(queryContent);
            return locResult;
        }
        */
        
    }
}
