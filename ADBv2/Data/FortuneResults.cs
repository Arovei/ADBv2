﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Threading.Tasks;
using ADBv2.Services;

namespace ADBv2.Data
{
    public partial class FortuneObj
    {
        [JsonProperty("fortune")]
        public string fortune { get; set; }
    }

    class FortuneResults
    {
        private async Task<FortuneObj> GetFortInfo(string queryStr)
        {
            string CallUrl = "";
            string BaseUrl = "http://yerkee.com/api/fortune/";
            string FormatOpt = queryStr;            

            CallUrl = $"{BaseUrl}{FormatOpt}";

            Console.WriteLine("Fortune\nREQUEST HTTP ADDR: " + CallUrl);

            var client = new HttpClient();
            client.BaseAddress = new Uri(CallUrl);

            var json = await client.GetStringAsync(CallUrl);
            var data = FortuneObj.FromJson(json);
            return data;
        }

        public async Task<FortuneObj> GetFortune(string queryStr)
        {
            var queryContent = string.Format(Uri.EscapeDataString(queryStr));
            var fortResult = await GetFortInfo(queryContent);
            return fortResult;
        }
    }

    public partial class FortuneObj
    {
        public static FortuneObj FromJson(string json) => JsonConvert.DeserializeObject<FortuneObj>(json, ADBv2.Data.Converter.Settings);
    }
    /*
    public static class Serialize
    {
        public static string ToJson(this FortuneObj self) => JsonConvert.SerializeObject(self, QuickType.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }*/
}