﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace ADBv2.Data
{
    public class DiceMath
    {
        public static bool IsInt(string value)
        {
            int discard = 0;
            return int.TryParse(value, out discard);
        }

        public static List<int> GetDice(string dice, string checkValid = null)
        {
            // See if the requested dice rolls fit in the ruleset
            bool valid = true;
            //Should only be using this if there is a D in the input
            List<int> toRoll = new List<int>();
            int index = 0;
            //find where the d is (lolololol)
            if (dice.Contains("d")) { index = dice.IndexOf('d'); }
            if (dice.Contains("D")) { index = dice.IndexOf('D'); }
            //Return null if no D is found
            if (index == 0) { return null; }
            //take the values up to the d and after the d and put them where they belong
            toRoll.Add(int.Parse(dice.Substring(0, index)));
            toRoll.Add(int.Parse(dice.Substring(index + 1, dice.Length - index - 1)));
            // If null, follow default validity of dice sides. If not, send ruleset.
            if (checkValid == null) { valid = ValidDice(toRoll); }
            if (checkValid != null) { valid = ValidDice(toRoll, checkValid); }
            if (valid == false) { return null; }
            return RollDice(toRoll);
        }

        public static int DiceCalc(int currentNum, int total, string operation)
        {
            Dictionary<string, Func<int, int, int>> operators = new Dictionary<string, Func<int, int, int>>();
            //Add math operations
            operators.Add("+", (first, second) => first + second);
            operators.Add("-", (first, second) => first - second);
            operators.Add("/", (first, second) => first / second);
            operators.Add("*", (first, second) => first * second);

            // Return the requested math operation
            int returnNum = operators[operation](total, currentNum);
            return returnNum;
        }

        public static bool ValidDice(List<int> input, string ruleSet = null)
        {
            // input[0] = number of dice to roll
            // input[1] = dice sides

            bool validity = false;

            //Valid dice sides allowed based on dice rule set, and 200 rolls or less
            if (input[0] > 200) { return false; }

            if (ruleSet == "sr") { if (input[1] == 6) { return true; } }
            if (ruleSet == "wod") { if (input[1] == 10) { return true; } }
            if (ruleSet == null) { return true; }

            return validity;
        }

        // Shamelessly copied from MSDN crypto page
        private static RandomNumberGenerator rng = RandomNumberGenerator.Create();
        public static List<int> RollDice(List<int> input)
        {
            // input[0] = number of dice to roll
            // input[1] = dice sides

            //List holds every individual roll
            List<int> output = new List<int>();

            // Roll dice loop
            for (int i = 0; i < input[0]; i++)
            {
                byte roll = Roller((byte)input[1]);
                output.Add((int)roll);
                //output[roll - 1]++;
            }
            return output;
        }

        public static byte Roller(byte numberSides)
        {
            // Create a byte array to hold the random value.
            byte[] randomNumber = new byte[1];
            do
            {
                // Fill the array with a random value.
                rng.GetBytes(randomNumber);
            }
            while (!RollRange(randomNumber[0], numberSides));
            // Return the random number mod the number
            // of sides.  The possible values are zero-
            // based, so we add one.
            return (byte)((randomNumber[0] % numberSides) + 1);
        }

        private static bool RollRange(byte roll, byte numSides)
        {
            // There are MaxValue / numSides full sets of numbers that can come up
            // in a single byte.  For instance, if we have a 6 sided die, there are
            // 42 full sets of 1-6 that come up.  The 43rd set is incomplete.
            int fullSetsOfValues = Byte.MaxValue / numSides;

            // If the roll is within this range of fair values, then we let it continue.
            // In the 6 sided die case, a roll between 0 and 251 is allowed.  (We use
            // < rather than <= since the = portion allows through an extra 0 value).
            // 252 through 255 would provide an extra 0, 1, 2, 3 so they are not fair
            // to use.
            return roll < numSides * fullSetsOfValues;
        }
    }
}
