﻿using Newtonsoft.Json;

//
// Many thanks to http://json2csharp.com/ and https://app.quicktype.io/#r=json2csharp for making my life faster and easier.
// Lotta this is commented out because it doesnt really get used, but useful if i decide to in the future.
//

namespace ADBv2.Services
{
    public partial class WebSearchObj
    {
        [JsonProperty("kind")]
        public string Kind { get; set; }
        /*
        [JsonProperty("url")]
        public Url Url { get; set; }

        [JsonProperty("queries")]
        public Queries Queries { get; set; }
        
        [JsonProperty("context")]
        public Context Context { get; set; }
        
        [JsonProperty("searchInformation")]
        public SearchInformation SearchInformation { get; set; }
        */
        [JsonProperty("items")]
        public Item[] Items { get; set; }
    }
    /*
    public partial class Context
    {
        [JsonProperty("title")]
        public string Title { get; set; }
    }
    */
    public partial class Item
    {
        [JsonProperty("kind")]
        public string Kind { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("htmlTitle")]
        public string HtmlTitle { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("displayLink")]
        public string DisplayLink { get; set; }

        [JsonProperty("snippet")]
        public string Snippet { get; set; }

        [JsonProperty("htmlSnippet")]
        public string HtmlSnippet { get; set; }

        [JsonProperty("cacheId")]
        public string CacheId { get; set; }

        [JsonProperty("formattedUrl")]
        public string FormattedUrl { get; set; }

        [JsonProperty("htmlFormattedUrl")]
        public string HtmlFormattedUrl { get; set; }
        /*
        [JsonProperty("pagemap")]
        public Pagemap Pagemap { get; set; }*/
    }
    /*
    public partial class Pagemap
    {
        [JsonProperty("metatags")]
        public Metatag[] Metatags { get; set; }

        [JsonProperty("cse_image")]
        public CseImage[] CseImage { get; set; }

        [JsonProperty("cse_thumbnail")]
        public CseThumbnail[] CseThumbnail { get; set; }

        [JsonProperty("mobileapplication")]
        public Mobileapplication[] Mobileapplication { get; set; }

        [JsonProperty("aggregaterating")]
        public Aggregaterating[] Aggregaterating { get; set; }

        [JsonProperty("offer")]
        public Offer[] Offer { get; set; }

        [JsonProperty("organization")]
        public Organization[] Organization { get; set; }
    }

    public partial class Aggregaterating
    {
        [JsonProperty("ratingvalue")]
        public string Ratingvalue { get; set; }

        [JsonProperty("ratingcount")]
        public string Ratingcount { get; set; }
    }

    public partial class CseImage
    {
        [JsonProperty("src")]
        public string Src { get; set; }
    }

    public partial class CseThumbnail
    {
        [JsonProperty("width")]
        public string Width { get; set; }

        [JsonProperty("height")]
        public string Height { get; set; }

        [JsonProperty("src")]
        public string Src { get; set; }
    }

    public partial class Metatag
    {
        [JsonProperty("msvalidate.01")]
        public string Msvalidate01 { get; set; }

        [JsonProperty("viewport")]
        public string Viewport { get; set; }

        [JsonProperty("og:type")]
        public string OgType { get; set; }

        [JsonProperty("og:url")]
        public string OgUrl { get; set; }

        [JsonProperty("og:site_name")]
        public string OgSiteName { get; set; }

        [JsonProperty("og:keywords")]
        public string OgKeywords { get; set; }

        [JsonProperty("og:image")]
        public string OgImage { get; set; }

        [JsonProperty("og:locale")]
        public string OgLocale { get; set; }

        [JsonProperty("fb:admins")]
        public string FbAdmins { get; set; }

        [JsonProperty("og:title")]
        public string OgTitle { get; set; }

        [JsonProperty("og:description")]
        public string OgDescription { get; set; }

        [JsonProperty("msapplication-tileimage")]
        public string MsapplicationTileimage { get; set; }

        [JsonProperty("msapplication-tilecolor")]
        public string MsapplicationTilecolor { get; set; }

        [JsonProperty("swift-page-name")]
        public string SwiftPageName { get; set; }

        [JsonProperty("swift-page-section")]
        public string SwiftPageSection { get; set; }

        [JsonProperty("al:ios:url")]
        public string AlIosUrl { get; set; }

        [JsonProperty("al:ios:app_store_id")]
        public string AlIosAppStoreId { get; set; }

        [JsonProperty("al:ios:app_name")]
        public string AlIosAppName { get; set; }

        [JsonProperty("al:android:url")]
        public string AlAndroidUrl { get; set; }

        [JsonProperty("al:android:package")]
        public string AlAndroidPackage { get; set; }

        [JsonProperty("al:android:app_name")]
        public string AlAndroidAppName { get; set; }

        [JsonProperty("web-experience-app/config/environment")]
        public string WebExperienceAppConfigEnvironment { get; set; }

        [JsonProperty("og:image:secure_url")]
        public string OgImageSecureUrl { get; set; }

        [JsonProperty("og:image:type")]
        public string OgImageType { get; set; }

        [JsonProperty("og:image:width")]
        public string OgImageWidth { get; set; }

        [JsonProperty("og:image:height")]
        public string OgImageHeight { get; set; }

        [JsonProperty("fb:app_id")]
        public string FbAppId { get; set; }

        [JsonProperty("twitter:title")]
        public string TwitterTitle { get; set; }

        [JsonProperty("twitter:description")]
        public string TwitterDescription { get; set; }

        [JsonProperty("twitter:site")]
        public string TwitterSite { get; set; }

        [JsonProperty("twitter:domain")]
        public string TwitterDomain { get; set; }

        [JsonProperty("twitter:image")]
        public string TwitterImage { get; set; }

        [JsonProperty("twitter:card")]
        public string TwitterCard { get; set; }

        [JsonProperty("apple:content_id")]
        public string AppleContentId { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("referrer")]
        public string Referrer { get; set; }

        [JsonProperty("csrf-param")]
        public string CsrfParam { get; set; }

        [JsonProperty("csrf-token")]
        public string CsrfToken { get; set; }

        [JsonProperty("twitter:url")]
        public string TwitterUrl { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("theme-color")]
        public string ThemeColor { get; set; }

        [JsonProperty("twitter:image:src")]
        public string TwitterImageSrc { get; set; }

        [JsonProperty("author")]
        public string Author { get; set; }

        [JsonProperty("article:publisher")]
        public string ArticlePublisher { get; set; }

        [JsonProperty("article:author")]
        public string ArticleAuthor { get; set; }

        [JsonProperty("article:published_time")]
        public System.DateTime? ArticlePublishedTime { get; set; }

        [JsonProperty("twitter:app:name:iphone")]
        public string TwitterAppNameIphone { get; set; }

        [JsonProperty("twitter:app:id:iphone")]
        public string TwitterAppIdIphone { get; set; }

        [JsonProperty("twitter:app:url:iphone")]
        public string TwitterAppUrlIphone { get; set; }

        [JsonProperty("al:web:url")]
        public string AlWebUrl { get; set; }
    }

    public partial class Mobileapplication
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("screenshot")]
        public string Screenshot { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("datepublished")]
        public string Datepublished { get; set; }

        [JsonProperty("numdownloads")]
        public string Numdownloads { get; set; }

        [JsonProperty("softwareversion")]
        public string Softwareversion { get; set; }

        [JsonProperty("operatingsystems")]
        public string Operatingsystems { get; set; }

        [JsonProperty("contentrating")]
        public string Contentrating { get; set; }
    }

    public partial class Offer
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("price")]
        public string Price { get; set; }
    }

    public partial class Organization
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("genre")]
        public string Genre { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }
    }

    public partial class Queries
    {
        [JsonProperty("request")]
        public NextPage[] Request { get; set; }

        [JsonProperty("nextPage")]
        public NextPage[] NextPage { get; set; }
    }

    public partial class NextPage
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("totalResults")]
        public string TotalResults { get; set; }

        [JsonProperty("searchTerms")]
        public string SearchTerms { get; set; }

        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("startIndex")]
        public long StartIndex { get; set; }

        [JsonProperty("inputEncoding")]
        public string InputEncoding { get; set; }

        [JsonProperty("outputEncoding")]
        public string OutputEncoding { get; set; }

        [JsonProperty("safe")]
        public string Safe { get; set; }

        [JsonProperty("cx")]
        public string Cx { get; set; }
    }

    public partial class SearchInformation
    {
        [JsonProperty("searchTime")]
        public double SearchTime { get; set; }

        [JsonProperty("formattedSearchTime")]
        public string FormattedSearchTime { get; set; }

        [JsonProperty("totalResults")]
        public string TotalResults { get; set; }

        [JsonProperty("formattedTotalResults")]
        public string FormattedTotalResults { get; set; }
    }

    public partial class Url
    {
        [JsonProperty("type")]
        public string PurpleType { get; set; }

        [JsonProperty("template")]
        public string Template { get; set; }
    }
    */
    public partial class WebSearchObj
    {
        public static WebSearchObj FromJson(string json) => JsonConvert.DeserializeObject<WebSearchObj>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this WebSearchObj self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    public class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
