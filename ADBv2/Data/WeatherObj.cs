﻿namespace ADBv2.Data
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using J = Newtonsoft.Json.JsonPropertyAttribute;
    using R = Newtonsoft.Json.Required;
    using N = Newtonsoft.Json.NullValueHandling;

    public partial class WeatherObj
    {
        [J("lat", NullValueHandling = N.Ignore)] public double? Lat { get; set; }
        [J("lon", NullValueHandling = N.Ignore)] public double? Lon { get; set; }
        [J("timezone", NullValueHandling = N.Ignore)] public string Timezone { get; set; }
        [J("timezone_offset", NullValueHandling = N.Ignore)] public double TimezoneOffset { get; set; }
        [J("current", NullValueHandling = N.Ignore)] public Current Current { get; set; }
        [J("minutely", NullValueHandling = N.Ignore)] public List<Minutely> Minutely { get; set; }
        [J("hourly", NullValueHandling = N.Ignore)] public List<Hourly> Hourly { get; set; }
        [J("daily", NullValueHandling = N.Ignore)] public List<Daily> Daily { get; set; }
        [J("alerts", NullValueHandling = N.Ignore)] public List<Alert> Alerts { get; set; }
    }

    public partial class Alert
    {
        [J("sender_name", NullValueHandling = N.Ignore)] public string SenderName { get; set; }
        [J("event", NullValueHandling = N.Ignore)] public string Event { get; set; }
        [J("start", NullValueHandling = N.Ignore)] public long? Start { get; set; }
        [J("end", NullValueHandling = N.Ignore)] public long? End { get; set; }
        [J("description", NullValueHandling = N.Ignore)] public string Description { get; set; }
        [J("tags", NullValueHandling = N.Ignore)] public List<string> Tags { get; set; }
    }

    public partial class Current
    {
        [J("dt", NullValueHandling = N.Ignore)] public long Dt { get; set; }
        [J("sunrise", NullValueHandling = N.Ignore)] public long? Sunrise { get; set; }
        [J("sunset", NullValueHandling = N.Ignore)] public long? Sunset { get; set; }
        [J("temp", NullValueHandling = N.Ignore)] public double? Temp { get; set; }
        [J("feels_like", NullValueHandling = N.Ignore)] public double? FeelsLike { get; set; }
        [J("pressure", NullValueHandling = N.Ignore)] public long? Pressure { get; set; }
        [J("humidity", NullValueHandling = N.Ignore)] public long? Humidity { get; set; }
        [J("dew_point", NullValueHandling = N.Ignore)] public double? DewPoint { get; set; }
        [J("uvi", NullValueHandling = N.Ignore)] public long? Uvi { get; set; }
        [J("clouds", NullValueHandling = N.Ignore)] public long? Clouds { get; set; }
        [J("visibility", NullValueHandling = N.Ignore)] public long? Visibility { get; set; }
        [J("wind_speed", NullValueHandling = N.Ignore)] public double? WindSpeed { get; set; }
        [J("wind_deg", NullValueHandling = N.Ignore)] public long? WindDeg { get; set; }
        [J("wind_gust", NullValueHandling = N.Ignore)] public double? WindGust { get; set; }
        [J("weather", NullValueHandling = N.Ignore)] public List<CurrentWeather> Weather { get; set; }
        //[J("rain", NullValueHandling = N.Ignore)] public List<CurrentRain> Rain { get; set; }
        //[J("snow", NullValueHandling = N.Ignore)] public List<CurrentSnow> Snow { get; set; }
    }

    public partial class CurrentWeather
    {
        [J("id", NullValueHandling = N.Ignore)] public long? Id { get; set; }
        [J("main", NullValueHandling = N.Ignore)] public string Main { get; set; }
        [J("description", NullValueHandling = N.Ignore)] public string Description { get; set; }
        [J("icon", NullValueHandling = N.Ignore)] public string Icon { get; set; }
    }

    /*public partial class CurrentRain
    {
        [J("1h", NullValueHandling = N.Ignore)] public long? oneh { get; set; }
    }

    public partial class CurrentSnow
    {
        [J("1h", NullValueHandling = N.Ignore)] public long? oneh { get; set; }
    }*/

    public partial class Daily
    {
        [J("dt", NullValueHandling = N.Ignore)] public long Dt { get; set; }
        [J("sunrise", NullValueHandling = N.Ignore)] public long? Sunrise { get; set; }
        [J("sunset", NullValueHandling = N.Ignore)] public long? Sunset { get; set; }
        [J("moonrise", NullValueHandling = N.Ignore)] public long? Moonrise { get; set; }
        [J("moonset", NullValueHandling = N.Ignore)] public long? Moonset { get; set; }
        [J("moon_phase", NullValueHandling = N.Ignore)] public double? MoonPhase { get; set; }
        [J("temp", NullValueHandling = N.Ignore)] public Temp Temp { get; set; }
        [J("feels_like", NullValueHandling = N.Ignore)] public FeelsLike FeelsLike { get; set; }
        [J("pressure", NullValueHandling = N.Ignore)] public long? Pressure { get; set; }
        [J("humidity", NullValueHandling = N.Ignore)] public long? Humidity { get; set; }
        [J("dew_point", NullValueHandling = N.Ignore)] public double? DewPoint { get; set; }
        [J("wind_speed", NullValueHandling = N.Ignore)] public double? WindSpeed { get; set; }
        [J("wind_deg", NullValueHandling = N.Ignore)] public long? WindDeg { get; set; }
        [J("wind_gust", NullValueHandling = N.Ignore)] public double? WindGust { get; set; }
        [J("weather", NullValueHandling = N.Ignore)] public List<DailyWeather> Weather { get; set; }
        [J("clouds", NullValueHandling = N.Ignore)] public long? Clouds { get; set; }
        [J("pop", NullValueHandling = N.Ignore)] public double? Pop { get; set; }
        [J("rain", NullValueHandling = N.Ignore)] public double? Rain { get; set; }
        [J("uvi", NullValueHandling = N.Ignore)] public double? Uvi { get; set; }
        [J("snow", NullValueHandling = N.Ignore)] public double? Snow { get; set; }
    }

    public partial class FeelsLike
    {
        [J("day", NullValueHandling = N.Ignore)] public double? Day { get; set; }
        [J("night", NullValueHandling = N.Ignore)] public double? Night { get; set; }
        [J("eve", NullValueHandling = N.Ignore)] public double? Eve { get; set; }
        [J("morn", NullValueHandling = N.Ignore)] public double? Morn { get; set; }
    }

    public partial class Temp
    {
        [J("day", NullValueHandling = N.Ignore)] public double? Day { get; set; }
        [J("min", NullValueHandling = N.Ignore)] public double? Min { get; set; }
        [J("max", NullValueHandling = N.Ignore)] public double? Max { get; set; }
        [J("night", NullValueHandling = N.Ignore)] public double? Night { get; set; }
        [J("eve", NullValueHandling = N.Ignore)] public double? Eve { get; set; }
        [J("morn", NullValueHandling = N.Ignore)] public double? Morn { get; set; }
    }

    public partial class DailyWeather
    {
        [J("id", NullValueHandling = N.Ignore)] public long? Id { get; set; }
        [J("main", NullValueHandling = N.Ignore)] public Main? Main { get; set; }
        [J("description", NullValueHandling = N.Ignore)] public string Description { get; set; }
        [J("icon", NullValueHandling = N.Ignore)] public string Icon { get; set; }
    }

    public partial class Hourly
    {
        [J("dt", NullValueHandling = N.Ignore)] public long Dt { get; set; }
        [J("temp", NullValueHandling = N.Ignore)] public double? Temp { get; set; }
        [J("feels_like", NullValueHandling = N.Ignore)] public double? FeelsLike { get; set; }
        [J("pressure", NullValueHandling = N.Ignore)] public long? Pressure { get; set; }
        [J("humidity", NullValueHandling = N.Ignore)] public long? Humidity { get; set; }
        [J("dew_point", NullValueHandling = N.Ignore)] public double? DewPoint { get; set; }
        [J("uvi", NullValueHandling = N.Ignore)] public double? Uvi { get; set; }
        [J("clouds", NullValueHandling = N.Ignore)] public long? Clouds { get; set; }
        [J("visibility", NullValueHandling = N.Ignore)] public long? Visibility { get; set; }
        [J("wind_speed", NullValueHandling = N.Ignore)] public double? WindSpeed { get; set; }
        [J("wind_deg", NullValueHandling = N.Ignore)] public long? WindDeg { get; set; }
        [J("wind_gust", NullValueHandling = N.Ignore)] public double? WindGust { get; set; }
        [J("weather", NullValueHandling = N.Ignore)] public List<CurrentWeather> Weather { get; set; }
        [J("pop", NullValueHandling = N.Ignore)] public double? Pop { get; set; }
        [J("rain", NullValueHandling = N.Ignore)] public Rain Rain { get; set; }
    }

    public partial class Rain
    {
        [J("1h", NullValueHandling = N.Ignore)] public double? The1H { get; set; }
    }

    public partial class Minutely
    {
        [J("dt", NullValueHandling = N.Ignore)] public long Dt { get; set; }
        [J("precipitation", NullValueHandling = N.Ignore)] public long? Precipitation { get; set; }
    }

    public enum Icon { The01D, The02N, The03N, The04D, The04N, The10D };

    public enum Main { Clear, Clouds, Rain, Snow };
    public partial class WeatherObj
    {
        public static WeatherObj FromJson(string json) => JsonConvert.DeserializeObject<WeatherObj>(json, ADBv2.Data.Converter.Settings);
    }
    /*
    public static class Serialize
    {
        public static string ToJson(this WeatherObj self) => JsonConvert.SerializeObject(self, ADBv2.Data.Converter.Settings);
    }
    /*
    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                IconConverter.Singleton,
                MainConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
    */
    internal class IconConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Icon) || t == typeof(Icon?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "01d":
                    return Icon.The01D;
                case "02n":
                    return Icon.The02N;
                case "03n":
                    return Icon.The03N;
                case "04d":
                    return Icon.The04D;
                case "04n":
                    return Icon.The04N;
                case "10d":
                    return Icon.The10D;
            }
            throw new Exception("Cannot unmarshal type Icon");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Icon)untypedValue;
            switch (value)
            {
                case Icon.The01D:
                    serializer.Serialize(writer, "01d");
                    return;
                case Icon.The02N:
                    serializer.Serialize(writer, "02n");
                    return;
                case Icon.The03N:
                    serializer.Serialize(writer, "03n");
                    return;
                case Icon.The04D:
                    serializer.Serialize(writer, "04d");
                    return;
                case Icon.The04N:
                    serializer.Serialize(writer, "04n");
                    return;
                case Icon.The10D:
                    serializer.Serialize(writer, "10d");
                    return;
            }
            throw new Exception("Cannot marshal type Icon");
        }

        public static readonly IconConverter Singleton = new IconConverter();
    }

    internal class MainConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Main) || t == typeof(Main?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "Clear":
                    return Main.Clear;
                case "Clouds":
                    return Main.Clouds;
                case "Rain":
                    return Main.Rain;
                case "Snow":
                    return Main.Snow;
            }
            throw new Exception("Cannot unmarshal type Main");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Main)untypedValue;
            switch (value)
            {
                case Main.Clear:
                    serializer.Serialize(writer, "Clear");
                    return;
                case Main.Clouds:
                    serializer.Serialize(writer, "Clouds");
                    return;
                case Main.Rain:
                    serializer.Serialize(writer, "Rain");
                    return;
                case Main.Snow:
                    serializer.Serialize(writer, "Snow");
                    return;
            }
            throw new Exception("Cannot marshal type Main");
        }

        public static readonly MainConverter Singleton = new MainConverter();
    }
}
