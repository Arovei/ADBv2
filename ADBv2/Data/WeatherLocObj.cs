﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using ADBv2.Data;
//
//    var data = WeatherLocObj.FromJson(jsonString);

namespace ADBv2.Data
{
    using System;
    using System.Net;
    using System.Collections.Generic;

    using Newtonsoft.Json;
    using J = Newtonsoft.Json.JsonPropertyAttribute;

    public partial class WeatherLocObj
    {
        [J("RESULTS")]
        public Result[] Results { get; set; }
    }

    public partial class Result
    {
        [J("name")]
        public string Name { get; set; }
        [J("type")]
        public string Type { get; set; }
        [J("c")]
        public string C { get; set; }
        [J("zmw")]
        public string Zmw { get; set; }
        [J("tz")]
        public string Tz { get; set; }
        [J("tzs")]
        public string Tzs { get; set; }
        [J("l")]
        public string L { get; set; }
        [J("ll")]
        public string Ll { get; set; }
        [J("lat")]
        public string Lat { get; set; }
        [J("lon")]
        public string Lon { get; set; }
    }

    public partial class WeatherLocObj
    {
        public static WeatherLocObj FromJson(string json) => JsonConvert.DeserializeObject<WeatherLocObj>(json, ADBv2.Data.Converter.Settings);
    }
}
