﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADBv2.Services;

namespace ADBv2.Data
{
    class LocationResult
    {
        private async Task<GeocodeObj> GetLocInfo(string queryStr)
        {
            ConfigService cred = new ConfigService();
            string CallUrl = "";
            string BaseUrl = "https://api.mapbox.com/geocoding/v5/mapbox.places/";
            string FormatOpt = "&limit=10&type=place";

            int trash;
            if (Int32.TryParse(queryStr, out trash)) { FormatOpt = "&limit=10&type=postal"; }

            CallUrl = $"{BaseUrl}{queryStr}.json?access_token={cred.MapboxApi}{FormatOpt}";

            Console.WriteLine("Geocode: REQUEST HTTP ADDR: " + CallUrl);

            var client = new HttpClient();
            client.BaseAddress = new Uri(CallUrl);

            var json = await client.GetStringAsync(CallUrl);
            var data = GeocodeObj.FromJson(json);
            return data;
        }

        public async Task<GeocodeObj> GetLocation(string queryStr)
        {
            var queryContent = string.Format(Uri.EscapeDataString(queryStr));
            var locResult = await GetLocInfo(queryContent);
            return locResult;
        }
    }
}
