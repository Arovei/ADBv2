﻿/*namespace ADBv2.Data
{
    using System;
    using System.Collections.Generic;
    using System.Net;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using J = Newtonsoft.Json.JsonPropertyAttribute;

    public partial class DictObj
    {
        [J("metadata")]
        public Metadata Metadata { get; set; }
        [J("results")]
        public List<Result> Results { get; set; }
    }

    public partial class Metadata
    {
        [J("provider")]
        public string Provider { get; set; }
    }

    public partial class Result
    {
        [J("id")]
        public string Id { get; set; }
        [J("language")]
        public string Language { get; set; }
        [J("lexicalEntries")]
        public List<LexicalEntry> LexicalEntries { get; set; }
        
        [J("type")]
        public string Type { get; set; }
        
        [J("word")]
        public string Word { get; set; }
    }

    public partial class LexicalEntry
    {
        [J("entries")]
        public List<Entry> Entries { get; set; }
        [J("language")]
        public string Language { get; set; }
        [J("lexicalCategory")]
        public string LexicalCategory { get; set; }
        [J("pronunciations")]
        public List<Pronunciation> Pronunciations { get; set; }
        [J("text")]
        public string Text { get; set; }
    }

    public partial class Entry
    {
        [J("etymologies")]
        public List<string> Etymologies { get; set; }
        [J("grammaticalFeatures")]
        public List<GrammaticalFeature> GrammaticalFeatures { get; set; }
        [J("homographNumber")]
        public string HomographNumber { get; set; }
        [J("senses")]
        public List<Sense> Senses { get; set; }
    }

    public partial class GrammaticalFeature
    {
        [J("text")]
        public string Text { get; set; }
        [J("type")]
        public string Type { get; set; }
    }

    public partial class Sense
    {
        [J("definitions")]
        public List<string> Definitions { get; set; }
        [J("domains")]
        public List<string> Domains { get; set; }
        [J("examples")]
        public List<SenseExample> Examples { get; set; }
        [J("id")]
        public string Id { get; set; }
        [J("registers")]
        public List<string> Registers { get; set; }
        [J("subsenses")]
        public List<Subsense> Subsenses { get; set; }
        [J("regions")]
        public List<string> Regions { get; set; }
    }

    public partial class SenseExample
    {
        [J("text")]
        public string Text { get; set; }
        [J("registers")]
        public List<string> Registers { get; set; }
    }

    public partial class Subsense
    {
        [J("definitions")]
        public List<string> Definitions { get; set; }
        [J("domains")]
        public List<string> Domains { get; set; }
        [J("examples")]
        public List<SubsenseExample> Examples { get; set; }
        [J("id")]
        public string Id { get; set; }
        [J("registers")]
        public List<string> Registers { get; set; }
        [J("notes")]
        public List<GrammaticalFeature> Notes { get; set; }
    }

    public partial class SubsenseExample
    {
        [J("text")]
        public string Text { get; set; }
    }

    public partial class Pronunciation
    {
        [J("audioFile")]
        public string AudioFile { get; set; }
        [J("dialects")]
        public List<string> Dialects { get; set; }
        [J("phoneticNotation")]
        public string PhoneticNotation { get; set; }
        [J("phoneticSpelling")]
        public string PhoneticSpelling { get; set; }
    }

    public partial class DictObjSpec
    {
        public static DictObj FromJson(string json) => JsonConvert.DeserializeObject<DictObj>(json, ADBv2.Data.ConverterDict.Settings);
    }

    public static class SerializeDict
    {
        public static string ToJson(this DictObj self) => JsonConvert.SerializeObject(self, ADBv2.Data.ConverterDict.Settings);
    }

    internal class ConverterDict
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter()
                {
                    DateTimeStyles = DateTimeStyles.AssumeUniversal,
                },
            },
        };
    }
}
*/
