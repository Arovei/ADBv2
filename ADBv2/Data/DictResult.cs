﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using ADBv2.Services;

namespace ADBv2.Data
{
    class DictResult
    {
        private const string OxfordApiBaseUrl = "https://od-api.oxforddictionaries.com/api/v2";

        ConfigService cred = new ConfigService();

        private HttpClient GetHttpClient()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.BaseAddress = new Uri(OxfordApiBaseUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("app_id", cred.OxfordApi);
            client.DefaultRequestHeaders.Add("app_key", cred.OxfordKey);
            return client;
        }

        public async Task<DictWordObj> GetDefinitions(string queryStr)
        {
            string CallUrl = "";
            
            // If the API key is filled in, go for it. If blank, stop the search.
            if (cred.OxfordApi != "") { CallUrl = OxfordApiBaseUrl + "/entries/en-us/" + queryStr; }
            else { return null; }

            Console.WriteLine("Definitions: REQUEST HTTP ADDR: " + CallUrl);

            using (var client = GetHttpClient())
            {
                var json = await client.GetStringAsync(CallUrl);
                var data = DictWordObj.FromJson(json);
                return data;
            }
        }

        public async Task<DictWordObj> GetWord(string queryStr)
        {
            string CallUrl = "";

            // If the API key is filled in, go for it. If blank, stop the search.
            if (cred.OxfordApi != "") { CallUrl = OxfordApiBaseUrl + "/lemmas/en-us/" + queryStr; }
            else { return null; }

            Console.WriteLine("Find Word: REQUEST HTTP ADDR: " + CallUrl);

            using (var client2 = GetHttpClient())
            {
                try
                {
                    var json = await client2.GetStringAsync(CallUrl);
                    var data = DictWordObj.FromJson(json);
                    return data;
                }
                catch (HttpRequestException)
                {
                    DictWordObj data = null;
                    return data;
                }
            }
        }
        /*
        public async Task<DictObj> QueryByTerm(string queryStr)
        {
            //var queryContent = string.Format(Uri.EscapeDataString(queryStr));
            var result = await GetDefinitions(queryStr);
            return result;
        }
        *//*
        public async Task<WeatherLocObj> GetLocation(string queryStr)
        {
            var queryContent = string.Format(Uri.EscapeDataString(queryStr));
            var locResult = await GetLocInfo(queryContent);
            return locResult;
        }
        */
    }
}
