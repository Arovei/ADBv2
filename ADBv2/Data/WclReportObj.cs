﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ADBv2.Data
{
    public partial class WclReportObj
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("owner")]
        public string Owner { get; set; }

        [JsonProperty("start")]
        public long Start { get; set; }

        [JsonProperty("end")]
        public long End { get; set; }

        [JsonProperty("zone")]
        public long Zone { get; set; }
    }

    public partial class WclReportObj
    {
        public static List<WclReportObj> FromJson(string json) => JsonConvert.DeserializeObject<List<WclReportObj>>(json, ADBv2.Data.Converter.Settings);
    }
    /*
    public static class Serialize
    {
        public static string ToJson(this List<WclReportObj> self) => JsonConvert.SerializeObject(self, ADBv2.Data.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                OwnerConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
    */
}
