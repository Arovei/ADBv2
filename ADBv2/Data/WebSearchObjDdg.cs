﻿using Newtonsoft.Json;

namespace ADBv2.Services
{
    public partial class WebSearchObjDdg
    {/*
        [JsonProperty("ImageIsLogo")]
        public long ImageIsLogo { get; set; }
        */
        [JsonProperty("Infobox")]
        public string Infobox { get; set; }
        /*
        [JsonProperty("ImageWidth")]
        public long ImageWidth { get; set; }
        */
        [JsonProperty("Heading")]
        public string Heading { get; set; }

        [JsonProperty("Abstract")]
        public string Abstract { get; set; }

        [JsonProperty("Definition")]
        public string Definition { get; set; }

        [JsonProperty("DefinitionURL")]
        public string DefinitionUrl { get; set; }

        [JsonProperty("AbstractSource")]
        public string AbstractSource { get; set; }

        [JsonProperty("Type")]
        public string PurpleType { get; set; }

        [JsonProperty("Redirect")]
        public string Redirect { get; set; }

        [JsonProperty("AnswerType")]
        public string AnswerType { get; set; }

        [JsonProperty("RelatedTopics")]
        public RelatedTopic[] RelatedTopics { get; set; }

        [JsonProperty("AbstractURL")]
        public string AbstractUrl { get; set; }

        [JsonProperty("DefinitionSource")]
        public string DefinitionSource { get; set; }

        [JsonProperty("Entity")]
        public string Entity { get; set; }

        [JsonProperty("Results")]
        public object[] Results { get; set; }

        [JsonProperty("AbstractText")]
        public string AbstractText { get; set; }

        [JsonProperty("Answer")]
        public string Answer { get; set; }
        /*
        [JsonProperty("ImageHeight")]
        public long ImageHeight { get; set; }

        [JsonProperty("Image")]
        public string Image { get; set; }
        
        [JsonProperty("meta")]
        public Meta Meta { get; set; }*/
    }
    /*
    public partial class Meta
    {
        [JsonProperty("blockgroup")]
        public object Blockgroup { get; set; }

        [JsonProperty("attribution")]
        public object Attribution { get; set; }

        [JsonProperty("src_domain")]
        public string SrcDomain { get; set; }

        [JsonProperty("dev_date")]
        public object DevDate { get; set; }

        [JsonProperty("signal_from")]
        public string SignalFrom { get; set; }

        [JsonProperty("created_date")]
        public object CreatedDate { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("perl_module")]
        public string PerlModule { get; set; }
        
        [JsonProperty("src_options")]
        public SrcOptions SrcOptions { get; set; }
        
        [JsonProperty("src_id")]
        public long SrcId { get; set; }

        [JsonProperty("src_name")]
        public string SrcName { get; set; }

        [JsonProperty("developer")]
        public Developer[] Developer { get; set; }

        [JsonProperty("producer")]
        public object Producer { get; set; }

        [JsonProperty("is_stackexchange")]
        public object IsStackexchange { get; set; }

        [JsonProperty("production_state")]
        public string ProductionState { get; set; }

        [JsonProperty("unsafe")]
        public long Unsafe { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("example_query")]
        public string ExampleQuery { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("src_url")]
        public object SrcUrl { get; set; }

        [JsonProperty("maintainer")]
        public Maintainer Maintainer { get; set; }

        [JsonProperty("live_date")]
        public object LiveDate { get; set; }

        [JsonProperty("topic")]
        public string[] Topic { get; set; }

        [JsonProperty("tab")]
        public string Tab { get; set; }

        [JsonProperty("repo")]
        public string Repo { get; set; }

        [JsonProperty("designer")]
        public object Designer { get; set; }

        [JsonProperty("dev_milestone")]
        public string DevMilestone { get; set; }

        [JsonProperty("js_callback_name")]
        public string JsCallbackName { get; set; }
    }
    */
    public partial class Developer
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("type")]
        public string PurpleType { get; set; }
    }
    /*
    public partial class Maintainer
    {
        [JsonProperty("github")]
        public string Github { get; set; }
    }
    /*
    public partial class SrcOptions
    {
        [JsonProperty("src_info")]
        public string SrcInfo { get; set; }

        [JsonProperty("min_abstract_length")]
        public string MinAbstractLength { get; set; }

        [JsonProperty("is_wikipedia")]
        public long IsWikipedia { get; set; }

        [JsonProperty("is_fanon")]
        public long IsFanon { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("source_skip")]
        public string SourceSkip { get; set; }

        [JsonProperty("skip_icon")]
        public long SkipIcon { get; set; }

        [JsonProperty("directory")]
        public string Directory { get; set; }

        [JsonProperty("skip_image_name")]
        public long SkipImageName { get; set; }

        [JsonProperty("is_mediawiki")]
        public long IsMediawiki { get; set; }

        [JsonProperty("skip_abstract")]
        public long SkipAbstract { get; set; }

        [JsonProperty("skip_end")]
        public string SkipEnd { get; set; }

        [JsonProperty("skip_qr")]
        public string SkipQr { get; set; }

        [JsonProperty("skip_abstract_paren")]
        public long SkipAbstractParen { get; set; }
    }
    */
    public partial class RelatedTopic
    {
        [JsonProperty("Result")]
        public string Result { get; set; }

        [JsonProperty("FirstURL")]
        public string FirstUrl { get; set; }

        [JsonProperty("Text")]
        public string Text { get; set; }
        /*
        [JsonProperty("Icon")]
        public Icon Icon { get; set; }
        */
        [JsonProperty("Topics")]
        public Topic[] Topics { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }
    }
    /*
    public partial class Icon
    {
        [JsonProperty("Height")]
        public string Height { get; set; }

        [JsonProperty("Width")]
        public string Width { get; set; }

        [JsonProperty("URL")]
        public string Url { get; set; }
    }
    */
    public partial class Topic
    {/*
        [JsonProperty("Icon")]
        public Icon Icon { get; set; }
        */
        [JsonProperty("Text")]
        public string Text { get; set; }

        [JsonProperty("Result")]
        public string Result { get; set; }

        [JsonProperty("FirstURL")]
        public string FirstUrl { get; set; }
    }

    public partial class WebSearchObjDdg
    {
        public static WebSearchObjDdg FromJson(string json) => JsonConvert.DeserializeObject<WebSearchObjDdg>(json, Converter.Settings);
    }
}
