﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using ADBv2.Services;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Interactivity;

namespace ADBv2.Data
{
    class WclReport
    {
        static HttpClient client = new HttpClient();

        public async Task<List<WclReportObj>> wcl()
        {
            string WclApiBaseUrl = "https://classic.warcraftlogs.com/v1/reports/guild/";
            string guildName = "Friends";
            string ServerName = "Anathema";
            string Region = "US";
            string CallUrl = "";

            ConfigService cred = new ConfigService();
            if (cred.WclApi != "") { CallUrl = $"{WclApiBaseUrl}{guildName}/{ServerName}/{Region}?api_key={cred.WclApi}"; }
            else { return null; }
            
            var response = await client.GetStringAsync(CallUrl);
            var result = WclReportObj.FromJson(response);
            //var resp = JsonConvert.DeserializeObject<WarcraftLog>(response);

            return result;
        }

        

    }
}
