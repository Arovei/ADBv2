﻿using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using ADBv2.Modules;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Net;
//using static System.Net.WebRequestMethods;


namespace ADBv2.Data
{
    class ReportFile
    {
        public static List<string> RaidReportIds(string request, string raidId)
        {
            if (request == "add")
            {
                List<string> result = new List<string>();
                string toWrite = "";
                string fileName = "FriendsRaidLogIDs.txt";
                string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "RaidIDs", fileName);
                Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "RaidIDs"));

                if (!File.Exists(destPath))
                {
                    toWrite += raidId + "|";
                    File.WriteAllText(destPath, toWrite);
                }
                else if (File.Exists(destPath))
                {
                    toWrite = File.ReadAllText(destPath);
                    toWrite += raidId + "|";                    
                    File.WriteAllText(destPath, toWrite);
                }

                result.Add("success");
                return result;
            }
            else if (request == "get")
            {
                string fileName = "FriendsRaidLogIDs.txt";
                string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "RaidIDs", fileName);

                List<string> result = new List<string>();

                if (!File.Exists(destPath))
                {
                    result.Add("nonexistant");
                    return result;
                }

                result.Add(File.ReadAllText(destPath));
                char[] delimiters = { '|' };
                List<string> inputSplit = new List<string>(result[0].Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
                return inputSplit;
            }
            return null;
        }

        public static List<string> WeatherLocations(string request, List<string> locInfo)
        {
            // List of results to be accounted for:
            //   [0] nonexistant = file does not exist

            if (request == "add")
            {
                // Data being sent
                //  [0] = "add"
                //  [1] = location
                //  [2] = user

                List<string> result = new List<string>();
                string toWrite = "";
                string fileName = "WeatherLocations.txt";
                string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "WeatherData", fileName);
                Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "WeatherData"));

                if (!File.Exists(destPath))
                {
                    toWrite = locInfo[2] + "|" + locInfo[1] + "|";
                    File.WriteAllText(destPath, toWrite);
                }
                else if (File.Exists(destPath))
                {
                    bool replace = false;
                    var doc = File.ReadAllText(destPath);
                    char[] delimiters = { '|' };
                    List<string> inputSplit = new List<string>(doc.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));

                    for (int i = 0; i < inputSplit.Count; i++)
                    {
                        if (locInfo[2] == inputSplit[i]) { inputSplit[i+1] = locInfo[1]; replace = true; break; }
                    }
                    if (!replace)
                    {
                        inputSplit.Add(locInfo[2]);
                        inputSplit.Add(locInfo[1]);
                    }
                    foreach (var text in inputSplit) { toWrite += text + "|"; }
                    File.WriteAllText(destPath, toWrite);
                }

                result.Add("success");
                return result;
            }
            else if (request == "get")
            {
                string fileName = "WeatherLocations.txt";
                string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "WeatherData", fileName);

                List<string> result = new List<string>();

                if (!File.Exists(destPath))
                {
                    result.Add("nonexistant");
                    return result;
                }

                result.Add(File.ReadAllText(destPath));
                return result;
            }
            return null;
        }

        public static List<string> PingCooldown(string request, SocketCommandContext message)
        {
            //Console.WriteLine("Interfacing with PingCooldown...");
            List<string> result = new();
            List<SocketRole> roleNames = message.Message.MentionedRoles.ToList();
            foreach (SocketRole roleName in roleNames)
            {
                //Console.WriteLine($"Working on role {roleName.Name}...");
                if (request == "add")
                {
                    string toWrite = "";
                    string fileName = "ApplicableRoles.txt";
                    string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PingCooldown", fileName);
                    Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PingCooldown"));

                    if (!File.Exists(destPath))
                    {
                        toWrite = message.Guild.Id + "|" + roleName.Id + "|" + message.Guild.Name + "|" + roleName.Name + "|";
                        File.WriteAllText(destPath, toWrite);
                    }
                    else if (File.Exists(destPath))
                    {
                        bool replace = false;
                        var doc = File.ReadAllText(destPath);
                        char[] delimiters = { '|' };
                        List<string> inputSplit = new List<string>(doc.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));

                        for (int i = 0; i < inputSplit.Count; i++)
                        {
                            if (message.Guild.Id.ToString() == inputSplit[i] && roleName.Id.ToString() == inputSplit[i + 1])
                            {
                                Console.WriteLine($"Role {roleName.Name} for guild {message.Guild.Name} already exists.");
                                result.Add($"Failure; Role {roleName.Name} for guild {message.Guild.Name} already exists.");
                                replace = true; break;
                            }
                        }
                        if (!replace)
                        {
                            inputSplit.Add(message.Guild.Id.ToString());
                            inputSplit.Add(roleName.Id.ToString());
                            inputSplit.Add(message.Guild.Name);
                            inputSplit.Add(roleName.Name);
                            Console.WriteLine($"Adding role {roleName.Name} for guild {message.Guild.Name} to the list.");
                            result.Add($"Success; Added role {roleName.Name} for guild {message.Guild.Name} to the list.");
                        }
                        foreach (var text in inputSplit) { toWrite += text + "|"; }
                        File.WriteAllText(destPath, toWrite);
                    }
                }
                else if (request == "remove")
                {
                    string toWrite = "";
                    string fileName = "ApplicableRoles.txt";
                    string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PingCooldown", fileName);
                    Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PingCooldown"));

                    if (!File.Exists(destPath))
                    {
                        Console.WriteLine("File does not exist to remove role from.");
                        result.Add("Failed; File does not exist to remove role from.");
                    }
                    else if (File.Exists(destPath))
                    {
                        var doc = File.ReadAllText(destPath);
                        char[] delimiters = { '|' };
                        List<string> inputSplit = new List<string>(doc.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));

                        for (int i = 0; i < inputSplit.Count; i++)
                        {
                            if (message.Guild.Id.ToString() == inputSplit[i] && roleName.Id.ToString() == inputSplit[i + 1])
                            {
                                inputSplit.RemoveAt(i + 3);
                                inputSplit.RemoveAt(i + 2);
                                inputSplit.RemoveAt(i + 1);
                                inputSplit.RemoveAt(i);
                                i--;
                                Console.WriteLine($"Role {roleName.Name} for guild {message.Guild.Name} removed from list.");
                                result.Add($"Success; Role {roleName.Name} for guild {message.Guild.Name} removed from list.");
                                break;
                            }
                        }
                        foreach (var text in inputSplit) { toWrite += text + "|"; }
                        File.WriteAllText(destPath, toWrite);
                        if (result.Count == 0) { result.Add($"Failed; Role {roleName.Name} for guild {message.Guild.Name} was not in the list."); }
                    }
                }
                else
                {
                    //This part adds a new cooldown when the role is pinged
                    string toWrite = "";
                    string fileName = "PingCooldown.txt";
                    string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PingCooldown", fileName);
                    Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PingCooldown"));

                    //Console.WriteLine($"Role {roleName.Name} has been pinged. Checking current cooldown...");
                    //Technically the program should never get here if it is on cooldown, because you shouldn't be able to ping the role if its on CD.
                    //So uh, im not going to change anything here even though I should.
                    if (!File.Exists(destPath))
                    {
                        toWrite = message.Guild.Id + "|" + roleName.Id + "|" + DateTime.Now + "|";
                        File.WriteAllText(destPath, toWrite);
                    }
                    else if (File.Exists(destPath))
                    {
                        bool replace = false;
                        var doc = File.ReadAllText(destPath);
                        char[] delimiters = { '|' };
                        List<string> inputSplit = new List<string>(doc.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));

                        for (int i = 0; i < inputSplit.Count; i++)
                        {
                            if (message.Guild.Id.ToString() == inputSplit[i] && roleName.Id.ToString() == inputSplit[i + 1]) 
                            {
                                result.Add($"Success; Cooldown refreshed for {roleName.Name} for guild {message.Guild.Name}.");
                                inputSplit[i + 2] = DateTime.Now.ToString(); 
                                replace = true; 
                                break;
                            }
                        }
                        if (!replace)
                        {
                            inputSplit.Add(message.Guild.Id.ToString());
                            inputSplit.Add(roleName.Id.ToString());
                            inputSplit.Add(DateTime.Now.ToString());
                            inputSplit.Add(message.Guild.Name.ToString());
                            inputSplit.Add(roleName.Name.ToString());
                        }
                        //Console.WriteLine($"Added cooldown for role {roleName}.");
                        foreach (var text in inputSplit) { toWrite += text + "|"; }
                        File.WriteAllText(destPath, toWrite);
                        result.Add($"Success; Cooldown added to {roleName.Name} for guild {message.Guild.Name}.");
                    }
                }
            }
            //result.Add("Failed; Unknown reason.");
            return result;
        }
        public static string ValidRole(string roleID, string guildID)
        {
            {
                //Console.WriteLine("Interfacing with ValidRole...");
                string fileName = "ApplicableRoles.txt";
                string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PingCooldown", fileName);
                Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PingCooldown"));

                if (!File.Exists(destPath))
                {
                    Console.WriteLine("File does not exist to check role aganst.");
                    return "invalid";
                }
                else if (File.Exists(destPath))
                {
                    var doc = File.ReadAllText(destPath);
                    char[] delimiters = { '|' };
                    List<string> inputSplit = new List<string>(doc.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));

                    for (int i = 0; i < inputSplit.Count; i++)
                    {
                        if (guildID == inputSplit[i] && roleID == inputSplit[i + 1])
                        {   
                            return "valid";
                        }
                    }
                }
                return "error";
            }
        }

        

        public static List<string> DRSchedule(string request, List<string> DRInfo, DateTime questTime)
        {
            // List DRInfo should include, in order, for ADDING:
            //  [0] Quest type - BTB, AT, PR
            //  [1] Ticket number
            //  [2] Date
            //  [3] Time
            //  [4] Password - optional
            //
            // List DRInfo should include, for GETTING:
            //  [0] 


            if (request == "add")
            {
                //string fileName = DRInfo[2];
                DateTime dateProper = DateTime.Parse(DRInfo[2]);
                string fileName = dateProper.Date.ToShortDateString().ToString();
                fileName = fileName.Substring(0, fileName.Length - 5);
                fileName = fileName.Replace('/', '-');                
                fileName += "-DRSched.xml";
                Console.WriteLine(fileName);
                string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);

                DataTable questInfo = new DataTable { TableName = "Quests" };
                questInfo.Clear();

                // If the file for that day does not exist, make it.
                if (!File.Exists(destPath))
                {
                    questInfo.Columns.Add("Quest");
                    questInfo.Columns.Add("Ticket");
                    questInfo.Columns.Add("Date");
                    questInfo.Columns.Add("Time");
                    questInfo.Columns.Add("Password");
                }
                if (File.Exists(destPath))
                {
                    questInfo.ReadXml(destPath);
                }

                List<string> result = new List<string>();

                DataRow questAdd = questInfo.NewRow();
                questAdd["Quest"] = DRInfo[0].ToUpper();
                questAdd["Ticket"] = DRInfo[1];
                questAdd["Date"] = DRInfo[2];
                questAdd["Time"] = DRInfo[3];
                if (DRInfo.Count == 5) { questAdd["Password"] = DRInfo[4]; }
                questInfo.Rows.Add(questAdd);

                questInfo.WriteXml(destPath, XmlWriteMode.WriteSchema);

                result.Add("success");
                return result;
            }
            else if (request == "get")
            {
                string fileName = questTime.Date.ToShortDateString().ToString();
                fileName = fileName.Substring(0, fileName.Length - 5);
                fileName = fileName.Replace('/', '-');
                fileName += "-DRSched.xml";
                string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);

                if (!File.Exists(destPath))
                {
                    DRInfo.Add("nonexistant");
                    return DRInfo;
                }

                DataTable questInfo = new DataTable();
                questInfo.Clear();
                questInfo.ReadXml(destPath);

                StringBuilder output = new StringBuilder();
                
                foreach (DataRow row in questInfo.Rows)
                {
                    foreach (DataColumn col in questInfo.Columns)
                    {
                        output.AppendFormat("{0} ", row[col]);
                    }
                    DRInfo.Add(output.ToString());
                    output = output.Clear();
                }
                return DRInfo;                
            }
            return null;
        }

        public static long[] ReportStorage(long[] report, bool firstBoot)
        {
            string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "d20report.txt");
            if (firstBoot)
            {
                if (File.Exists(destPath))
                {
                    // Read the file as one string.
                    StreamReader myFile = new StreamReader(destPath);
                    string inputFile = myFile.ReadToEnd();

                    myFile.Close();

                    report = inputFile.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).Select(long.Parse).ToArray();
                }
                else
                {
                    using (StreamWriter sr = new StreamWriter(destPath))
                    {
                        foreach (var amount in report)
                        {
                            sr.Write(amount + " ");
                        }
                    }
                }
            }
            else
            {
                using (StreamWriter sr = new StreamWriter(destPath))
                {
                    foreach (long amount in report)
                    {
                        sr.Write(amount.ToString() + " ");
                    }
                }
            }
            return report;
        }

        public static List<string> ReportSR(bool request, List<string> extendSR)
        {
            // List extendSR should include, in order:
            //  [0] user name
            //  [1] keyword
            //  [2] hits
            //  [3] glitches

            List<string> reportInfo = new List<string>();
            List<string> returnInfo = new List<string>();
            string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, extendSR[0] + ".txt");
            if (File.Exists(destPath))
            {
                // Read the file
                StreamReader extendedTest = new StreamReader(destPath);
                string inputFile = extendedTest.ReadToEnd();

                extendedTest.Close();

                reportInfo = inputFile.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            // File doesn't exist and they requested a report
            if (!File.Exists(destPath) && request) { return null; }
            // File exists and requested a report
            if (File.Exists(destPath) && request)
            {
                // Keyword exists
                if (reportInfo.Contains(extendSR[1]))
                {
                    int currIndex = reportInfo.IndexOf(extendSR[1]);
                    returnInfo.Add(reportInfo[currIndex + 1]);
                    returnInfo.Add(reportInfo[currIndex + 2]);
                    return returnInfo;
                }
                // File exists, keyword doesn't
                else { return null; }
            }
            // File doesn't exist, no report. Make it.
            if (!File.Exists(destPath) && !request)
            {
                returnInfo.Add(extendSR[2]);
                returnInfo.Add(extendSR[3]);
                string fileName = extendSR[0];
                extendSR.RemoveAt(0);
                WriteSR(fileName, false, extendSR);
                return returnInfo;
            }
            // File exists, no report, add to it.
            if (File.Exists(destPath) && !request)
            {
                // keyword doesn't exist
                if (!reportInfo.Contains(extendSR[1]))
                {
                    WriteSR(extendSR[0], true, extendSR);
                    returnInfo.Add(extendSR[2]);
                    returnInfo.Add(extendSR[3]);
                    return returnInfo;
                }
                // keyword exists
                else
                {
                    // find the index of the keyword, add hits and glitches
                    int currIndex = reportInfo.IndexOf(extendSR[1]);
                    returnInfo.Add(reportInfo[currIndex + 1]);
                    returnInfo.Add(reportInfo[currIndex + 2]);

                    // convert the strings to numbers to add them
                    returnInfo[0] = (((int.Parse(returnInfo[0])) + (int.Parse(extendSR[2]))).ToString());
                    returnInfo[1] = (((int.Parse(returnInfo[1])) + (int.Parse(extendSR[3]))).ToString());

                    reportInfo[currIndex + 1] = returnInfo[0];
                    reportInfo[currIndex + 2] = returnInfo[1];
                    WriteSR(extendSR[0], false, reportInfo);
                    return returnInfo;
                }
            }
            return returnInfo;
        }

        public static void WriteSR(string fileName, bool append, List<string> toWrite)
        {
            string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName + ".txt");
            if (append)
            {
                using (StreamWriter sr = new StreamWriter(destPath, true))
                {
                    // Write "keyword hits glitches"
                    sr.Write(toWrite[1] + " " + toWrite[2] + " " + toWrite[3] + " ");
                }
            }
            else
            {
                using (StreamWriter sr = new StreamWriter(destPath))
                {
                    foreach (var item in toWrite)
                    {
                        sr.Write(item + " ");
                    }
                }
            }
        }

        public static string DeleteSR(string user, string keyword)
        {
            string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, user + ".txt");

            List<string> reportInfo = new List<string>();
            if (File.Exists(destPath))
            {
                // Read the file
                StreamReader extendedTest = new StreamReader(destPath);
                string inputFile = extendedTest.ReadToEnd();

                extendedTest.Close();

                reportInfo = inputFile.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            else { return null; }
            List<int> indexRemove = new List<int>();

            for (int i = 0; i < reportInfo.Count; i++)
            {
                if (reportInfo[i] == keyword)
                {
                    indexRemove.Add(i + 2);
                    indexRemove.Add(i + 1);
                    indexRemove.Add(i);
                }
            }
            if (indexRemove.Count == 0) { return null; }
            foreach (var item in indexRemove) { reportInfo.RemoveAt(item); }

            using (StreamWriter sr = new StreamWriter(destPath))
            {
                foreach (var item in reportInfo)
                {
                    sr.Write(item + " ");
                }
            }
            if (new FileInfo(destPath).Length == 0) { File.Delete(destPath); }
            return "success";
        }

        public static List<string> Keywords(string user)
        {
            string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, user + ".txt");

            List<string> reportInfo = new List<string>();
            if (File.Exists(destPath))
            {
                // Read the file
                StreamReader extendedTest = new StreamReader(destPath);
                string inputFile = extendedTest.ReadToEnd();

                extendedTest.Close();

                reportInfo = inputFile.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            else { return null; }

            List<string> keywordList = new List<string>();

            foreach (var item in reportInfo)
            {
                int discard;
                if (!int.TryParse(item, out discard)) { keywordList.Add(item); }
            }

            return keywordList;
        }

        public static void TimerWipe()
        {
            string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "timer.txt");
            if (File.Exists(destPath)) { File.Delete(destPath); }
        }

        public static List<string> TimerReport()
        {
            string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "timer.txt");
            if (File.Exists(destPath)) {  }
            return null;
        }
    }
}
