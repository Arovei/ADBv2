﻿using System;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ADBv2.Services;

namespace ADBv2.Data
{
    class WebSearchResult
    {
        // REMINDER: DDG requires "Results from DuckDuckGo" + logo + link to results page to use API.
        
        private const string GoogleApiBaseUrl = "https://www.googleapis.com/customsearch/v1?q=";
        private const string GoogleApiSearchEngId = "&cx=" + "011191364808987230944%3Arvtfuyzy1iw";
        
        //private const string GoogleApiKey = "&key=" + ;
        private const string DdgApiBaseUrl = "http://api.duckduckgo.com/?q=";
        private const string DdgApiFormatters = "&format=json&t=ADBv2";

        private HttpClient GetHttpClient(string engine)
        {
            var client = new HttpClient();
            if (engine == "google") { client.BaseAddress = new Uri(GoogleApiBaseUrl); }
            else if (engine == "ddg") { client.BaseAddress = new Uri(DdgApiBaseUrl); }            
            return client;
        }

        private async Task<WebSearchObj> ExecuteAsync(string queryStr, string engine = "ddg")
        {            
            string CallUrl = "";
            
            // Only needed to get the API key for google searches.
            ConfigService cred = new ConfigService();
            // If the API key is filled in, go for it. If blank, switch to DDG.
            if (cred.GoogleApi != "") { CallUrl = GoogleApiBaseUrl + queryStr + GoogleApiSearchEngId + "&key=" + cred.GoogleApi; }
            else { return null; }
            
            Console.WriteLine("ExecuteAsync: REQUEST HTTP ADDR: " + CallUrl);

            using (var client = GetHttpClient(engine))
            {
                var json = await client.GetStringAsync(CallUrl);
                var data = WebSearchObj.FromJson(json);
                return data;   
            }
        }

        private async Task<WebSearchObjDdg> ExecuteAsyncDdg(string queryStr)
        {
            // The kp=-2 disables safe search
            string CallUrl = DdgApiBaseUrl + queryStr + DdgApiFormatters + "&kp=-2";
            Console.WriteLine("ExecuteAsync: REQUEST HTTP ADDR: " + CallUrl);

            using (var client = GetHttpClient("ddg"))
            {
                var json = await client.GetStringAsync(CallUrl);
                var dataDdg = WebSearchObjDdg.FromJson(json);
                return dataDdg;
            }
        }

        public async Task<WebSearchObj> QueryByTerm(string queryStr, string engine = "google")
        {
            var queryContent = string.Format(Uri.EscapeDataString(queryStr));
            var result = await ExecuteAsync(queryContent, engine);
            return result;
        }

        public async Task<WebSearchObjDdg> QueryByTermDdg(string queryStr)
        {
            var queryContent = string.Format(Uri.EscapeDataString(queryStr));
            var resultDdg = await ExecuteAsyncDdg(queryContent);
            return resultDdg;
        }
    }
}
