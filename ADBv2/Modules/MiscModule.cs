﻿using System;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Interactivity;
using ADBv2.Data;
using ADBv2.Services;

namespace ADBv2.Modules
{
    [Name("Misc Commands")]
    [Summary("Random small/fun commands.")]
    public class MiscModule : ModuleBase<SocketCommandContext>
    {
        private readonly InteractivityService _interactivity;
        public MiscModule(InteractivityService interactivity)
        {
            _interactivity = interactivity;
        }
        //private readonly DiscordSocketClient _client;

        /*public MiscModule()
        {
            _client.UserLeft += UserLeft;
            _client.UserJoined += UserJoined;
        }*/
        [Command("math"), Alias("m")]
        [Summary("Does math!")]
        public async Task DoMath([Remainder]string input)
        {
            double result = Convert.ToDouble(new DataTable().Compute(input, null));
            Console.WriteLine(input + "=" + result.ToString());
            await ReplyAsync($"`{input} = {result}`");
        }

        [Command("tempconv"), Alias("tc")]
        [Summary("Converts temperatures")]
        public async Task TempConv([Remainder]string input)
        {
            //int temper = 0;
            string temperType;

            input = input.ToLower();
            List<string> conTemps = new List<string>();
            if (string.IsNullOrEmpty(input)) { await ReplyAsync("`Requires a numeric value and temperature type (F, C, K)`"); return; }
            if (input.Contains("f")) { temperType = "f"; }
            else if (input.Contains("c")) { temperType = "c"; }
            else if (input.Contains("k")) { temperType = "k"; }
            else { await ReplyAsync("`Requires a numeric value and temperature type (F, C, K)`"); return; }

            string result = Regex.Replace(input, "[^0-9.-]", ""); //extract number from input

            bool check = double.TryParse(result, out double temper); //convert string to number
            if (!check) { await ReplyAsync("`Failed to parse input as an integer.`"); return; }

            byte orderOne = 0, orderTwo = 0, orderThree = 0;
            switch (temperType)
            {
                case "f":
                    temper = Math.Round(temper, 2);
                    conTemps.Add(temper.ToString() + " °F");
                    temper = Math.Round(5 / (double)9 * (temper - 32), 2); //convert to Celsius
                    conTemps.Add(temper.ToString() + " °C");
                    temper += 273.15; //convert to Kelvin
                    temper = Math.Round(temper, 2);
                    conTemps.Add(temper.ToString() + " K");
                    orderOne = 0;
                    orderTwo = 1;
                    orderThree = 2;
                    break;
                case "c":
                    temper = Math.Round(temper, 2);
                    conTemps.Add(temper.ToString() + " °C");
                    temper += 273.15; //convert to Kelvin
                    temper = Math.Round(temper, 2);
                    conTemps.Add(temper.ToString() + " K");
                    temper = Math.Round((9 /(double) 5) * (temper - 273) + 32, 2); //convert to farenheit
                    conTemps.Add(temper.ToString() + " °F");
                    orderOne = 2;
                    orderTwo = 0;
                    orderThree = 1;
                    break;
                case "k":
                    temper = Math.Round(temper, 2);
                    conTemps.Add(temper.ToString() + " K");
                    temper -= 273.15; //convert to Celsius
                    temper = Math.Round(temper, 2);
                    conTemps.Add(temper.ToString() + " °C");
                    temper = Math.Round((9 /(double) 5) * (temper) + 32, 2); //convert to farenheit
                    conTemps.Add(temper.ToString() + " °F");
                    orderOne = 2;
                    orderTwo = 1;
                    orderThree = 0;
                    break;
            }

            string display = $"```Temperature Conversion:\n{conTemps[orderOne]}\n{conTemps[orderTwo]}\n{conTemps[orderThree]}```";
            await ReplyAsync(display);
        }

        [Command("scree")]
        [Summary("scree")]
        public async Task Scree()
        {
            ConfigService cred = new ConfigService();
            List<int> randomNum = new List<int>();
            if (Context.User.ToString() == cred.Scree) { randomNum = new List<int> { 1, 3 }; }
            else { randomNum = new List<int> { 1, 6 }; }
            int output = DiceMath.RollDice(randomNum)[0];

            if (Context.User.Id.ToString() == cred.Scree)
            {
                switch (output)
                {
                    case 1:
                        await ReplyAsync("*Makes cookies~*");
                        break;
                    case 2:
                        await ReplyAsync("*Tosses chicken at scree~*");
                        break;
                    case 3:
                        await ReplyAsync("*Pets Scree~*");
                        break;
                }
            }
            else
            {
                switch (output)
                {
                    case 1:
                        await ReplyAsync("*Makes cookies~*");
                        break;
                    case 2:
                        await ReplyAsync("*Tosses chicken at scree~*");
                        break;
                    case 3:
                        await ReplyAsync("*Rolls around~*");
                        break;
                    case 4:
                        await ReplyAsync("*Meows~*");
                        break;
                    case 5:
                        await ReplyAsync("*Floods the vents!*");
                        break;
                    case 6:
                        await ReplyAsync("*Tantrums!*");
                        break;
                }
            }
        }

        [Command("florida.gif"), Alias("florida")]
        [Summary("Display gif florida saw")]
        public async Task Florida()
        {
            await ReplyAsync("https://i.imgur.com/WYsk3A0.gifv");            
        }

        [Command("boop")]
        [Summary("Boops a nose~")]
        public async Task UserInfoAsync(SocketUser user = null)
        {
            var userInfo = user ?? Context.User;
            await ReplyAsync($"*Boops {userInfo.Mention}'s nose*");
        }

        [Command("purge")]
        [Summary("Deletes previous <x> messages from <y> user. Defaults to user who called command.")]
        [RequireBotPermission(GuildPermission.ManageMessages)]
        public async Task DelMessages(string delAmount, SocketGuildUser user = null)
        {
            ConfigService cred = new ConfigService();
            user = user ?? Context.Message.Author as SocketGuildUser;
            if (!user.GuildPermissions.ManageMessages)
            {
                if (user != Context.Message.Author)
                {
                    if (Context.Guild.Owner != Context.Message.Author)
                    {
                        bool matchFound = false;
                        foreach (var item in cred.AuthUsers)
                        {
                            if (item == Context.Message.Author.ToString()) { matchFound = true; break; }
                        }
                        if (!matchFound) { await ReplyAsync("`Insufficent privledges.`"); return; }
                    }
                }
            }
            if (Int32.TryParse(delAmount, out int msgRemAmount))
            {
                if (msgRemAmount == 0) { await ReplyAsync("Why did you bother calling me to do nothing?"); return; }
                if (msgRemAmount > 100) { msgRemAmount = 100; delAmount = "100"; }
            }
            else { await ReplyAsync("`Input must be numeric.`"); return; }
            var Items = await Context.Channel.GetMessagesAsync().FlattenAsync();
            if (msgRemAmount != 100) { msgRemAmount++; }
            var usermessages = Items.Where(x => x.Author == ((IGuildUser)user)).Take(msgRemAmount);
            _interactivity.DelayedSendMessageAndDeleteAsync(Context.Channel, deleteDelay: TimeSpan.FromSeconds(15), text: $"`Deleting the last {delAmount} messages from {user?.Nickname ?? user.Username}.`");
            foreach (var item in usermessages) { await Context.Channel.DeleteMessageAsync(item); }
        }

        [Command("purgeb")]
        [Summary("Deletes previous <x> messages from the bot.")]
        [RequireBotPermission(GuildPermission.ManageMessages)]
        public async Task DelBotMessages(string delAmount)
        {
            var user = (IGuildUser)Context.Message.Author;            
            ConfigService cred = new ConfigService();
            if (Context.Guild.Owner != Context.Message.Author)
            {
                if (!user.GuildPermissions.ManageMessages)
                {
                    bool matchFound = false;
                    foreach (var item in cred.AuthUsers)
                    {
                        if (item == Context.Message.Author.ToString()) { matchFound = true; break; }
                    }
                    if (!matchFound) { await ReplyAsync("`Insufficent privledges.`"); return; }
                }
            }
            var userInfo = Context.Guild.CurrentUser;
            if (Int32.TryParse(delAmount, out int msgRemAmount))
            {
                if (msgRemAmount == 0) { await ReplyAsync("Why did you bother calling me to do nothing?"); return; }
                if (msgRemAmount > 100) { msgRemAmount = 100; delAmount = "100"; }                
            }
            else { await ReplyAsync("`Input must be numeric.`"); return; }
            if (msgRemAmount != 100) { msgRemAmount++; }
            var Items = await Context.Channel.GetMessagesAsync().FlattenAsync();
            var usermessages = Items.Where(x => x.Author == Context.Guild.CurrentUser).Take(msgRemAmount);
            _interactivity.DelayedSendMessageAndDeleteAsync(Context.Channel, deleteDelay: TimeSpan.FromSeconds(15), text: $"`Deleting the last {delAmount} messages from {Context.Guild.CurrentUser.Username}.`");
            foreach (var item in usermessages) { await Context.Channel.DeleteMessageAsync(item); }
        }
        
        [Command("purgea")]
        [Summary("Deletes the previous <x> messages from the channel.")]
        [RequireBotPermission(GuildPermission.ManageMessages)]
        public async Task DelAllMessages(string delAmount)
        {
            ConfigService cred = new ConfigService();
            var user = Context.Message.Author as SocketGuildUser;
            if (!user.GuildPermissions.ManageMessages)
            {
                    if (Context.Guild.Owner != Context.Message.Author)
                    {
                        await ReplyAsync("`Requires guild manage message permissions.`");
                        return;
                        //foreach (var item in cred.AuthUsers)
                        //{
                        //    if (item != Context.Message.Author.ToString()) { await ReplyAsync("`Insufficent privledges.`"); return; }
                        //    else if (item == Context.Message.Author.ToString()) { break; }
                        //}                        
                    }
            }
            if (Int32.TryParse(delAmount, out int msgRemAmount))
            {
                if (msgRemAmount == 0) { await ReplyAsync("Why did you bother calling me to do nothing?"); return; }
                if (msgRemAmount > 100) { msgRemAmount = 100; delAmount = "100"; }
            }
            else { await ReplyAsync("`Input must be numeric.`"); return; }
            var Items = await Context.Channel.GetMessagesAsync().FlattenAsync();
            if (msgRemAmount != 100) { msgRemAmount++; }
            Items = Items.Take(msgRemAmount);
            _interactivity.DelayedSendMessageAndDeleteAsync(Context.Channel, deleteDelay: TimeSpan.FromSeconds(15), text: $"`Deleting the last {delAmount} messages from {Context.Channel.Name}.`");
            foreach (var msg in Items) { await Context.Channel.DeleteMessageAsync(msg); }
        }
        
        [Command("flop")]
        [Summary("Flops!")]
        public async Task Flop(SocketUser user = null)
        {
            var userInfo = user ?? Context.User;
            await ReplyAsync($"*Flops on {userInfo.Mention}!* Hi!~");
        }

        [Command("hug")]
        [Summary("Hugs!")]
        public async Task Hug(SocketUser user = null)
        {
            var userInfo = user ?? Context.User;
            await ReplyAsync($"*Hugs {userInfo.Mention}!* ^_^");
        }

        [Command("slap")]
        [Summary("Slaps! :o")]
        public async Task Slap(SocketUser user = null)
        {
            var userInfo = user ?? Context.User;
            if (userInfo == Context.User) { await ReplyAsync($"*Slaps {userInfo.Mention} around a bit with a large trout... they did ask for it.*"); }
            else { await ReplyAsync($"*Slaps {userInfo.Mention} around a bit with a large trout*"); }
        }

        [Command("nick")]
        [Summary("Changes the bot's nickname")]
        [RequireBotPermission(GuildPermission.ChangeNickname)]
        public async Task Nick(string newName)
        {            
            ConfigService cred = new ConfigService();
            //var guild = Context.Guild;
            var user = Context.Guild.CurrentUser;
            //var user = Context.Message.Author as SocketGuildUser;
            if (Context.Guild.Owner != Context.Message.Author)
            {
                await ReplyAsync("`Requires guild ownership.`");
                return;
            }
            Console.WriteLine($"User {Context.Message.Author} renamed me to {newName} in {Context.Guild.Name}");
            await user.ModifyAsync(x => x.Nickname = newName);
        }

        [Command("nick")]
        [Summary("Changes the bot's nickname")]
        [RequireBotPermission(GuildPermission.ChangeNickname)]
        public async Task NickOther(ulong guildId, string newName)
        {
            ConfigService cred = new ConfigService();
            //var guild = Context.Guild;
            var guild = Context.Client.GetGuild(guildId);
            var user = guild.CurrentUser;
            //var user = Context.Message.Author as SocketGuildUser;
            if (cred.BotOwner != Context.Message.Author.ToString())
            {
                await ReplyAsync("`Requires bot ownership.`");
                return;
            }
            Console.WriteLine($"User {Context.Message.Author} renamed me to {newName} in {Context.Guild.Name}");
            await user.ModifyAsync(x => x.Nickname = newName);
        }

        [Command("leaveguild")]
        [Summary("Leaves a guild using a guild ID")]
        public async Task LeaveGuild(ulong guildId)
        {
            ConfigService cred = new ConfigService();
            var guild = Context.Client.GetGuild(guildId);
            if (cred.BotOwner != Context.Message.Author.ToString())
            {
                await ReplyAsync("`Requires bot ownership.`");
                return;
            }
            Console.WriteLine($"\nUser {Context.Message.Author} called me to leave {guild.Name}.\n");
            await ReplyAsync($"`Attempting to leave guild {guild.Name}...`");
            await guild.LeaveAsync();
            await ReplyAsync("`Check console for result.`");
        }

        // Comment these methods if you don't want to use this log method, or just get the messages in Console anyway.
        // JK i could never get this to work right
        /*
        public async Task UserLeft(IGuildUser user)
        {
            var config = new ConfigService();
            ulong logCh = 0;
            try { UInt64.TryParse(config.LogChannel, out logCh); }
            catch { Console.WriteLine("Log channel not set or improperly set."); return; }
            var channel = Context.Guild.GetChannel(logCh) as SocketTextChannel;
                        
            await ((ISocketMessageChannel)_client.GetChannel(logCh)).SendMessageAsync(user.ToString() + " has left the server.");
        }

        public async Task UserJoined(IGuildUser user)
        {
            var config = new ConfigService();
            ulong logCh = 0;
            try { UInt64.TryParse(config.LogChannel, out logCh); }
            catch { Console.WriteLine("Log channel not set or improperly set."); return; }
            var channel = Context.Guild.GetChannel(logCh) as SocketTextChannel;

            await ((ISocketMessageChannel)_client.GetChannel(logCh)).SendMessageAsync(user.ToString() + " has joined the server.");
        }
        */
    }
    public static class StringExt
    {
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }
}
