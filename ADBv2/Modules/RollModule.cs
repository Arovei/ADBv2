﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Interactivity;
using ADBv2.Data;

namespace ADBv2.Modules
{
    [Name("Dice Roll")]
    [Summary("Roll specified dice.")]
    public class RollModule : ModuleBase<SocketCommandContext>
    {
        private readonly InteractivityService _interactivity;
        public RollModule(InteractivityService interactivity)
        {
            _interactivity = interactivity;
        }

        [Command("flip"), Alias("coin")]
        [Summary("Flips a coin.")]
        public async Task Flip([Remainder]string input = null)
        {
            // Use diceroller to flip a 2 sided "dice", create said dice first.
            List<int> flipSides = new List<int> { 1, 2 };
            List<int> bonusOption = new List<int> { 1, 255 };

            // Another funny gag!
            List<int> bonusAnswer = DiceMath.RollDice(bonusOption);
            if (bonusAnswer[0] > 250) { await ReplyAsync("And it's... huh. Landed on its side. Try again!"); return; }

            List<int> output = DiceMath.RollDice(flipSides);
            string headsTails;
            bool match = false;

            // Check to see if input is either heads or tails so it doesn't output the wrong symbol for an invalid call.
            if (input != null)
            {
                if (!string.Equals(input, "heads", StringComparison.OrdinalIgnoreCase))
                {
                    if (!string.Equals(input, "tails", StringComparison.OrdinalIgnoreCase))
                    {
                        // Set input to null so it ignores symbol answer, since it will always be false if it is not heads or tails.
                        input = null;
                        await ReplyAsync("`Call must be heads or tails.`");
                    }
                }
            }
            if (output[0] == 1)
            {
                headsTails = "Heads";
                if (input != null) { if (string.Equals(input, headsTails, StringComparison.OrdinalIgnoreCase)) { match = true; } }
            }
            else if (output[0] == 2)
            {
                headsTails = "Tails";
                if (input != null) { if (string.Equals(input, headsTails, StringComparison.OrdinalIgnoreCase)) { match = true; } }
            }
            else
            {
                headsTails = "Result fell out of bounds. Please report error.";
            }
            if (input != null && match) { await ReplyAsync("`Flip: " + headsTails + "` :white_check_mark:"); }
            else if (input != null && !match) { await ReplyAsync("`Flip: " + headsTails + "` :negative_squared_cross_mark:"); }
            else { await ReplyAsync("`Flip: " + headsTails + "`"); }
        }

        [Command("roll"), Alias("r")]
        [Summary("Roll specified dice.")]
        public async Task D20([Remainder]string input = null)
        {
            bool epRoller = false;
            if (input.Contains("epRoller")) { input = input.Remove(input.LastIndexOf(' ') + 1); epRoller = true; }
            char[] delimiters = { '+', '-', '*', '/', ' ' };
            // Can't roll dice without a volume, can you?
            if (input == null) { input = "1d20"; await ReplyAsync("`No roll requested. Defaulting to 1D20.`"); }
            // Roll a d20 if no d is found (lolololol)
            if (!input.Contains('d')) 
            { 
                if (!delimiters.Any(input.Contains)) { input = "+" + input; }
                input = "1d20" + input; 
            }
            // All the dice rolls and flat numbers
            List<string> inputSplit = new List<string>(input.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
            // Sort the input into an array for finding operands
            char[] charInput = input.ToCharArray();
            // Put matching delimiters in as operations
            List<string> operations = new List<string>();
            //Store the results of all the rolls to display at the end
            List<string> displayRolls = new List<string>();
            // Storage for each dice roll total
            List<int> diceTotals = new List<int>();
            // And store the final tally
            int total = 0;
            // Flag for just math
            bool justMath = false;
            input = "";

            for (int i = 0; i < inputSplit.Count;)
            {
                if (DiceMath.IsInt(inputSplit[i])) { i++; }
                else if (!inputSplit[i].ToLower().Contains("d"))
                {
                    if (!DiceMath.IsInt(inputSplit[i])) { inputSplit.RemoveAt(i); }
                    // ADD CHECK FOR D
                }
                else { i++; }
            }
            if (inputSplit.ElementAtOrDefault(0) == null) { await ReplyAsync("`Invalid input.`"); return; }

            // Temporary? Order of Operations flag
            bool ooo = false;

            // Adding all math operations to the list
            foreach (var item in charInput)
            {
                foreach (var operand in delimiters)
                {
                    if (operand == item && operand != ' ') { operations.Add(item.ToString()); }
                    // Order of Operations check
                    if (item == '/' || item == '*') { ooo = true; }
                }
            }
            // For every individual dice/solid number (IE: 2d6, 5d10, 6, etc)
            for (int i = 0; i < inputSplit.Count; i++)
            {
                // Not necessarily only dice, but allows for storage of results of dice and single add digits to diceTotals
                int currentDiceTotal = 0;
                // If it contains a d/D, it's a dice roll
                if (inputSplit[i].Contains("d") || inputSplit[i].Contains("D"))
                {
                    // Store the results of each roll from DiceMath
                    List<int> resultsRolls = new List<int>();
                    resultsRolls = DiceMath.GetDice(inputSplit[i]);

                    if (resultsRolls == null) { await ReplyAsync("Too many dice, or dice do not fit ruleset. Check !help."); }
                    displayRolls.Add("[");
                    // Cycle through the list of rolls, adding to the total of this dice
                    for (int j = 0; j < resultsRolls.Count; j++)
                    {
                        displayRolls.Add(resultsRolls[j].ToString());
                        if (j < (resultsRolls.Count - 1)) { displayRolls.Add(", "); }
                        // Add each roll to the total
                        currentDiceTotal += resultsRolls[j];
                    }
                    displayRolls.Add("]");
                    if (i != inputSplit.Count - 1)
                    {
                        //Check if an operand was used before trying to access the array.
                        if (operations.ElementAtOrDefault(i) == null) { operations.Add("+"); }
                        displayRolls.Add(" " + operations[i] + " ");
                        input += inputSplit[i] + operations[i];
                    }
                    else { input += inputSplit[i]; }
                    diceTotals.Add(currentDiceTotal);
                }
                // If it's a solid number
                else if (DiceMath.IsInt(inputSplit[i]))
                {
                    if (i == 0) { justMath = true; }
                    displayRolls.Add(inputSplit[i]);
                    if (i != inputSplit.Count - 1) { displayRolls.Add(" " + operations[i] + " "); input += inputSplit[i] + operations[i]; }
                    else { input += inputSplit[i]; }
                    diceTotals.Add(int.Parse(inputSplit[i]));
                }
            }

            // Calculate dat shit yo
            for (int i = 0; i < diceTotals.Count; i++)
            {
                // First roll is just input, can't do the math without two numbers to work with
                if (i == 0) { total = diceTotals[i]; }
                if (i != 0) { total = DiceMath.DiceCalc(diceTotals[i], total, operations[i - 1]); }
            }

            // Setup ouput
            string display = "[";
            if (justMath) { display = inputSplit[0]; }
            int temp = 0;
            foreach (var item in displayRolls)
            {
                Console.Write(item);
                // old line, dont remember why i had temp != 0
                if (temp != 0) { display = display + item; }
                temp++;
            }

            Console.WriteLine(" = " + total);
            display = display + " = " + total;

            // Check if results are too long for a discord message
            bool tooLong = false;
            if (display.Length > 2000) { tooLong = true; }

            string resultWord = "";
            resultWord = input;

            var builder = new EmbedBuilder()
                .WithDescription(resultWord)
                .WithColor(new Color(0x344294));
            var footer = new EmbedFooterBuilder();
            footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
            if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
            builder.Footer = footer;
            //.WithThumbnailUrl("https://i.imgur.com/RyEOUyA.jpg")
            if (epRoller)
            {
                builder.WithAuthor(author =>
                {
                    author
                        .WithName("Eclipse Phase");
                    //.WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
                });
            }
            else
            {
                builder.WithAuthor(author =>
                {
                    author
                        .WithName("D20");
                    //.WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
                });
            }
            if (!tooLong) { builder.AddField("Rolls", display); }
            else if (tooLong) { builder.AddField("Rolls Total", total); }
            var embed = builder.Build();
            await ReplyAsync("", false, embed)
                .ConfigureAwait(false);

            if (ooo == true) { await ReplyAsync("```WARNING ||| * or / was used. Order of Operations is NOT in effect. Math is done in the order given. ||| WARNING```"); }
        }

        [Command("shadowrun"), Alias("sr")]
        [Summary("Roll using Shadowrun rules.")]
        public async Task Shadowrun([Remainder]string input)
        {
            if (input.ToLower() == "help")
            {
                HelpModule help = new(_interactivity);
                await help.HelpSrRoll(Context.Channel);
                return;
            }
            var info = Context;

            // Currently for calculating critical glitch in SR
            int totalRolls = 0;
            int totalRollsVS = 0;

            char[] delimiters = { '+', '-', '*', '/', ' ' };
            // All the dice rolls and flat numbers
            List<string> inputSplit = new List<string>(input.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
            // Sort the input into an array for finding operands
            char[] charInput = input.ToCharArray();
            // Put matching delimiters in as operations
            List<string> operations = new List<string>();
            List<string> operationsVS = new List<string>();
            //Store the results of all the rolls to display at the end
            List<string> displayRolls = new List<string>();
            List<string> displayRollsVS = new List<string>();
            List<string> displayRollsSC = new List<string>();            
            // Storage for each dice roll total
            List<int> diceTotals = new List<int>();
            List<int> diceTotalsVS = new List<int>();
            List<int> diceTotalsSC = new List<int>();
            // And store the final tally
            int total = 0;
            int totalVS = 0;
            int totalSC = 0;
            // Limit to check against
            int inputLimit = 0;
            int inputLimitVS = 0;

            // WoD/SR flag for exploding 10s/edge
            bool xplodeTens = false;
            // Shadowrun text flags
            bool extendTest = false;
            bool delTest = false;
            bool srReport = false;
            // Shadowrun versus and versus edge flags
            bool vs = false;
            bool vsx = false;
            // Limit flag
            bool limit = false;
            bool limitVS = false;
            // Second Chance flag
            bool sc = false;
            // Push the Limit flag
            bool ptl = false;
            // Minor display fix for xpld/ptl flags
            bool displayXpld = false;
            if (inputSplit[0] == "ptl" || inputSplit[0] == "x" || inputSplit[0] == "edge") { displayXpld = true; }

            string etKeyword = null;

            // SR VS edge roll
            List<string> inputVSX = new List<string>();
            // Needed to pull reports for extended tests
            bool removeAtZero = true;
            // How many dice to roll
            int diceCount = 0;

            // If not just asking for a report or to delete a keyword
            if (DiceMath.IsInt(inputSplit[0]))
            {
                removeAtZero = false;
                inputSplit[0] = inputSplit[0] + "d6";
                diceCount++;
            }
            // Use this to ignore any keyword past the first
            int twoTimes = 0;
            // Use this to ignore any vs past the first
            int vsTimes = 0;
            // Use this to ignore any limit past the first
            int limitTimes = 0;
            for (int i = 0; i < inputSplit.Count; i++)
            {
                if (inputSplit[i].Equals("x") || inputSplit[i].Equals("edge") || inputSplit[i].Equals("ptl"))
                {
                    if (sc == true) { await ReplyAsync("You cannot use Edge in two different ways for one roll.\n*Error caused by: using [x/edge/ptl] with [sc].*"); return; }
                    if (inputSplit.ElementAtOrDefault(i + 1) != null)
                    {
                        if (DiceMath.IsInt(inputSplit[i + 1]))
                        {
                            xplodeTens = true;
                            inputSplit[i + 1] = inputSplit[i + 1] + "d6";
                            diceCount++;
                        }
                        else if (!DiceMath.IsInt(inputSplit[i + 1]))
                        {
                            xplodeTens = true;
                            inputSplit.Insert(i + 1, "1d6");
                            diceCount++;
                            await ReplyAsync("`Defaulting to 1D6 for PTL.`");
                        }
                    }
                    else
                    {
                        xplodeTens = true;
                        inputSplit.Insert(i + 1, "1d6");
                        diceCount++;
                        await ReplyAsync("`Defaulting to 1D6 for PTL.`");
                    }
                }
                if (inputSplit[i].Equals("et") || inputSplit[i].Equals("report") || inputSplit[i].Equals("del"))
                {
                    if (vs == true) { await ReplyAsync("Extended tests not allowed in VS rolls."); return; }
                    if (inputSplit[i].Equals("report")) { srReport = true; }
                    if (inputSplit[i].Equals("et")) { extendTest = true; }
                    if (inputSplit[i].Equals("del")) { delTest = true; }
                    if ((srReport || delTest) && sc) { sc = false; }
                    if (twoTimes != 1)
                    {
                        if (inputSplit.ElementAtOrDefault(i + 1) == null)
                        {
                            List<string> keywordList = ReportFile.Keywords(Context.User.ToString());
                            if (keywordList == null) { await ReplyAsync("`Requires extended test keyword.\nNo keywords found in storage.`"); return; }
                            string showList = "";
                            foreach (var item in keywordList) { showList += item + " "; }
                            await ReplyAsync("`Requires extended test keyword.\nAvailable keywords: " + showList + "`");
                            return;
                        }
                        if ((!inputSplit[i + 1].Equals("et")) || (!inputSplit[i + 1].Equals("report")) || (!inputSplit[i + 1].Equals("del")))
                        {
                            etKeyword = inputSplit[i + 1];
                            twoTimes++;
                        }
                    }
                }
                /*
                if (inputSplit[i].Equals("ptl"))
                {
                    if (sc) { await ReplyAsync("You cannot use Edge in two different ways for one roll.\n*Error caused by: using [ptl] with [sc].*"); }
                    if (delTest || srReport) { await ReplyAsync("[del]/[report] unsupported with [ptl]."); }
                    ptl = true;
                    if (inputSplit.ElementAtOrDefault(i + 1) != null)
                    {
                        if (DiceMath.IsInt(inputSplit[i + 1]))
                        {
                            xplodeTens = true;
                            inputSplit[i + 1] = inputSplit[i + 1] + "d6";
                            diceCount++;
                        }
                        else if (!DiceMath.IsInt(inputSplit[i + 1])) { await ReplyAsync("Requires Edge amount after [ptl]."); return; }
                    }
                    else { await ReplyAsync("Requires Edge amount after [ptl]."); return; }
                }
                */
                if (inputSplit[i].Equals("sc"))
                {
                    if (ptl) { await ReplyAsync("You cannot use Edge in two different ways for one roll.\n*Error caused by: using [x/edge/ptl] with [sc].*"); }
                    if (xplodeTens) { await ReplyAsync("You cannot use Edge in two different ways for one roll.\n*Error caused by: using [x/edge/ptl] with [sc].*"); return; }
                    sc = true;
                    if (srReport || delTest) { sc = false; }
                    if (vs) { await ReplyAsync("Second Chance rolls disallowed during vs rolls."); return; }
                }
                if (vsTimes == 0)
                {
                    if (inputSplit[i].Equals("vs") || inputSplit[i].Equals("v"))
                    {
                        // Do not allow extended tests, second chance in a vs roll
                        if (extendTest || delTest || srReport) { await ReplyAsync("Extended tests disallowed during vs rolls."); return; }
                        if (sc) { await ReplyAsync("[sc] rolls disallowed during vs rolls."); return; }
                        if (inputSplit.ElementAtOrDefault(i + 1) != null)
                        {
                            if (DiceMath.IsInt(inputSplit[i + 1])) { inputVSX.Add(inputSplit[i + 1] + "d6"); }
                            else { await ReplyAsync("`Requires number **directly** after vs call.`"); return; }
                            i++;
                            vs = true;
                            if (inputSplit.ElementAtOrDefault(i + 1) != null)
                            {
                                while (inputSplit.ElementAtOrDefault(i + 1) != null)
                                {
                                    if (inputSplit[i + 1].Equals("edge") || inputSplit[i + 1].Equals("x") || inputSplit[i + 1].Equals("ptl"))
                                    {
                                        if (inputSplit.ElementAtOrDefault(i + 2) != null)
                                        {
                                            if (DiceMath.IsInt(inputSplit[i + 2])) { inputVSX.Add(inputSplit[i + 2] + "d6"); }
                                            vsx = true;
                                            i += 2;
                                        }
                                        else
                                        {
                                            vsx = true;
                                            inputVSX.Add("1d6");
                                            i += 2;
                                            await ReplyAsync("`Defaulting to 1D6 for PTL.`");
                                        }
                                    }
                                    if (inputSplit.ElementAtOrDefault(i + 1) != null && inputSplit[i + 1].Equals("l"))
                                    {
                                        if (inputSplit.ElementAtOrDefault(i + 2) != null)
                                        {
                                            if (Int32.TryParse(inputSplit[i + 2], out inputLimitVS)) { limitVS = true; }
                                            else { await ReplyAsync("`Requires number after limit call.`"); return; }
                                            i += 2;
                                        }
                                    }
                                    else { i++; }
                                    // this line might need edge/ptl added
                                    //if (inputSplit.ElementAtOrDefault(i + 1) != null && (inputSplit[i + 1].Equals("l")) || (inputSplit[i + 1].Equals("x"))) { i++; }
                                }
                            }
                        }
                        else { await ReplyAsync("`Requires number after vs call.`"); return; }
                        vsTimes++;
                    }
                }
                if (limitTimes == 0)
                {
                    if (inputSplit.ElementAtOrDefault(i) != null && inputSplit[i].Equals("l"))
                    {
                        if (inputSplit.ElementAtOrDefault(i + 1) != null)
                        {
                            if (Int32.TryParse(inputSplit[i + 1], out inputLimit)) { limit = true; }
                            else { await ReplyAsync("`Requires number after limit call.`"); return; }
                            limitTimes++;
                            i += 1;
                        }
                    }
                }
            }
            /*
            // Discard anything that isn't a dice roll, we have all the flags triggered by now
            if (removeAtZero) { while (inputSplit.Count > diceCount) { inputSplit.RemoveAt(diceCount); } }
            else { while (inputSplit.Count > diceCount) { inputSplit.RemoveAt(diceCount); } }    
            */
            // New discard code, remove anything that does not contain "d6"
            int j = 0;
            while (inputSplit.Count > diceCount)
            {
                if (!inputSplit[j].Contains("d6")) { inputSplit.RemoveAt(j); j = 0; }
                else { j++; }
            }

            // Tracking special ruleset triggers
            int[] hitsFails = new int[2];
            // Tracking special ruleset triggers for vs flag
            int[] hitsFailsVS = new int[2];
            // More tracking for yet another ruleset
            int[] hitsFailsSC = new int[2];

            // Temporary? Order of Operations flag
            bool ooo = false;

            // Adding all math operations to the list
            foreach (var item in charInput)
            {
                foreach (var operand in delimiters)
                {
                    if (operand == item && operand != ' ') { operations.Add(item.ToString()); }
                    // Order of Operations check
                    if (item == '/' || item == '*') { ooo = true; }
                }
            }
            // For every individual dice/solid number (IE: 2d6, 5d10, 6, etc)
            for (int i = 0; i < inputSplit.Count; i++)
            {
                // Not necessarily only dice, but allows for storage of results of dice and single add digits to diceTotals
                int currentDiceTotal = 0;
                int currentDiceTotalSC = 0;
                // If it contains a d/D, it's a dice roll
                if (inputSplit[i].Contains("d") || inputSplit[i].Contains("D"))
                {
                    // Store the results of each roll from DiceMath
                    List<int> resultsRolls = new List<int>();
                    resultsRolls = DiceMath.GetDice(inputSplit[i], "sr");


                    if (resultsRolls == null) { await ReplyAsync("Too many dice, or dice do not fit ruleset. Check !help."); }
                    totalRolls += resultsRolls.Count;
                    displayRolls.Add("[");
                    displayRollsSC.Add("[");
                    // Cycle through the list of rolls, adding to the total of this dice
                    for (int k = 0; k < resultsRolls.Count; k++)
                    {
                        displayRolls.Add(resultsRolls[k].ToString());
                        // Special ruleset checks
                        if (resultsRolls[k] == 1) { hitsFails[1]++; }
                        if (resultsRolls[k] == 5 || resultsRolls[k] == 6) { hitsFails[0]++; }
                        if (resultsRolls[k] == 6 && xplodeTens) { resultsRolls.Add(DiceMath.GetDice("1d6", "sr")[0]); }
                        if (k < (resultsRolls.Count - 1)) { displayRolls.Add(", "); }
                        // Add each roll to the total
                        currentDiceTotal += resultsRolls[k];
                        // Second chance checks
                        if (sc)
                        {
                            if (resultsRolls[k] == 1 || resultsRolls[k] == 2 || resultsRolls[k] == 3 || resultsRolls[k] == 4) { resultsRolls[k] = DiceMath.GetDice("1d6", "sr")[0]; }
                            if (resultsRolls[k] == 1) { hitsFailsSC[1]++; }
                            if (resultsRolls[k] == 5 || resultsRolls[k] == 6) { hitsFailsSC[0]++; }
                            displayRollsSC.Add(resultsRolls[k].ToString());
                            if (k < (resultsRolls.Count - 1)) { displayRollsSC.Add(", "); }
                            currentDiceTotalSC += resultsRolls[k];
                        }
                    }
                    displayRolls.Add("]");
                    displayRollsSC.Add("]");
                    if (i != inputSplit.Count - 1)
                    {
                        //Check if an operand was used before trying to access the array.
                        if (operations.ElementAtOrDefault(i) == null) { operations.Add("+"); }
                        displayRolls.Add(" " + operations[i] + " ");
                        displayRollsSC.Add(" " + operations[i] + " ");
                    }
                    diceTotals.Add(currentDiceTotal);
                    diceTotalsSC.Add(currentDiceTotalSC);
                }
                // If it's a solid number
                else if (DiceMath.IsInt(inputSplit[i]))
                {
                    displayRolls.Add(inputSplit[i].ToString());
                    if (i != inputSplit.Count - 1) { displayRolls.Add(" " + operations[i] + " "); }
                    diceTotals.Add(int.Parse(inputSplit[i]));
                    if (sc)
                    {
                        displayRollsSC.Add(inputSplit[i].ToString());
                        if (i != inputSplit.Count - 1) { displayRollsSC.Add(" " + operations[i] + " "); }
                        diceTotalsSC.Add(int.Parse(inputSplit[i]));
                    }
                }
                // Add the fails from the first roll to the SC roll
                if (sc) { hitsFailsSC[1] += hitsFails[1]; }
            }
            // VS flagged dice rolls.
            if (vs)
            {
                for (int i = 0; i < inputVSX.Count; i++)
                {
                    int currentDiceTotalVS = 0;
                    // If it contains a d/D, it's a dice roll
                    if ((i == 0) || (vsx))
                    {
                        if ((inputVSX[i].Contains("d") || inputVSX[i].Contains("D")))
                        {
                            // Store the results of each roll from DiceMath
                            List<int> resultsRollsVS = new List<int>();
                            if (vs) { resultsRollsVS = DiceMath.GetDice(inputVSX[i], "sr"); }

                            if (resultsRollsVS == null && vs) { await ReplyAsync("Error on VS dice roll. Too many dice, or dice do not fit ruleset. Check !help."); return; }
                            if (vs) { totalRollsVS += resultsRollsVS.Count; }
                            // Cycle through the list of rolls, adding to the total of this dice
                            displayRollsVS.Add("[");
                            for (int k = 0; k < resultsRollsVS.Count; k++)
                            {
                                displayRollsVS.Add(resultsRollsVS[k].ToString());
                                // Special ruleset checks
                                if (resultsRollsVS[k] == 1) { hitsFailsVS[1]++; }
                                if (resultsRollsVS[k] == 5 || resultsRollsVS[k] == 6) { hitsFailsVS[0]++; }
                                if (resultsRollsVS[k] == 6 && vsx) { resultsRollsVS.Add(DiceMath.GetDice("1d6", "sr")[0]); }
                                if (k < (resultsRollsVS.Count - 1)) { displayRollsVS.Add(", "); }
                                // Add each roll to the total
                                currentDiceTotalVS += resultsRollsVS[k];
                            }
                            displayRollsVS.Add("]");
                            if (vsx && i != inputVSX.Count - 1)
                            {
                                if (operationsVS.ElementAtOrDefault(i) == null) { operationsVS.Add("+"); }
                                displayRollsVS.Add(" " + operationsVS[i] + " ");
                            }
                            diceTotalsVS.Add(currentDiceTotalVS);
                        }
                    }
                }
            }
            // Calculate dat shit yo
            for (int i = 0; i < diceTotals.Count; i++)
            {
                // First roll is just input, can't do the math without two numbers to work with
                if (i == 0) { total = diceTotals[i]; }
                if (i != 0) { total = DiceMath.DiceCalc(diceTotals[i], total, operations[i - 1]); }
            }
            if (vs)
            {
                for (int i = 0; i < diceTotalsVS.Count; i++)
                {
                    if (i == 0) { totalVS = diceTotalsVS[i]; }
                    if (i != 0 && vsx) { totalVS = DiceMath.DiceCalc(diceTotalsVS[i], totalVS, operationsVS[i - 1]); }
                }
            }
            if (sc)
            {
                for (int i = 0; i < diceTotalsSC.Count; i++)
                {
                    if (i == 0) { totalSC = diceTotalsSC[i]; }
                }
            }

            //Check for glitch/crit glitch in shadowrun
            bool critFail = false;
            bool critFailVS = false;
            bool critFailSC = false;
            bool fail = false;
            bool failVS = false;
            bool failSC = false;
            if ((hitsFails[1] >= (Math.Ceiling(((double)totalRolls / 2)))) && (hitsFails[0] == 0)) { critFail = true; }
            if ((hitsFails[1] >= (Math.Ceiling(((double)totalRolls / 2)))) && (hitsFails[0] != 0)) { fail = true; }
            if (vs) { if ((hitsFailsVS[1] >= (Math.Ceiling(((double)totalRollsVS / 2)))) && (hitsFailsVS[0] == 0)) { critFailVS = true; } }
            if (vs) { if ((hitsFailsVS[1] >= (Math.Ceiling(((double)totalRollsVS / 2)))) && (hitsFailsSC[0] != 0)) { failVS = true; } }
            if (sc)
            {
                // SC cannot negate a previous glitch/critglitch
                if ((hitsFailsSC[1] >= (Math.Ceiling(((double)totalRolls / 2)))) && (hitsFailsSC[0] == 0)) { critFailSC = true; }
                if (critFail) { critFailSC = true; }
                if ((hitsFailsSC[1] >= (Math.Ceiling(((double)totalRolls / 2)))) && (hitsFailsSC[0] != 0)) { failSC = true; }
                if (fail) { failSC = true; }
            }


            // Do the extended test logging for shadowrun
            List<string> report = new List<string>();
            if (extendTest || srReport || delTest)
            {
                // etKeyword = place to look for extended test
                if (delTest)
                {
                    string result = ReportFile.DeleteSR(Context.User.ToString(), etKeyword);
                    if (result == "success") { await ReplyAsync("`Extended Test: " + etKeyword + " for " + Context.User.Username + " deleted.`"); }
                    else { await ReplyAsync("`Extended Test: " + etKeyword + " for " + Context.User.Username + " failed to delete.`"); }
                }
                if (extendTest && !sc)
                {
                    List<string> SRET = new List<string>(new string[] { Context.User.ToString(), etKeyword, hitsFails[0].ToString(), hitsFails[1].ToString() });
                    report = ReportFile.ReportSR(false, SRET);
                }
                if (srReport && !extendTest)
                {
                    List<string> SRET = new List<string>(new string[] { Context.User.ToString(), etKeyword });
                    report = ReportFile.ReportSR(true, SRET);
                    if (report == null) { await ReplyAsync("`No data to present for keyword: " + etKeyword + "`"); return; }
                }
            }

            // Setup ouput
            string display;
            string displayVS = "[";
            string displaySC = "[";
            if (!removeAtZero) { display = displayRolls[0]; }
            // So this is to make sure if you launch with only x/edge/ptl the display don't mess up
            else if (displayXpld) { display = "["; }
            else { display = ""; }
            int temp = 0;
            int tempVS = 0;
            int tempSC = 0;
            int totSucc = 0;
            int totSuccVS = 0;
            int totSuccSC = 0;
            if (limit && !ptl)
            {
                if (hitsFails[0] > inputLimit) { totSucc = inputLimit; }
                else { totSucc = hitsFails[0]; }
                if (sc)
                {
                    if (hitsFailsSC[0] > inputLimit) { totSuccSC = inputLimit; }
                    else { totSuccSC = hitsFailsSC[0]; }
                }
            }
            if (vs) { if (limitVS) { if (hitsFailsVS[0] > inputLimitVS) { totSuccVS = inputLimitVS; } else { totSuccVS = hitsFailsVS[0]; } } }

            foreach (var item in displayRolls)
            {
                Console.Write(item);
                // old line, dont remember why i had temp != 0
                if (temp != 0) { display = display + item; }
                temp++;
            }
            if (sc)
            {
                foreach (var item2 in displayRollsSC)
                {
                    Console.Write(item2);
                    // old line, dont remember why i had temp != 0
                    if (tempSC != 0) { displaySC = displaySC + item2; }
                    tempSC++;
                }
            }
            if (vs)
            {
                foreach (var item in displayRollsVS)
                {
                    Console.Write(item);
                    // old line, dont remember why i had temp != 0
                    if (tempVS != 0) { displayVS = displayVS + item; }
                    tempVS++;
                }
            }
            Console.WriteLine(" = " + total);
            display = display + " = " + total;
            displaySC = displaySC + " = " + totalSC;
            if (vs) { Console.WriteLine(" = " + totalVS); displayVS = displayVS + " = " + totalVS; }

            // Check if results are too long for a discord message
            bool tooLong = false;
            bool tooLongVS = false;
            if (display.Length > 2000) { tooLong = true; }
            if (vs && (displayVS.Length > 2000)) { tooLongVS = true; }

            if (srReport && (inputSplit.Count > 0)) { extendTest = true; }
            // Extended Test
            if (extendTest)
            {
                string resultWord = "";
                if (critFail) { resultWord = "***Critical Glitch.***"; }
                if (fail) { resultWord = "**Glitch.**"; }
                input = "";
                foreach (var item in inputSplit) { input += item + " "; }
                if (xplodeTens && !ptl) { input += ("Push the Limit"); }
                else if (ptl) { input += "Push the Limit"; }

                var builder = new EmbedBuilder()
                    .WithColor(new Color(0x344294))
                    //.WithThumbnailUrl("http://i.imgur.com/KbjvIl6.jpg")
                    .WithAuthor(author => {
                        author
                            .WithName("Shadowrun");
                        //.WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
                    });
                var footer3 = new EmbedFooterBuilder();
                footer3.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
                if (footer3.Text == "Result for ") { footer3.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                //footer3.Text = "Result for " + (((IGuildUser)Context.Message.Author).Nickname);
                //if (footer3.Text == null) { footer3.Text = "Result for " + ((IGuildUser)Context.Message.Author).ToString(); }
                builder.Footer = footer3;
                if (limit && !ptl) { builder.WithDescription($"{input}\nLimit: {inputLimit}\n__*Total Successes:*__ {totSucc}\n {resultWord}"); }
                else if (limit && ptl) { builder.WithDescription($"{input}\nPTL has no limits!"); }
                else { builder.WithDescription(input + "\n" + resultWord); }

                if (!tooLong) { builder.AddField("Rolls", display); }
                else if (tooLong) { builder.AddField("Rolls Total", total); }
                {
                    builder.AddField("Hits", hitsFails[0], inline: true)
                           .AddField("Glitches", hitsFails[1], inline: true);
                }
                var embed = builder.Build();
                await ReplyAsync("", false, embed)
                    .ConfigureAwait(false);

                if (sc)
                {
                    await ReplyAsync("Do you want to use Edge for Second Chance? [y/n] *(30s before timeout)*");
                    var response = await _interactivity.NextMessageAsync();
                    if ((response.ToString()).ToLower() == "y")
                    {
                        List<string> SRET = new List<string>(new string[] { Context.User.ToString(), etKeyword, hitsFailsSC[0].ToString(), hitsFailsSC[1].ToString() });
                        report = ReportFile.ReportSR(false, SRET);

                        input = "";
                        if (critFailSC) { resultWord = "***Critical Glitch.***"; }
                        if (failSC) { resultWord = "**Glitch.**"; }
                        foreach (var item in inputSplit) { input += item + " "; }
                        if (xplodeTens) { input += ("Push the Limit"); }

                        var builder3 = new EmbedBuilder()
                            .WithColor(new Color(0x344294))
                            //.WithThumbnailUrl("http://i.imgur.com/KbjvIl6.jpg")
                            .WithAuthor(author => {
                                author
                                    .WithName("Shadowrun");
                            //.WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
                        });
                        var footer2 = new EmbedFooterBuilder();
                        footer2.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
                        if (footer2.Text == "Result for ") { footer2.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                        //footer2.Text = "Result for " + (((IGuildUser)Context.Message.Author).Nickname);
                        //if (footer2.Text == null) { footer2.Text = "Result for " + ((IGuildUser)Context.Message.Author).ToString(); }
                        builder.Footer = footer2;
                        if (limit) { builder3.WithDescription($"{input} Second Chance!\nLimit: {inputLimit}\n__*Total Successes:*__ {totSuccSC}\n {resultWord}\n*SC rolls cannot negate Glitch/Crit Glitch.\nGlitches are automatically carried over from first roll.*\n"); }
                        else { builder3.WithDescription(input + "\n" + resultWord + "\n*SC rolls cannot negate Glitch/Crit Glitch.\nGlitches are automatically carried over from first roll.*\n"); }
                        if (!tooLong) { builder3.AddField("Rolls", displaySC); }
                        else if (tooLong) { builder3.AddField("Rolls Total", total); }
                        {
                            builder3.AddField("Hits", hitsFailsSC[0], inline: true)
                                   .AddField("Glitches", hitsFailsSC[1], inline: true);
                        }

                        var embed3 = builder3.Build();
                        await ReplyAsync("", false, embed3)
                            .ConfigureAwait(false);

                        var builder2 = new EmbedBuilder()
                        .WithDescription("Extended Test Total: " + etKeyword)
                        .WithColor(new Color(0x344294))
                        //.WithThumbnailUrl("http://i.imgur.com/KbjvIl6.jpg")
                        .WithAuthor(author => {
                            author
                                .WithName("Shadowrun");
                            //.WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
                        })
                        .AddField("Hits", report[0], inline: true)
                        .AddField("Glitches", report[1], inline: true);
                        var footer = new EmbedFooterBuilder();
                        footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
                        if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                        //footer.Text = "Result for " + (((IGuildUser)Context.Message.Author).Nickname);
                        //if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                        builder.Footer = footer;
                        var embed2 = builder2.Build();
                        await ReplyAsync("", false, embed2)
                            .ConfigureAwait(false);
                    }
                    else
                    {
                        await ReplyAsync("*Timeout/Non-y answer.*");
                        var builder2 = new EmbedBuilder()
                        .WithDescription("Extended Test Total: " + etKeyword)
                        .WithColor(new Color(0x344294))
                        //.WithThumbnailUrl("http://i.imgur.com/KbjvIl6.jpg")
                        .WithAuthor(author => {
                            author
                                .WithName("Shadowrun");
                            //.WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
                        })
                        .AddField("Hits", report[0], inline: true)
                        .AddField("Glitches", report[1], inline: true);
                        var footer = new EmbedFooterBuilder();
                        footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
                        if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                        //footer.Text = "Result for " + (((IGuildUser)Context.Message.Author).Nickname);
                        //if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                        builder.Footer = footer;
                        var embed2 = builder2.Build();
                        await ReplyAsync("", false, embed2)
                            .ConfigureAwait(false);
                    }
                }
                else if (!sc)
                {
                    var builder2 = new EmbedBuilder()
                        .WithDescription("Extended Test Total: " + etKeyword)
                        .WithColor(new Color(0x344294))
                        //.WithThumbnailUrl("http://i.imgur.com/KbjvIl6.jpg")
                        .WithAuthor(author => {
                            author
                                .WithName("Shadowrun");
                            //.WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
                        })
                        .AddField("Hits", report[0], inline: true)
                        .AddField("Glitches", report[1], inline: true);
                    var footer = new EmbedFooterBuilder();
                    footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
                    if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                    //footer.Text = "Result for " + (((IGuildUser)Context.Message.Author).Nickname);
                    //if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                    builder.Footer = footer;
                    var embed2 = builder2.Build();
                    await ReplyAsync("", false, embed2)
                        .ConfigureAwait(false);
                }
                
            }
            // Report output for extended test
            if (srReport && !extendTest)
            {
                if (report == null) { await ReplyAsync("`No data to present for keyword: " + etKeyword + "`"); return; }
                else
                {
                    var builder = new EmbedBuilder()
                        .WithDescription("Extended Test Total: " + etKeyword)
                        .WithColor(new Color(0x344294))
                        //.WithThumbnailUrl("http://i.imgur.com/KbjvIl6.jpg")
                        .WithAuthor(author => {
                            author
                                .WithName("Shadowrun");
                            //.WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
                        })
                        .AddField("Hits", report[0], inline: true)
                        .AddField("Glitches", report[1], inline: true);
                    var footer = new EmbedFooterBuilder();
                    footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
                    if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                    //footer.Text = "Result for " + (((IGuildUser)Context.Message.Author).Nickname);
                    //if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                    builder.Footer = footer;
                    var embed = builder.Build();
                    await ReplyAsync("", false, embed)
                        .ConfigureAwait(false);
                }
            }
            // Push the Limit roll
            if (ptl && !extendTest)
            {
                string resultWord = "";
                if (critFail) { resultWord = "***Critical Glitch.***"; }
                if (fail) { resultWord = "**Glitch.**"; }
                input = "";
                foreach (var item in inputSplit) { input += item + " "; }
                if (xplodeTens) { input += ("Push the Limit"); }

                var builder = new EmbedBuilder()
                    .WithColor(new Color(0x344294))
                    //.WithThumbnailUrl("http://i.imgur.com/KbjvIl6.jpg")
                    .WithAuthor(author => {
                        author
                            .WithName("Shadowrun");
                        //.WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
                    });
                var footer = new EmbedFooterBuilder();
                footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
                if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                //footer.Text = "Result for " + (((IGuildUser)Context.Message.Author).Nickname);
                //if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                builder.Footer = footer;
                if (limit && !ptl) { builder.WithDescription($"{input}\nLimit: {inputLimit}\n__*Total Successes:*__ {totSucc}\n {resultWord}"); }
                else if (limit && ptl) { builder.WithDescription($"{input}\nPTL has no limits!"); }
                else { builder.WithDescription(input + "\n" + resultWord); }
                if (!tooLong) { builder.AddField("Rolls", display); }
                else if (tooLong) { builder.AddField("Rolls Total", total); }
                {
                    builder.AddField("Hits", hitsFails[0], inline: true)
                           .AddField("Glitches", hitsFails[1], inline: true);
                }
                var embed = builder.Build();
                await ReplyAsync("", false, embed)
                    .ConfigureAwait(false);
            }
            // Second chance roll
            if (sc && !extendTest)
            {
                string resultWord = "";
                if (critFail) { resultWord = "***Critical Glitch.***"; }
                if (fail) { resultWord = "**Glitch.**"; }
                input = "";
                foreach (var item in inputSplit) { input += item + " "; }
                if (xplodeTens) { input += ("Push the Limit"); }

                var builder = new EmbedBuilder()
                    .WithColor(new Color(0x344294))
                    //.WithThumbnailUrl("http://i.imgur.com/KbjvIl6.jpg")
                    .WithAuthor(author => {
                        author
                            .WithName("Shadowrun");
                        //.WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
                    });
                var footer2 = new EmbedFooterBuilder();
                footer2.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
                if (footer2.Text == "Result for ") { footer2.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                //footer2.Text = "Result for " + (((IGuildUser)Context.Message.Author).Nickname);
                //if (footer2.Text == null) { footer2.Text = "Result for " + ((IGuildUser)Context.Message.Author).ToString(); }
                builder.Footer = footer2;
                if (limit) { builder.WithDescription($"{input}\nLimit: {inputLimit}\n__*Total Successes:*__ {totSucc}\n {resultWord}"); }
                else { builder.WithDescription(input + "\n" + resultWord); }
                if (!tooLong) { builder.AddField("Rolls", display); }
                else if (tooLong) { builder.AddField("Rolls Total", total); }
                {
                    builder.AddField("Hits", hitsFails[0], inline: true)
                           .AddField("Glitches", hitsFails[1], inline: true);
                }
                var embed = builder.Build();
                await ReplyAsync("", false, embed)
                    .ConfigureAwait(false);
                await ReplyAsync("Do you want to use Edge for Second Chance? [y/n] *(30s before timeout)*");
                var response = await _interactivity.NextMessageAsync();
                if ((response.ToString()).ToLower() == "y")
                {
                    input = "";
                    if (critFailSC) { resultWord = "***Critical Glitch.***"; }
                    if (failSC) { resultWord = "**Glitch.**"; }
                    foreach (var item in inputSplit) { input += item + " "; }
                    if (xplodeTens) { input += ("Push the Limit"); }

                    var builder2 = new EmbedBuilder()
                        .WithColor(new Color(0x344294))
                        //.WithThumbnailUrl("http://i.imgur.com/KbjvIl6.jpg")
                        .WithAuthor(author => {
                            author
                                .WithName("Shadowrun");
                            //.WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
                        });
                    var footer = new EmbedFooterBuilder();
                    footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
                    if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                    //footer.Text = "Result for " + (((IGuildUser)Context.Message.Author).Nickname);
                    //if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                    builder.Footer = footer;
                    if (limit) { builder2.WithDescription($"{input} Second Chance!\nLimit: {inputLimit}\n__*Total Successes:*__ {totSuccSC}\n {resultWord}\n*SC rolls cannot negate Glitch/Crit Glitch.\nGlitches are automatically carried over from first roll.*\n"); }
                    else { builder2.WithDescription(input + "\n" + resultWord + "\n*SC rolls cannot negate Glitch/Crit Glitch.\nGlitches are automatically carried over from first roll.*\n"); }
                    if (!tooLong) { builder2.AddField("Rolls", displaySC); }
                    else if (tooLong) { builder2.AddField("Rolls Total", total); }
                    {
                        builder2.AddField("Hits", hitsFailsSC[0], inline: true)
                               .AddField("Glitches", hitsFailsSC[1], inline: true);
                    }
                    var embed2 = builder2.Build();
                    await ReplyAsync("", false, embed2)
                        .ConfigureAwait(false);
                }
                else { await ReplyAsync("*No registered or timed out.*"); }
            }
            // Basic Roll
            if (!srReport && !extendTest && !delTest && !sc && !ptl)
            {
                string resultWord = "";
                if (critFail) { resultWord = "***Critical Glitch.***"; }
                if (fail) { resultWord = "**Glitch.**"; }
                input = "";
                foreach (var item in inputSplit) { input += item + " "; }
                if (xplodeTens) { input += ("Push the Limit"); }

                var builder = new EmbedBuilder()
                    .WithColor(new Color(0x344294))
                    //.WithThumbnailUrl("http://i.imgur.com/KbjvIl6.jpg")
                    .WithAuthor(author => {
                        author
                            .WithName("Shadowrun");
                        //.WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
                    });
                var footer = new EmbedFooterBuilder();
                footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
                if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                //footer.Text = "Result for " + (((IGuildUser)Context.Message.Author).Nickname);
                //if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                builder.Footer = footer;
                if (limit) { builder.WithDescription($"{input}\nLimit: {inputLimit}\n__*Total Successes:*__ {totSucc}\n {resultWord}"); }
                else { builder.WithDescription(input + "\n" + resultWord); }
                if (!tooLong) { builder.AddField("Rolls", display); }
                else if (tooLong) { builder.AddField("Rolls Total", total); }
                {
                    builder.AddField("Hits", hitsFails[0], inline: true)
                           .AddField("Glitches", hitsFails[1], inline: true);
                }
                var embed = builder.Build();
                await ReplyAsync("", false, embed)
                    .ConfigureAwait(false);
            }
            // VS Roll output
            if (!srReport && !extendTest && !delTest && vs)
            {
                string resultWord = "";
                if (critFailVS) { resultWord = "***Critical Glitch.***"; }
                if (failVS) { resultWord = "**Glitch.**"; }
                input = "";
                foreach (var item in inputVSX) { input += item + " "; }
                if (vsx) { input += ("Push the Limit"); }

                var builder = new EmbedBuilder()
                    .WithColor(new Color(0x344294))
                    //.WithThumbnailUrl("http://i.imgur.com/KbjvIl6.jpg")
                    .WithAuthor(author => {
                        author
                            .WithName("Shadowrun");
                        //.WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
                    });
                var footer = new EmbedFooterBuilder();
                footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
                if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                //footer.Text = "Result for " + (((IGuildUser)Context.Message.Author).Nickname);
                //if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                builder.Footer = footer;
                if (limitVS) { builder.WithDescription($"{input}\nLimit: {inputLimitVS}\n__*Total Successes:*__ {totSuccVS}\n {resultWord}"); }
                else { builder.WithDescription(input + "\n" + resultWord); }
                if (!tooLongVS) { builder.AddField("Rolls", displayVS); }
                else if (tooLongVS) { builder.AddField("Rolls Total", totalVS); }
                {
                    builder.AddField("Hits", hitsFailsVS[0], inline: true)
                           .AddField("Glitches", hitsFailsVS[1], inline: true);
                }
                var embed = builder.Build();
                await ReplyAsync("", false, embed)
                    .ConfigureAwait(false);
            }
            if (ooo == true) { await ReplyAsync("```WARNING ||| * or / was used. Order of Operations is NOT in effect. Math is done in the order given. ||| WARNING```"); }
        }

        [Command("wod")]
        [Summary("Roll using World of Darkness rules.")]
        public async Task Wod([Remainder]string input)
        {
            char[] delimiters = { '+', '-', '*', '/', ' ' };
            // All the dice rolls and flat numbers
            List<string> inputSplit = new List<string>(input.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
            // Sort the input into an array for finding operands
            char[] charInput = input.ToCharArray();
            // Put matching delimiters in as operations
            List<string> operations = new List<string>();
            //Store the results of all the rolls to display at the end
            List<string> displayRolls = new List<string>();
            // Storage for each dice roll total
            List<int> diceTotals = new List<int>();
            // And store the final tally
            int total = 0;

            // Difficulty level for WoD rolls (default 6), enable specialization roll for WoD
            int wodDC = 6;

            // WoD specialization roll
            bool wodSpecRoll = false;
            // WoD/SR flag for exploding 10s/edge
            bool xplodeTens = false;

            inputSplit[0] = inputSplit[0] + "d10";
            int toRemove = 0;
            //if (DiceMath.IsInt(inputSplit[1])) { wodDC = int.Parse(inputSplit[1]); inputSplit.RemoveAt(1); }
            foreach (var item in inputSplit)
            {
                if (DiceMath.IsInt(item)) { wodDC = int.Parse(item); toRemove++; }
                //if (item.Equals("dc") || item.Equals("DC")) { wodDC = int.Parse(item.Substring(2, item.Length - 2)); toRemove++; }
                if (item.Equals("spec")) { wodSpecRoll = true; toRemove++; }
                if (item.Equals("x")) { xplodeTens = true; toRemove++; }
            }
            // Remove all the special triggers
            for (int i = 0; i < toRemove; i++) { inputSplit.RemoveAt(1); }

            // Tracking special ruleset triggers
            int[] hitsFails = new int[2];

            // Temporary? Order of Operations flag
            bool ooo = false;

            // Adding all math operations to the list
            foreach (var item in charInput)
            {
                foreach (var operand in delimiters)
                {
                    if (operand == item && operand != ' ') { operations.Add(item.ToString()); }
                    // Order of Operations check
                    if (item == '/' || item == '*') { ooo = true; }
                }
            }
            // For every individual dice/solid number (IE: 2d6, 5d10, 6, etc)
            for (int i = 0; i < inputSplit.Count; i++)
            {
                // Not necessarily only dice, but allows for storage of results of dice and single add digits to diceTotals
                int currentDiceTotal = 0;
                // If it contains a d/D, it's a dice roll
                if (inputSplit[i].Contains("d") || inputSplit[i].Contains("D"))
                {
                    // Store the results of each roll from DiceMath
                    List<int> resultsRolls = new List<int>();
                    resultsRolls = DiceMath.GetDice(inputSplit[i], "wod");

                    if (resultsRolls == null) { await ReplyAsync("Too many dice, or dice do not fit ruleset. Check !help."); }
                    displayRolls.Add("[");
                    // Cycle through the list of rolls, adding to the total of this dice
                    for (int j = 0; j < resultsRolls.Count; j++)
                    {
                        displayRolls.Add(resultsRolls[j].ToString());
                        // Special ruleset checks
                        if (resultsRolls[j] == 1) { hitsFails[1]++; }
                        if (resultsRolls[j] >= wodDC) { hitsFails[0]++; }
                        if (resultsRolls[j] == 10 && wodSpecRoll) { hitsFails[0]++; }
                        if (resultsRolls[j] == 10 && xplodeTens) { resultsRolls.Add(DiceMath.GetDice("1d10", "wod")[0]); }
                        if (j < (resultsRolls.Count - 1)) { displayRolls.Add(", "); }
                        // Add each roll to the total
                        currentDiceTotal += resultsRolls[j];
                    }
                    displayRolls.Add("]");
                    if (i != inputSplit.Count - 1)
                    {
                        //Check if an operand was used before trying to access the arry.
                        if (operations.ElementAtOrDefault(i) == null) { operations.Add("+"); }
                        displayRolls.Add(" " + operations[i] + " ");
                    }
                    diceTotals.Add(currentDiceTotal);
                }
                // If it's a solid number
                else if (DiceMath.IsInt(inputSplit[i]))
                {
                    displayRolls.Add(inputSplit[i].ToString());
                    if (i != inputSplit.Count - 1) { displayRolls.Add(" " + operations[i] + " "); }
                    diceTotals.Add(int.Parse(inputSplit[i]));
                }
            }
            // Calculate dat shit yo
            for (int i = 0; i < diceTotals.Count; i++)
            {
                // First roll is just input, can't do the math without two numbers to work with
                if (i == 0) { total = diceTotals[i]; }
                if (i != 0) { total = DiceMath.DiceCalc(diceTotals[i], total, operations[i - 1]); }
            }
            // Setup ouput
            string display;
            display = displayRolls[0];
            int temp = 0;
            foreach (var item in displayRolls)
            {
                Console.Write(item);
                // old line, dont remember why i had temp != 0
                if (temp != 0) { display = display + item; }
                temp++;
            }

            Console.WriteLine(" = " + total);
            display = display + " = " + total;

            // Check if results are too long for a discord message
            bool tooLong = false;
            if (display.Length > 2000) { tooLong = true; }

            string resultWord = "";
            if ((hitsFails[0] - hitsFails[1]) > 0) { resultWord = "Attempt Success."; }
            else if ((hitsFails[0] - hitsFails[1]) == 0) { resultWord = "Attempt Failed."; }
            else if ((hitsFails[0] - hitsFails[1]) < 0) { resultWord = "Attempt Botched."; }
            input = "";
            foreach (var item in inputSplit) { input += item; }
            if (xplodeTens) { input += (" Exploding Tens"); }
            if (wodSpecRoll) { input += (" Specialization Roll"); }

            var builder = new EmbedBuilder()
                .WithDescription(input + "\nDifficulty: " + wodDC + "\nResult: " + (hitsFails[0] - hitsFails[1]).ToString() + "\n" + resultWord)
                .WithColor(new Color(0x344294))
                //.WithThumbnailUrl("https://i.imgur.com/d61z2jx.jpg")
                .WithAuthor(author => {
                    author
                        .WithName("World of Darkness");
                    //.WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
                });
            var footer = new EmbedFooterBuilder();
            footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
            if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
            //footer.Text = "Result for " + (((IGuildUser)Context.Message.Author).Nickname);
            //if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
            builder.Footer = footer;
            if (!tooLong) { builder.AddField("Rolls", display); }
            else if (tooLong) { builder.AddField("Rolls Total", total); }
            {
                builder.AddField("Successes", hitsFails[0], inline: true)
                       .AddField("Botches", hitsFails[1], inline: true);
            }
            var embed = builder.Build();
            await ReplyAsync("", false, embed)
                .ConfigureAwait(false);

            if (ooo == true) { await ReplyAsync("```WARNING ||| * or / was used. Order of Operations is NOT in effect. Math is done in the order given. ||| WARNING```"); }
        }

        [Command("ep")]
        [Summary("Roll using Eclipse Phase rules.")]
        public async Task EclPha([Remainder] string input = null)
        {
            if (input == null) { await ReplyAsync("Yea... I at least need a target number."); }
            else
            {
                char[] delimiters = { '+', '-', '*', '/', ' ' };
                // All the dice rolls and flat numbers
                List<string> inputSplit = new List<string>(input.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
                // Sort the input into an array for finding operands
                char[] charInput = input.ToCharArray();
                // Put matching delimiters in as operations
                List<string> operations = new List<string>();
                //Store the results of all the rolls to display at the end
                List<string> displayRolls = new List<string>();
                // And store the final tally
                int total = 0;

                // Check if multidice roll
                bool multidice = false;
                if (inputSplit[0].Equals("m")) { multidice = true; }

                if (!multidice)
                {
                    // Target difficulty base
                    int targetValue;
                    bool validInput = Int32.TryParse(inputSplit[0], out targetValue);
                    if (!validInput) { await ReplyAsync("`First input must be number for target value, or use m for multidice roll.`"); return; }
                    else { inputSplit[0] = "1d100"; }

                    bool harder = true; // o bby
                    bool severity = false;
                    if (input.Contains("minor") || input.Contains("moderate") || input.Contains("major"))
                    {
                        if (input.Contains("effortless") || input.Contains("simple") || input.Contains("easy") || input.Contains("difficult") || input.Contains("challenging") || input.Contains("hard"))
                        {
                            if (input.Contains("effortless") || input.Contains("simple") || input.Contains("easy")) { harder = false; }
                            severity = true;
                        }
                        else
                        {
                            await ReplyAsync("`Severity modifiers (Minor/Moderate/Major) require difficulty modifier(Effortless, Simple, Easy, Difficult, Challenging, Hard).`");
                            return;
                        }
                    }
                    input = targetValue.ToString();
                    // Temporary? Order of Operations flag
                    bool ooo = false;
                    // Adding all math operations to the list
                    foreach (var item in charInput)
                    {
                        foreach (var operand in delimiters)
                        {
                            if (operand == item && operand != ' ') { operations.Add(item.ToString()); }
                            // Order of Operations check
                            if (item == '/' || item == '*') { ooo = true; }
                        }
                    }
                    // Flat modifier numbers
                    int targetModifier = 0;
                    int flatMod;
                    int numbersToOperands = 0;
                    for (int i = 1; i < inputSplit.Count; i++)
                    {
                        if (Int32.TryParse(inputSplit[i], out flatMod))
                        {
                            numbersToOperands++;
                            if (numbersToOperands <= operations.Count)
                            {
                                input += operations[numbersToOperands - 1] + flatMod;
                                targetModifier = DiceMath.DiceCalc(flatMod, targetModifier, operations[numbersToOperands - 1]);
                                //if (operations[i - 1].Equals("+")) { targetModifier += flatMod; }
                                //if (operations[i - 1].Equals("-")) { targetModifier -= flatMod; }
                            }
                            else { await ReplyAsync("`Missing a +/- for a modifier.`"); return; }
                        }
                    }

                    // Flagging section
                    bool simpSucc = false;
                    bool crit = false;
                    bool sevFlag = false; // Only allow one severity and difficulty modifier. First come first serve.
                    bool diffFlag = false;

                    foreach (var item in inputSplit)
                    {
                        if (item.Equals("minor") && severity && !sevFlag) { if (harder) { targetModifier -= 10; input += "-10"; } else { targetModifier += 10; input += "+10(Minor)"; } }
                        if (item.Equals("moderate") && severity && !sevFlag) { if (harder) { targetModifier -= 20; input += "-20"; } else { targetModifier += 20; input += "+20(Moderate)"; } }
                        if (item.Equals("major") && severity && !sevFlag) { if (harder) { targetModifier -= 30; input += "-30"; } else { targetModifier += 30; input += "+30(Major)"; } }
                        if (item.Equals("effortless") && !diffFlag) { targetModifier += 30; input += "+30(Effortless)"; }
                        if (item.Equals("simple") && !diffFlag) { targetModifier += 20; input += "+20(Simple)"; }
                        if (item.Equals("easy") && !diffFlag) { targetModifier += 10; input += "+10(Easy)"; }
                        if (item.Equals("difficult") && !diffFlag) { targetModifier -= 10; input += "-10(Difficult)"; }
                        if (item.Equals("challenging") && !diffFlag) { targetModifier -= 20; input += "-20(Challenging)"; }
                        if (item.Equals("hard") && !diffFlag) { targetModifier -= 30; input += "-30(Hard)"; }
                        if (item.Equals("ss")) { simpSucc = true; }
                    }
                    // Remove all the non-dice rolls
                    while (inputSplit.ElementAtOrDefault(1) != null) { inputSplit.RemoveAt(1); }

                    // Check if the targetModifier is > +/-60 and set to +/-60 if true
                    if (targetModifier > 60) { targetModifier = 60; }
                    else if (targetModifier < -60) { targetModifier = -60; }
                    targetValue += targetModifier;
                    // Make the target range in game bounds
                    if (targetValue > 98) { targetValue = 98; }
                    else if (targetValue < 1) { targetValue = 1; }

                    // Not necessarily only dice, but allows for storage of results of dice and single add digits to diceTotals
                    int currentDiceTotal = 0;

                    // Store the results of each roll from DiceMath
                    List<int> resultsRolls = new List<int>();
                    resultsRolls = DiceMath.GetDice(inputSplit[0]);

                    if (resultsRolls == null) { await ReplyAsync("Too many dice, or dice do not fit ruleset. Check !help."); return; }
                    displayRolls.Add("[");
                    // Cycle through the list of rolls, adding to the total of this dice
                    for (int j = 0; j < resultsRolls.Count; j++)
                    {
                        displayRolls.Add((resultsRolls[j] - 1).ToString());
                        if (j < (resultsRolls.Count - 1)) { displayRolls.Add(", "); }
                        // Add each roll to the total
                        currentDiceTotal += (resultsRolls[j] - 1);
                    }
                    displayRolls.Add("]");
                    total = currentDiceTotal;

                    // Setup ouput
                    string display;
                    display = displayRolls[0];
                    int temp = 0;
                    foreach (var item in displayRolls)
                    {
                        Console.Write(item);
                        // old line, dont remember why i had temp != 0, but i need it.
                        if (temp != 0) { display = display + item; }
                        temp++;
                    }

                    Console.WriteLine(" = " + total);
                    display = display + " = " + total;

                    // Check if critical
                    if (total == 00 || total == 11 || total == 22 || total == 33 || total == 44 || total == 55 || total == 66 || total == 77 || total == 88 || total == 99) { crit = true; }

                    string resultWord = "";
                    if (!simpSucc)
                    {
                        if (total <= targetValue)
                        {
                            if (crit) { resultWord = "Attempt **Critical** Success."; }
                            else { resultWord = "Attempt Success."; }
                        }
                        else if (total > targetValue)
                        {
                            if (crit) { resultWord = "Attempt **Critical** Failure."; }
                            else { resultWord = "Attempt Failed."; }
                        }
                    }
                    else
                    {
                        if (total <= targetValue)
                        {
                            if (crit) { resultWord = "Strongly Successful. **(Critical)**"; }
                            else { resultWord = "Strong Success."; }
                        }
                        else if (total > targetValue)
                        {
                            if (crit) { resultWord = "Weakly Successful. **(Critical)**"; }
                            else { resultWord = "Weak Success."; }
                        }
                    }
                    if (simpSucc) { input += (" Simple Success"); }

                    var builder = new EmbedBuilder()
                        .WithDescription(input + "\nTarget Level: " + targetValue + "\nResult: " + total + "\n\n" + resultWord)
                        .WithColor(new Color(0x344294))
                        //.WithThumbnailUrl("https://i.imgur.com/d61z2jx.jpg")
                        .WithAuthor(author => {
                            author
                                .WithName("Eclipse Phase");
                            //.WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
                        });
                    var footer = new EmbedFooterBuilder();
                    footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
                    if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                    //footer.Text = "Result for " + (((IGuildUser)Context.Message.Author).Nickname);
                    //if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                    builder.Footer = footer;
                    builder.AddField("Rolls", display, inline: true);
                    if (total <= targetValue) { builder.AddField("Margin of Success", total, inline: true); }
                    else if (total > targetValue && !simpSucc) { builder.AddField("Margin of Failure", Math.Abs(targetValue - total), inline: true); }
                    else if (total > targetValue && simpSucc) { builder.AddField("Margin of \"Weak Success\"", Math.Abs(targetValue - total), inline: true); }
                    var embed = builder.Build();
                    await ReplyAsync("", false, embed)
                        .ConfigureAwait(false);

                    if (ooo == true) { await ReplyAsync("```WARNING ||| * or / was used. Order of Operations is NOT in effect. Math is done in the order given. ||| WARNING```"); }
                }
                else
                {
                    if (DiceMath.IsInt(inputSplit[1]))
                    {
                        input = inputSplit[1] + "d10";
                        input += " epRoller";
                        await D20(input);
                    }
                    else { await ReplyAsync("`Requires number after m for amount of dice to roll.`"); return; }
                }

            }
        }
    }
}
