﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using ADBv2.Data;

namespace ADBv2.Modules
{
    public class ScheduleModule : ModuleBase<SocketCommandContext>
    {
        [Command("drsadd")]
        [Summary("Add to the DragonRealms quest schedule.")]
        public async Task DRScheduleAdd([Remainder]string input)
        {
            DateTime questTime;
            // Set up timezone offset
            TimeSpan ctz = TimeSpan.Zero;
            ctz = TimeZoneInfo.Local.BaseUtcOffset - ctz;

            char[] delimiters = { '\n' };
            List<string> inputSplit = new List<string>(input.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));

            if (inputSplit.Count < 4)
            {
                await ReplyAsync("Requires at least Quest Type, Ticket Number, Date, and Time. Password optional.");
                await ReplyAsync("*Format:*\nBTB\n389\n05/07\n09:30 PM (ET)\npassword\n\nCopy paste from DR web page. Quest types are BTB, PR, and AT.");
                return;
            }
            // Make sure the quest type is valid
            List<string> questType = new List<string> { "BTB", "PR", "AT" };
            if (!questType.Contains(inputSplit[0].ToUpper())) { await ReplyAsync("Quest Type must be BTB, PR, or AT."); return; }
            // Remove (ET)
            inputSplit[3] = inputSplit[3].Substring(0, inputSplit[3].Length - 4);
            // Parse the time
            bool validTime = DateTime.TryParse(inputSplit[3], out questTime);
            if (!validTime) { await ReplyAsync("`Error with time validity.`"); return; }
            // Offset it to UTC
            questTime += ctz;
            List<string> result = ReportFile.DRSchedule("add", inputSplit, questTime);

            if (result[0] == "success") { await ReplyAsync("Quest added to schedule!"); }
            else if (result[0] == "exists") { await ReplyAsync("Ticket number already exists!"); }
            else { await ReplyAsync("Error adding quest to scheudle."); }
            return;
        }

        [Command("drs")]
        [Summary("Report the DragonRealms Quest Schedule.")]
        public async Task DRScheduleReport([Remainder]string input = null)
        {
            DateTime dateToCheck = DateTime.Today;
            if (input == null) { input = DateTime.Today.ToShortDateString(); }
            else if (!DateTime.TryParse(input, out dateToCheck)) { await ReplyAsync("Unable to convert to a proper date. Please use DD/MM."); return; }

            List<string> trash = new List<string>();
            List<string> result = ReportFile.DRSchedule("get", trash, dateToCheck);

            if (result[0] == "nonexistant") { await ReplyAsync("No quests for that day exist."); return; }

            char[] delimiters = { ' ', '\n' };
            

            var builder = new EmbedBuilder()
                .WithColor(new Color(0x2EE4F2))
                .WithAuthor(author =>
                {
                    author
                        .WithName(dateToCheck.ToShortDateString().ToString() + " Quests");
                });
            List<string> timeList = new List<string>();
            // For every quest in result
            foreach (var quest in result)
            {
                List<string> questList = new List<string>();
                List<string> outputSplit = new List<string>(quest.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
                // Split all the info back up
                foreach (var qItem in outputSplit)
                {
                    if (qItem == "BTB") { questList.Add("Beyond the Barrier"); }
                    else if (qItem == "AT") { questList.Add("Akigwe's Tower"); }
                    else if (qItem == "PR") { questList.Add("Prison Riot"); }
                    else { questList.Add(qItem); }
                }
                // The results of questList should be:
                //  [0] Quest Type - BTB/AT/PR
                //  [1] Ticket
                //  [2] Date
                //  [3] Time
                //  [4] AM/PM - should be ignored, will be added to 3
                //  [5] Password - if exists

                // This line adds the AM/PM to the Time
                questList[3] += " " + questList[4];
                // If password doesn't exist, add a blank line, else output will have issues
                if (questList.Count != 6) { questList.Add(""); }


                // Wibbly wobbly timey wimey stuff for zone conversions. Stolen from my Timezone module because I'm too lazy to adjust it for access from there.
                DateTime timeToConvert;
                // By default, if no specific time is requestd, do it for whatever time it is right now.
                timeToConvert = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now);

                // Modifyable list of US Timezones to check for
                List<string> tzc = TimeZoneCheckUS();
                List<TimeZoneInfo> zones = new List<TimeZoneInfo>();
                List<TimeZoneInfo> zUsed = new List<TimeZoneInfo>();
                // Put alllll the zones in a list.
                foreach (var z in TimeZoneInfo.GetSystemTimeZones())
                {
                    zones.Add(z);
                }
                // Cross reference the list of desired zones and add the actual data to the list of zones we'll convert to.
                foreach (var tz in zones)
                {
                    if (tzc.Contains(tz.Id)) { zUsed.Add(tz); }
                }

                // Modifyable list of US Timezone abbreviations
                Dictionary<string, string> tza = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
                Dictionary<string, string> tzar = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
                tza = TimeZoneAbbreviationUS();
                tzar = TimeZoneAbbrRevUS();
                {
                    TimeSpan zoneReq = TimeSpan.Zero;
                    // Base timezone requested [DR quests always set for EST]
                    string zoneRequest = "est";
                    // Filter to proper DateTime name matching
                    tza.TryGetValue(zoneRequest, out zoneRequest);

                    // Convert the input to valid DateTime parameters
                    bool validInput = DateTime.TryParse(questList[3], out timeToConvert);
                    if (!validInput) { await ReplyAsync("`Error converting time."); }
                    // Find the valid timezone
                    foreach (var tz in zUsed) { if (tz.Id.ToString().Contains(zoneRequest)) { zoneReq = tz.BaseUtcOffset; } }
                    //if (zoneReq == TimeSpan.Zero) { await ReplyAsync("`Invalid timezone. Valid ones are: hst, akst, pst, mst, cst, est"); }
                    // Shift the timezone offset from local time to the requested one
                    zoneReq = TimeZoneInfo.Local.BaseUtcOffset - zoneReq;
                    timeToConvert += zoneReq;
                    // Convert to UTC for output method (which converts from UTC)
                    timeToConvert = TimeZoneInfo.ConvertTimeToUtc(timeToConvert);

                    
                    foreach (var zone in zUsed)
                    {                        
                        string s = (zone.StandardName.ToString());
                        tzar.TryGetValue(s, out s);
                        // Remove "Standard Time" from each name. It gets a bit long and ugly using inline fields.
                        //string zoneName = s.Substring(0, s.IndexOf(" Standard"));
                        //builder.AddInlineField(zoneName, (TimeZoneInfo.ConvertTimeFromUtc(timeToConvert, zone)).ToShortTimeString());
                        timeList.Add((TimeZoneInfo.ConvertTimeFromUtc(timeToConvert, zone)).ToShortTimeString() + " " + s.ToString().ToUpper());
                    }
                }
                // timeList order: 2 = est, 1 = cst, 0 = pst
                builder.AddField(questList[0], $"Ticket: {questList[1]}\nPassword: {questList[5]}\nTime:\n     {timeList[2]}\n     {timeList[1]}\n     {timeList[0]}", inline: true);
                timeList.Clear();
            }

            var embed = builder.Build();
            await ReplyAsync("", false, embed).ConfigureAwait(false);
        }

        public Dictionary<string, string> TimeZoneAbbreviationUS()
        {
            Dictionary<string, string> TimeZoneAbbr = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

            //TimeZoneAbbr.Add("akst", "Alaskan Standard Time");
            //TimeZoneAbbr.Add("hst", "Hawaiian Standard Time");
            //TimeZoneAbbr.Add("mst", "Mountain Standard Time");
            TimeZoneAbbr.Add("cst", "Central Standard Time");
            TimeZoneAbbr.Add("est", "Eastern Standard Time");
            TimeZoneAbbr.Add("pst", "Pacific Standard Time");

            return TimeZoneAbbr;
        }

        public Dictionary<string, string> TimeZoneAbbrRevUS()
        {
            Dictionary<string, string> TimeZoneAbbr = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

            //TimeZoneAbbr.Add("akst", "Alaskan Standard Time");
            //TimeZoneAbbr.Add("hst", "Hawaiian Standard Time");
            //TimeZoneAbbr.Add("mst", "Mountain Standard Time");
            TimeZoneAbbr.Add("Central Standard Time", "cst");
            TimeZoneAbbr.Add("Eastern Standard Time", "est");
            TimeZoneAbbr.Add("Pacific Standard Time", "pst");

            return TimeZoneAbbr;
        }

        public List<string> TimeZoneCheckUS()
        {
            List<string> TimeZoneCheck = new List<string>();

            //TimeZoneCheck.Add("Alaskan Standard Time");
            //TimeZoneCheck.Add("Hawaiian Standard Time");
            //TimeZoneCheck.Add("Mountain Standard Time");
            TimeZoneCheck.Add("Central Standard Time");
            TimeZoneCheck.Add("Eastern Standard Time");
            TimeZoneCheck.Add("Pacific Standard Time");

            return TimeZoneCheck;
        }
    }
}
