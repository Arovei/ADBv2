﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Discord;
using Discord.Commands;
using Interactivity;
using DarkSky.Services;
using DarkSky.Models;
using DarkSky.Extensions;
using OpenWeatherAPI;
using ADBv2.Services;
using ADBv2.Data;

namespace ADBv2.Modules
{
    public class WeatherModule : ModuleBase<SocketCommandContext>
    {
        ConfigService key = new ConfigService();

        //readonly double _latitude = 42.915;
        //readonly double _longitude = -78.741;
        //private DarkSkyService _darkSky;        

        public WeatherModule()
        {
            //var apiKey = key.OpenWeatherAPI;
            // Is posting even when working, fix later.
            //if (!string.IsNullOrWhiteSpace(apiKey)) { Console.WriteLine("API key not set."); };
            //_darkSky = new DarkSkyService(apiKey);
                       
        }

        [Command("weather"), Alias("w")]
        [Summary("Display weather for selected area.")]
        public async Task WeatherCall([Remainder]string input = null)
        {
            double _latitude = 0;
            double _longitude = 0;

            if (input != null && input.Contains(" add "))
            {
                char[] delimiters = { ' ' };
                List<string> inputSplit = new List<string>(input.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
                if (inputSplit.Count < 2) { await ReplyAsync("Please use !w add [zip code/location]"); return; }
                inputSplit.Add(Context.User.ToString());
                List<string> resultTwo = ReportFile.WeatherLocations("add", inputSplit);

                if (resultTwo[0] == "success") { await ReplyAsync("Location successfully added. Fetching weather..."); }
                else { await ReplyAsync("Error adding location."); }
            }

            if (input != null && input.ToLower() == "the sun")
            {
                string display = "```Sun Surface Temperature:\n5788 K\n5504.85 °C\n9940.73 °F```";
                await ReplyAsync(display);
                await ReplyAsync("https://i.imgur.com/GWdMXIJ.mp4");
                return;
            }

            if (input == null || input.Contains("add"))
            {
                List<string> locTable = new List<string>();
                locTable = ReportFile.WeatherLocations("get", null);
                if (locTable[0] != "nonexistant")
                {
                    // inputSplit[0] = User
                    // inputSplit[1] = Location

                    char[] delimiters = { '|' };
                    List<string> inputSplit = new List<string>(locTable[0].Split(delimiters, StringSplitOptions.RemoveEmptyEntries));

                    for (int i = 0; i < inputSplit.Count; i++)
                    {
                        if (Context.User.ToString() == inputSplit[i]) { input = inputSplit[i + 1]; break; }
                    }
                    if (input == null) { await ReplyAsync("No saved location stored, please save one using `!w add [location]`, or `!w [location]` to use without saving."); return; }
                }
                else { await ReplyAsync("No saved location stored, please save one using `!w add [location]` or `!w [location]` to use without saving."); return; }
            }

            //List<string> inputSplit = new List<string>(input.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));

            LocationResult locResults = new LocationResult();
            var moreLocResults = locResults.GetLocation(input).Result;
            var result = moreLocResults.Features;

            /*
             * Other cities feature
            int i = 0;
            if (result.Count > 1)
            {
                foreach (var item in result)
                {
                    if (item.AdminArea5.ToUpper().Contains(inputSplit[0].ToUpper()) || item.PostalCode.Contains(input))
                    {
                        break;
                    }
                    else
                    {
                        if (i+1 == result[0].Locations.Count)
                        {
                            await ReplyAsync("`No results found for city.`");
                            return;
                        }                        
                    }
                    i++;
                }
            }
            */

            _latitude = result[0].Geometry.Coordinates[1];
            _longitude = result[0].Geometry.Coordinates[0];
            //string locQuery = _latitude.ToString() + "," + _longitude.ToString();

            //var queryContent = string.Format(Uri.EscapeDataString(input));
            //var locResult = await GetLocInfo(queryContent);

            /*var forecast = await _darkSky.GetForecast(_latitude, _longitude, new OptionalParameters
            {
                ExtendHourly = true,
                //DataBlocksToExclude = new List<ExclusionBlock> { ExclusionBlock.Flags },
                //LanguageCode = "english",
                //MeasurementUnits = "si",
            });*/
            WeatherResult weatherResult = new WeatherResult();

            var forecast = await weatherResult.QueryByLatLong(_latitude.ToString(), _longitude.ToString());

            //var openWeatherAPI = new OpenWeatherApiClient(key.OpenWeatherAPI);
            //var query = await openWeatherAPI.QueryAsync(input);

            var location = $"{result[0].PlaceName}";
            //var obvTime = "PirateWeather is in Beta, results can be slow.";
            DateTime obvTime = DateTimeOffset.FromUnixTimeSeconds(forecast.Current.Dt).DateTime;
            obvTime = obvTime.AddSeconds(forecast.TimezoneOffset);
            //forecast.Currently.Time; //was DateTime

            var temperature = forecast.Current.Temp.Value;
            //var temperature = Math.Round(forecast.Currently.Temperature ?? default, 2); //pirateweather
            //var tempc = ConvertToC(temperature);
            var feelsLike = forecast.Current.FeelsLike.Value;
            //var feelsLike = Math.Round(forecast.Currently.ApparentTemperature ?? default, 2); //pirateweather
            //var feelsLikeC = ConvertToC(feelsLike);
            var dewpoint = forecast.Current.DewPoint.Value;
            //var dewpoint = Math.Round(forecast.Currently.DewPoint ?? default, 2); //pirateweather
            //var dewpointC = ConvertToC(dewpoint);
            //var windChill = weatherInfo.CurrentObservation.WindchillString;
            //var visibility = weatherInfo.CurrentObservation.VisibilityKm + "(KM), " + weatherInfo.CurrentObservation.VisibilityMi + "(MI)";
            //var weather = weatherInfo.CurrentObservation.Weather;
            var humidity = forecast.Current.Humidity.Value;
            double? precip = 0;
            if (forecast.Hourly[0].Rain != null) { precip = forecast.Hourly[0].Rain.The1H; }
            //if (forecast.Current.Snow != null) { precip = forecast.Current.Snow[0].oneh.Value; }
            //var weathIcon = forecast.Current.Icon;
            //var heatIndex = forecast.Response.Currently;

            string windInfo = "0";
            string windInfoKPH = "0";
            if (forecast.Current.WindSpeed != null)
            {
                windInfo = $"{ConvertToMPH(forecast.Current.WindSpeed)} MPH/{ConvertToMPH(forecast.Current.WindGust)} MPH";
                windInfoKPH = $"{ConvertToKPH(forecast.Current.WindSpeed)} KPH/{ConvertToKPH(forecast.Current.WindGust)} KPH";
            }
            var forecastLink = $"https://openweathermap.org/weathermap?basemap=map&cities=true&layer=temperature&lat={_latitude}&lon={_longitude}&zoom=12";
            List<string> alertInfo = new List<string>();
            List<string> alertDesc = new List<string>();
            var alertTemp = "";
            if (forecast.Alerts != null) {
                foreach (var alert in forecast.Alerts) {
                    //forecast.Response.Alerts[0]
                    alertInfo.Add(alert.Event);
                    alertTemp = alert.Description;

                    alertTemp = alertTemp.Truncate(50);
                    alertDesc.Add(alertTemp + "...");
                }
                
            }

            string baseIconURL = "http://openweathermap.org/img/wn/";
            string specIconURL = "";
            switch (forecast.Current.Weather[0].Icon)
            {
                case "01d":
                    specIconURL = "01d@2x.png";
                    break;
                case "01n":
                    specIconURL = "01n@2x.png";
                    break;
                case "02d":
                    specIconURL = "02d@2x.png";
                    break;
                case "02n":
                    specIconURL = "02n@2x.png";
                    break;
                case "03d":
                    specIconURL = "03d@2x.png";
                    break;
                case "03n":
                    specIconURL = "03n@2x.png";
                    break;
                case "04d":
                    specIconURL = "04d@2x.png";
                    break;
                case "04n":
                    specIconURL = "04n@2x.png";
                    break;
                case "09d":
                    specIconURL = "09d@2x.png";
                    break;
                case "09n":
                    specIconURL = "09n@2x.png";
                    break;
                case "10d":
                    specIconURL = "10d@2x.png";
                    break;
                case "10n":
                    specIconURL = "10n@2x.png";
                    break;
                case "11d":
                    specIconURL = "11d@2x.png";
                    break;
                case "11n":
                    specIconURL = "11n@2x.png";
                    break;
                case "13d":
                    specIconURL = "13d@2x.png";
                    break;
                case "13n":
                    specIconURL = "13n@2x.png";
                    break;
                case "50d":
                    specIconURL = "50d@2x.png";
                    break;
                case "50n":
                    specIconURL = "50n@2x.png";
                    break;
                default:
                    baseIconURL = "https://i.imgur.com/rC9Qwt5.png";
                    break;
            }

            var builder = new EmbedBuilder()
                    .WithDescription($"**[{location}]({forecastLink})**")
                    .WithColor(new Color(0x77AB9A))
                    .AddField("Current Weather:", forecast.Current.Weather[0].Description)
                    //.WithThumbnailUrl(weathIcon)  *** Leaving this in makes inline only do 2 per row instead of 3
                    .AddField("Temperature", $"{ConvertToF(temperature)}° F\n{ConvertToC(temperature)}° C", inline: true)
                    .AddField("Feels Like", $"{ConvertToF(feelsLike)}° F\n{ConvertToC(feelsLike)}° C", inline: true)
                    //.AddInlineField("Heat Index", heatIndex)
                    //.AddInlineField("Weather", weather)
                    //.AddField("Relative Humidity", Math.Round(humidity*100 ?? default, 2) + "%", inline: true) //pirateweather
                    .AddField("Relative Humidity", humidity + "%", inline: true)
                    .AddField("Dewpoint", $"{ConvertToF(dewpoint)}° F\n{ConvertToC(dewpoint)}° C", inline: true)
                    .AddField("Precip Intensity", $"{ConvertToInches(precip)} inches/hour\n{ConvertToCM(precip)} cm/hour", inline: true)
                    //.AddInlineField("Visibility", visibility)                    
                    .AddField("Wind Speed/Gust", $"{windInfo}\n{windInfoKPH}", inline: true)
                    .WithThumbnailUrl(baseIconURL+specIconURL)
                    .WithFooter(footer =>
                    {
                        footer
                            .WithText(obvTime.ToString() + " " + forecast.Timezone);
                    })
                    .WithAuthor(author =>
                    {
                        author
                            .WithName("OpenWeather")
                            .WithUrl("https://openweathermap.org/")
                            .WithIconUrl("https://i.imgur.com/rC9Qwt5.png");
                    });
            if (alertInfo.Count > 0)
            {
                for (int i = 0; i < alertInfo.Count; i++)
                {
                    builder.AddField(alertInfo[i], $"{alertDesc[i]}");
                }
            }
            var embed = builder.Build();
            await ReplyAsync("", false, embed);
            
            /*
            Assert.NotNull(forecast);
            Assert.NotNull(forecast.Response);
            Assert.NotNull(forecast.Headers);
            Assert.NotEmpty(forecast.Response.Daily.Data);
            Assert.NotEmpty(forecast.Response.Hourly.Data);
            Assert.Null(forecast.Response.Flags);
            Assert.Equal(forecast.Response.Latitude, _latitude);
            Assert.Equal(forecast.Response.Longitude, _longitude);
            */

            /*
             * This section for finding other cities. Slightly buggy as it grabs too many options.
            {
                int i = 1;
                string locs = "";
                foreach (var item in latLng) { locs += ($"{i}. {item.AdminArea5}, {item.AdminArea3}, {item.AdminArea1}\n"); i++; }

                var builder = new EmbedBuilder()
                    .WithDescription(locs)
                    .WithTitle("Locations available:")
                    .WithColor(new Color(0xE6E6FA));
                var embed = builder.Build();
                await ReplyAsync("", false, embed);
                await ReplyAsync("Please choose a correspoinding number. *[30s before timeout]*");
                var response = await NextMessageAsync();
                if (DiceMath.IsInt(response.ToString()))
                {
                    Int32.TryParse(response.ToString(), out i);
                    string formattedLoc = latLng[i-1].AdminArea5 + " " + latLng[i-1].AdminArea3;
                    await WeatherCall(formattedLoc);
                }
                else if (!DiceMath.IsInt(response.ToString())) { await ReplyAsync("`Non-numeric value input.`"); return; }
                else { await ReplyAsync("`Timeout`"); return; }
            }
            */
        }

        private double? ConvertToC(double? tempK)
        {
            // From Kelvin
            double result = 0;
            if (tempK != null) result = Math.Round(tempK.Value - 273.15, 2);
            return result;
        }
        private double? ConvertToF(double? tempK)
        {
            // From Kelvin
            double result = 0;
            if (tempK != null) result = Math.Round((tempK.Value - 273.15) * 9/5 + 32, 2);
            return result;
        }

        private double? ConvertToInches(double? mm)
        {
            double result = 0;
            if (mm != null) result = Math.Round(mm.Value / 25.4, 4);
            return result;
        }
        private double? ConvertToCM(double? mm)
        {
            double result = 0;
            if (mm != null) result = Math.Round(mm.Value / 10, 4);
            return result;
        }
        private double? ConvertToKPH(double? mps)
        {
            // From m/s
            double result = 0;
            if (mps != null) result = Math.Round(mps.Value * 3.6, 2);
            return result;
        }

        private double? ConvertToMPH(double? mps)
        {
            // From m/s
            double result = 0;
            if (mps != null) result = Math.Round(mps.Value * 2.237, 2);
            return result;
        }


    }
}
        /* OLD WUNDERGROUND CODE
        await ReplyAsync("`Weather Underground API has been discontinued. This module no longer works.`"); return;
        if (input == null) { await ReplyAsync("`Requires location input.`"); return; }
        else
        {
            WeatherResult weatherResults = new WeatherResult();
            var locResults = weatherResults.GetLocation(input).Result;
            var result = locResults.Results;

            if (result.Length == 0)
            {
                await ReplyAsync($"`No results for requested area.`");
                return;
            }
            else if (result.Length == 1)
            {
                var weatherInfo = weatherResults.QueryByTerm(result[0].L).Result;
                if (weatherInfo == null) { await ReplyAsync("`Wunderground API key not set.`"); }

                var location = weatherInfo.CurrentObservation.DisplayLocation["full"] + ", " + weatherInfo.CurrentObservation.DisplayLocation["country"];
                var obvTime = weatherInfo.CurrentObservation.ObservationTime;
                var temperature = weatherInfo.CurrentObservation.TemperatureString;
                var feelsLike = weatherInfo.CurrentObservation.FeelslikeString;
                //var dewpoint = weatherInfo.CurrentObservation.DewpointString;
                var windChill = weatherInfo.CurrentObservation.WindchillString;
                //var visibility = weatherInfo.CurrentObservation.VisibilityKm + "(KM), " + weatherInfo.CurrentObservation.VisibilityMi + "(MI)";
                var weather = weatherInfo.CurrentObservation.Weather;
                var humidity = weatherInfo.CurrentObservation.RelativeHumidity;
                var precip = weatherInfo.CurrentObservation.PrecipTodayString;
                var weathIcon = weatherInfo.CurrentObservation.IconUrl;
                var heatIndex = weatherInfo.CurrentObservation.HeatIndexString;
                //var windInfo = ($"{weatherInfo.CurrentObservation.WindMph} MPH/{weatherInfo.CurrentObservation.WindGustMph} MPH");
                var windInfo2 = ($"{weatherInfo.CurrentObservation.WindMph} MPH ({weatherInfo.CurrentObservation.WindKph} KPH)/{weatherInfo.CurrentObservation.WindGustMph} MPH ({weatherInfo.CurrentObservation.WindGustKph} KPH)");

                var builder = new EmbedBuilder()
                    .WithDescription($"**[{location}]({weatherInfo.CurrentObservation.ForecastUrl})**")
                    .WithColor(new Color(0x77AB9A))
                    //.WithThumbnailUrl(weathIcon)  *** Leaving this in makes inline only do 2 per row instead of 3
                    .AddInlineField("Temperature", temperature)
                    .AddInlineField("Feels Like", feelsLike)
                    .AddInlineField("Heat Index", heatIndex)
                    .AddInlineField("Weather", weather)
                    .AddInlineField("Relative Humidity", humidity)
                    .AddInlineField("Precip [Today]", precip)
                    //.AddInlineField("Visibility", visibility)
                    //.AddInlineField("Dewpoint", dewpoint)
                    .AddInlineField("Wind Speed/Gust", windInfo2)
                    .WithFooter(footer =>
                    {
                        footer
                            .WithText(obvTime);
                    })
                    .WithAuthor(author =>
                    {
                        author
                            .WithName(weatherInfo.CurrentObservation.Image.Title)
                            .WithUrl(weatherInfo.CurrentObservation.Image.Link)
                            .WithIconUrl(weatherInfo.CurrentObservation.Image.Url);
                    });
                var embed = builder.Build();
                await ReplyAsync("", false, embed);
            }
            else
            {
                int i = 1;
                string locs = "";
                foreach (var item in result) { locs += ($"{i}. {item.Name}\n"); i++; }

                var builder = new EmbedBuilder()
                    .WithDescription(locs)
                    .WithTitle("Locations available:")
                    .WithColor(new Color(0xE6E6FA));
                var embed = builder.Build();
                await ReplyAsync("", false, embed);
                await ReplyAsync("Please choose a correspoinding number. *[30s before timeout]*");
                var response = await NextMessageAsync();
                if (DiceMath.IsInt(response.ToString()))
                {
                    Int32.TryParse(response.ToString(), out i);
                    await WeatherCall(result[i-1].Name);
                }
                else if (!DiceMath.IsInt(response.ToString())) { await ReplyAsync("`Non-numeric value input.`"); return; }
                else { await ReplyAsync("`Timeout`"); return; }
            }
        }
        */