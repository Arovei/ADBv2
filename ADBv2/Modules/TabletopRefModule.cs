﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Discord.Commands;
using Interactivity;
/*
namespace ADBv2.Modules
{
    public class TabletopRefModule : ModuleBase<SocketCommandContext>
    {        
        [Command("ref")]
        [Summary("Reference command information.")]
        public async Task Cs()
        {
            var builder = new EmbedBuilder()
                        .WithTitle("Reference options: ")
                        .WithColor(new Color(0xC16BF2))
                        .AddField("Shadowrun 5E", "!ref sr")
                        .AddField("D&D 5E", "!ref dnd")
                        .WithFooter("This message will auto-delete after 2 minutes.");
            var embed = builder.Build();
            await ReplyAsync(embed: embed).ConfigureAwait(false);
            var Items = await Context.Channel.GetMessagesAsync().FlattenAsync();
            var usermessages = Items.Where(x => x.Author == Context.Message.Author as SocketGuildUser).Take(1);
            var delmsg = usermessages.First();
            await Context.Channel.DeleteMessageAsync(delmsg);
        }

        [Command("ref sr")]
        [Summary("Shadowrun 5E References.")]
        public async Task CsSr([Remainder]string input = "")
        {
            var Items = await Context.Channel.GetMessagesAsync().FlattenAsync();
            var usermessages = Items.Where(x => x.Author == Context.Message.Author as SocketGuildUser).Take(1);
            var delmsg = usermessages.First();
            await Context.Channel.DeleteMessageAsync(delmsg);
            var builder = new EmbedBuilder()
                        .WithTitle("Shadowrun 5E References: ")
                        .WithColor(new Color(0xC16BF2))
                        .AddField("General", "[General Help](https://drive.google.com/open?id=0BxpJzB_orMORWjd2ZHZXWFU1Ym8)\n[Cheat Sheet](https://drive.google.com/open?id=0BxpJzB_orMORN0FQbF82Qm5udnc)\n[Rules Summary - GM](https://drive.google.com/open?id=0BxpJzB_orMORVE5WZ3FKMUZaTGc)")
                        .AddField("Hayek Sheets", "[Drive Folder](https://drive.google.com/open?id=0BxpJzB_orMORVVFvdm14cjJHWjA)")
                        .AddField("Summaries Folder", "[Drive Folder](https://drive.google.com/open?id=0BxpJzB_orMORUkljOXhrUWFPeFk)")
                        .AddField("Combat", "[SR Combat Quick Reference - adragon202](https://drive.google.com/open?id=0BxpJzB_orMORa1p3UDZzRWp3RlU)\n[Rules Summary - Combat](https://drive.google.com/open?id=0BxpJzB_orMORQXJJZnpYNmthcm8)")
                        .AddField("Gear", "[Rules Summary - Gear](https://drive.google.com/open?id=0BxpJzB_orMORT2huYWQ3UUlTWEk)")
                        .AddField("Magic", "[SR Magic Quick Reference - adragon202](https://drive.google.com/open?id=0BxpJzB_orMORbUlSQUVfN1k3UVE)\n[Rules Summary - Magic](https://drive.google.com/open?id=0BxpJzB_orMORYXRYYzM3Q2t4UDg)")
                        .AddField("Matrix", "[SR Matrix & Rigging Quick Reference - adragon202](https://drive.google.com/open?id=0BxpJzB_orMOReEJucU1UQVNjS0k)\n[Rules Summary - Matrix](https://drive.google.com/open?id=0BxpJzB_orMORenM5ZjNvdUhXLWM)")
                        .AddField("Rigging", "[SR Matrix & Rigging Quick Reference - adragon202](https://drive.google.com/open?id=0BxpJzB_orMOReEJucU1UQVNjS0k)\n[Rules Summary - Rigger](https://drive.google.com/open?id=0BxpJzB_orMORU1dNc3laNm9kOHc)\n[Riggers](https://drive.google.com/open?id=0BxpJzB_orMOROG5WTmNYdkpOSVk)")
                        .AddField("Skills/Qualities", "[Rules Summary - Skills & Qualities](https://drive.google.com/open?id=0BxpJzB_orMORQWVvelNVZHNHTmc)")
                        .AddField("Vehicles", "[Vehicles](https://drive.google.com/open?id=0BxpJzB_orMORU1dNc3laNm9kOHc) *This is the Rigger document, has vehicle info as well.*")
                        .WithFooter("This message will auto-delete after 2 minutes.");
            if (input.Contains("perm")) { builder.WithFooter("Message set to not delete."); }
            var embed = builder.Build();
            if (input.Contains("perm")) { await ReplyAsync("", false, embed).ConfigureAwait(false); }
            else { await ReplyAsync("", false, embed).ConfigureAwait(false); }            
            /*
            var pages = new[] {
                "**!roll | !r <Dice Amount>D<Dice sides>**\nRolls dice.\n\n***Examples***: !roll 2D6 | !roll 1D10\nCan also use !r. Ex: !r 2d10 | !r 5d10-3+6d5",
                "**!sr <Dice Amount> [Modifiers]**\nShadowrun guidelines dice.\n\n***Examples***: !sr 20 | !sr 8 x 2\n***Modifiers***: [x/edge/ptl], [vs/v], [l], [sc]\n[x [#] , edge [#] , ptl [#]] - Use edge for Push the Limit. " +
                "Rolling a 6 adds a extra dice. Defaults to 1 edge dice. This is usable by itself by calling \"!sr <ptl/x/edge> <#>\"\n[vs] , [v] - Versus roll. Accepts edge use. Use to roll two sets of dice seperately." +
                "\n[l] - Specify a limit.\n[sc] - Second Chance. Prompts if you want to expend an edge dice to reroll all non-hit rolls. Automatically carries over the glitches from the first roll.",
                "**!wod <Dice Amount> [Modifiers]**\nWorld of Darkness guidlines dice. Defaults to 6 for difficulty.\n\n***Examples***: !wod 10 | !wod 8 4 | !wod 3 x 4" +
                "\n***Modifiers***: [x], [spec], [#]\n[x] - Exploding Tens. Rolling a 10 adds a free extra dice.\n[spec] - Specialization roll. Tens are worth two successes." +
                "\n[#] - Difficulty. Optional. Any number past the first is taken for DC.",
                "**!ep <Target> [Modifiers] | !ep m <Dice Amount>**\nEclipse Phase guidelines dice. Two options, one rolls against a target level, the other rolls multidice." +
                "\n\n***Examples***: !ep m 5 | !ep 45 | !ep 34 -10 | !ep 56 simple\n***Modifiers***: [m], [+/-##], [difficulty], [severity]\n[m] - Enables multidice rolls. Must come directly after !ep. " +
                "Accepts no other modifiers.\n[+/-##] - Adds/Subtracts from the target number.\n[difficulty] - Options are: effortless(+30), simple(+20), easy(+10), difficult(-10), challenging(-20), hard(-30)." +
                "\n[severity] - Options are: minor(+/-10), moderate(+/-20), major(+/-30)"};
            var pagiOpt = new PaginatedAppearanceOptions() { Timeout = TimeSpan.FromMinutes(2), JumpDisplayOptions = 0 };
            var testTask = new PaginatedMessage() { Color = new Color(0xC16BF2), Title = "<> are required items | [] are optional items.", Content = "***Roll Commands:***", Pages = pages, Options = pagiOpt };
            await PagedReplyAsync(testTask);
            *//*
        }

        [Command("ref dnd")]
        [Summary("Dungeons and Dragons 5E References.")]
        public async Task CsDnd([Remainder]string input = "")
        {
            var Items = await Context.Channel.GetMessagesAsync().FlattenAsync();
            var usermessages = Items.Where(x => x.Author == Context.Message.Author as SocketGuildUser).Take(1);
            var delmsg = usermessages.First();
            await Context.Channel.DeleteMessageAsync(delmsg);
            var builder = new EmbedBuilder()
                        .WithTitle("Dungeons and Dragons 5E References: ")
                        .WithColor(new Color(0xC16BF2))
                        .AddField("General", "[DM Screen](https://drive.google.com/open?id=10IfazUdKkPPZp3sp5QzQMAcAJJ97x3V1)\n[Player Reference Sheet (Long)](https://drive.google.com/open?id=1xLaqE8T6-AL9ytc1Lww6Db4WJtCYYgAP)" +
                        "\n[Combat Sheet](https://drive.google.com/open?id=1RGjQrdrZTv20nOE_VI9ftXi-ZjJZFa2F)\n[Player Reference (IMG)](https://drive.google.com/open?id=1DMAqh6aiIBzhdH2gGnCVmNYG3qcge6Dc)")
                        .AddField("Character Sheet", "[TWC Fillable Character Sheet - V1](https://drive.google.com/open?id=0BxpJzB_orMORNFVZdGNhZVdhQjg)")
                        .AddField("DnD 5E Folder", "[Drive Folder](https://drive.google.com/drive/folders/0BxpJzB_orMORM3Rsd2E3UXZhZWs?usp=sharing)")
                        .AddField("Utility Sites", "[Dice to Distribution (And the Killable Zone)](http://www.d8uv.org/dice-to-distribution/)\n[Eigengrau's Essential Establishment Generator](http://www.philome.la/rhyscgray/eigengraus-essential-establishment-generator010/play)" +
                        "\n[Massive DM Toolkit](https://www.reddit.com/r/DnDBehindTheScreen/comments/7nqfgh/massive_dms_toolkit_online_resources/)")
                        .WithFooter("This message will auto-delete after 2 minutes.");
            if (input.Contains("perm")) { builder.WithFooter("Message set to not delete."); }
            var embed = builder.Build();
            if (input.Contains("perm")) { await ReplyAsync("", false, embed).ConfigureAwait(false); }
            else { await ReplyAsync("", false, embed).ConfigureAwait(false); }
        }*/
        /*
        [Command("help sr")]
        [Summary("Help for shadowrun command.")]
        public async Task HelpSrRoll()
        {
            var builder = new EmbedBuilder()
                        .WithTitle("Special Shadowrun Commands: ")
                        .WithDescription("<> are required items | [] are optional items.")
                        .WithColor(new Color(0xC16BF2))
                        .AddField("Extended Test - !sr <#> et <keyword>", "Creates/Modifies an extended test roll. Rolled exactly like normal dice with ET and a keyword on the end.\nEx: !sr 3 et sprite | !sr 5 x et gunbuild")
                        .AddField("Extended Test Report - !sr report [keyword]", "Displays roll status of chosen keyword. If blank, lists all available keywords.\nEx: !sr report | !sr report sprite")
                        .AddField("Extended Test Delete - !sr del [keyword]", "Deletes extended test information. If blank, lists all available keywords. \nEx: !sr del | !sr del gunbuild")
                        .WithFooter("This message will auto-delete after 2 minutes.");
            var embed = builder.Build();
            await ReplyAndDeleteAsync("", false, embed, TimeSpan.FromMinutes(2)).ConfigureAwait(false);
            var Items = await Context.Channel.GetMessagesAsync().Flatten();
            var usermessages = Items.Where(x => x.Author == Context.Message.Author as SocketGuildUser).Take(1);
            await Context.Channel.DeleteMessagesAsync(usermessages);
        }

        [Command("help time")]
        [Summary("Help for time command. Works for both time and timeintl.")]
        public async Task HelpTime()
        {
            var builder = new EmbedBuilder()
                .WithTitle("Time Commands: ")
                .WithDescription("<> are required items | [] are optional items.")
                .WithColor(new Color(0xC16BF2))
                .AddField("!time", "Displays the current time in US timezones.")
                .AddField("!time <hour>[:minute] <pm/am> <timezone>", "Displays the requested time in US timezones.\n***Examples***: !time 2 pm pst | !time 3:45 am cst\nValid timezones: hst, akst, pst, mst, cst, est.")
                .AddField("!timeintl", "Displays the current time in major cities around the globe.")
                .AddField("!timeintl <hour>[:minute] <pm/am> <timezone>", "Displays the requested time in (certain) global timezones. This is not yet implemented.")
                .AddField("!timer <#><h/m> [here/everyone/self] | !alarm", "Sets an alarm that pings @here or @everyone.\n***Examples***: !timer 2h everyone | !alarm 5m here\nDefaults to here if unspecified or unrecognized.")
                .WithFooter("This message will auto-delete after 2 minutes.");
            var embed = builder.Build();
            await ReplyAndDeleteAsync("", false, embed, TimeSpan.FromMinutes(2)).ConfigureAwait(false);
            var Items = await Context.Channel.GetMessagesAsync().Flatten();
            var usermessages = Items.Where(x => x.Author == Context.Message.Author as SocketGuildUser).Take(1);
            await Context.Channel.DeleteMessagesAsync(usermessages);
        }

        [Command("fullhelp")]
        [Summary("Every command option. Authorized users only.")]
        public async Task FullHelp()
        {
            ConfigService cred = new ConfigService();
            string user = Context.User.ToString();
            if (user == cred.BotOwner)
            {
                await ReplyAsync(
                "`Authorized User.`\n" +
                "```Full Command List:```" +
                "`!roll, !r, !sr, !wod, !ep\n" +
                "!sr # [mods] et <name> | !sr report <name> !sr del <name>\n" +
                "!rs, !rollstats\n" +
                //"!report, !reportwipe\n" +
                "!flip, !flip heads/tails\n" +
                "!ud\n" +
                "!time, !timeintl\n" +
                "!boop, !boop @user, !boop user\n" +
                "!timer\n" +
                "!help, !fullhelp, !help roll, !help time\n" +
                "!invite\n" +
                //"!ping\n" +
                //"!play, !join\n" +
                "!debug\n" +
                "!scree`"
                );
                var Items = await Context.Channel.GetMessagesAsync().Flatten();
                var usermessages = Items.Where(x => x.Author == Context.Message.Author as SocketGuildUser).Take(1);
                await Context.Channel.DeleteMessagesAsync(usermessages);
            }
            else
            {
                await ReplyAsync("`Must be bot owner.`");
            }
        }

        [Command("invite")]
        [Summary("Display invite code for the bot to add to guilds.")]
        public async Task InviteMe()
        {
            var builder = new EmbedBuilder()
                .WithTitle("Invite Me!")
                .WithColor(new Color(0xC16BF2))
                .WithDescription("Use this code to invite Ievora to a server you are an admin of!\n" +
                "Uncheck whatever permissions you want, but that might break some functionality.\n" +
                "[Invite Link!](https://discordapp.com/oauth2/authorize?client_id=269953683217842178&scope=bot&permissions=37219392)");
            var embed = builder.Build();
            await ReplyAsync("", false, embed)
                .ConfigureAwait(false);
            var Items = await Context.Channel.GetMessagesAsync().Flatten();
            var usermessages = Items.Where(x => x.Author == Context.Message.Author as SocketGuildUser).Take(1);
            await Context.Channel.DeleteMessagesAsync(usermessages);
        }
        
    }
}
        */
