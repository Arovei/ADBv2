﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Discord;
using Discord.Audio;
using Discord.Commands;
using Discord.WebSocket;
using Interactivity;
using ADBv2.Services;
using ADBv2.Data;

// https://discord.foxbot.me/docs/guides/voice/sending-voice.html
// https://gist.github.com/Joe4evr/773d3ce6cc10dbea6924d59bbfa3c62a
// https://stackoverflow.com/questions/39877884/c-sharp-download-the-sound-of-a-youtube-video

namespace ADBv2.Modules
{
    //public class AudioModule : ModuleBase<ICommandContext>
    /*public class AudioModule
    {
        private readonly AudioService _service;

        // Remember to add an instance of the AudioService
        // to your IServiceCollection when you initialize your bot
        public AudioModule(AudioService service)
        {
            _service = service;
        }

        // You *MUST* mark these commands with 'RunMode.Async'
        // otherwise the bot will not respond until the Task times out.
        [Command("join", RunMode = RunMode.Async)]
        [Summary("Connect to a voice channel.")]
        public async Task JoinCmd()
        {
            await _service.JoinAudio(Context.Guild, (Context.User as IVoiceState).VoiceChannel);
        }

        [Command("leave", RunMode = RunMode.Async)]
        public async Task LeaveCmd()
        {
            await _service.LeaveAudio(Context.Guild);
        }

        [Command("play", RunMode = RunMode.Async)]
        public async Task PlayCmd([Remainder] string song = null)
        {
            if (song == null)
            {
                song = await FindSong();
                if (song == null) { return; }
            }
            if (song.Contains("youtube"))
            {
                await ReplyAsync("`Prepping song...`");

            }
            if (DiceMath.IsInt(song))
            {
                int i = 0;
                Int32.TryParse(song.ToString(), out i);
                song = await FindSong(i);
            }
            await LeaveCmd();
            await JoinCmd();
            await ReplyAsync($"Playing: {song.Substring(9)}");
            await _service.SendAudioAsync(Context.Guild, Context.Channel, song);
        }

        private async Task<string> FindSong(int i = 0)
        {
            string dir = "D:\\MUSIC\\";
            string song ="";
            List<string> files = new List<string>();

            // Load up the options in the directory
            foreach (string item in Directory.GetFiles(dir)) { if (item.Contains(".mp3")) { files.Add(item); } }
            foreach (string d in Directory.GetDirectories(dir))
            {
                foreach (string f in Directory.GetFiles(d))
                {
                    if (f.Contains(".mp3")) { files.Add(f); }
                    if (f.Contains(".wav")) { files.Add(f); }
                }
                foreach (string di in Directory.GetDirectories(d))
                {
                    foreach (string fi in Directory.GetFiles(di))
                    {
                        if (fi.Contains(".mp3")) { files.Add(fi); }
                        if (fi.Contains(".wav")) { files.Add(fi); }
                    }
                }
            }

            if (i != 0)
            {
                song = files[i - 1];
                return song;
            }
            //var Items = await Context.Channel.GetMessagesAsync().Flatten();
            //var usermessages = Items.Where(x => x.Author == Context.Message.Author as SocketGuildUser).Take(1);
            //await Context.Channel.DeleteMessagesAsync(usermessages);
            List<string> pages = new List<string>();
            int j = 0;
            int blockSize = 15;
            var group = files.Select((x, index) => new { x, index })
                               .GroupBy(x => x.index / blockSize, y => y.x);
            foreach (var batch in group)
            {
                pages.Add("");
                foreach (var item in batch)
                {
                    pages[i] = pages[i] + $"{j + 1}. {item.Substring(9)}\n";
                    j++;
                }
                i++;
            }
            var pagiOpt = new PaginatedAppearanceOptions() { Timeout = TimeSpan.FromMinutes(5), JumpDisplayOptions = 0 };
            var testTask = new PaginatedMessage() { Color = new Color(0xC16BF2), Title = "Please choose a number [5 minute timeout]:", Content = "*If you know the number you don't need to go to the page.*", Pages = pages, Options = pagiOpt };
            await PagedReplyAsync(testTask);
            var response = await NextMessageAsync(true, true, TimeSpan.FromMinutes(5));
            if (Int32.TryParse(response.ToString(), out i)) { song = files[i - 1]; return song; }
            else { await ReplyAsync("`Non-numeric input.`"); return null; }
        }
    }
    */
}