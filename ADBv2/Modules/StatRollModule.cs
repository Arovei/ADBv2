﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Discord.Commands;
using ADBv2.Data;

namespace ADBv2.Modules
{
    [Name("Roll Stats")]
    [Summary("Roll stats for DnD characters.")]
    public class StatRollModule : ModuleBase<SocketCommandContext>
    {
        [Command("rs"), Alias("rollstats")]
        [Summary("Roll stats for DnD characters.")]
        public async Task StatRoll([Remainder]string input = null)
        {
            if (input == null) { input = "3d6"; await ReplyAsync("Defaulting to 3d6."); }
            int styleType = 0;
            string styleWord = "";
            if (input.Contains("3d6"))
            {
                styleType = 0;
                styleWord = "3d6";
            }
            else if (input.Contains("4d6"))
            {
                styleType = 1;
                styleWord = "4d6 - 1d6";
            }
            else if (input.Contains("d20"))
            {
                styleType = 2;
                styleWord = "d20";
            }
            else
            {
                await ReplyAsync("`Defaulting to 3d6.`");
                styleType = 0;
                styleWord = "3d6";
            }
            if (input.Contains("help"))
            {
                await ReplyAsync("`Roll types: 3d6, 4d6, d20.\nEx: !rollstats 4d6`");
            }
            else
            {
                List<int> stats = StatsRoll.RollStats(styleType);
                await ReplyAsync("`Roll results for " + styleWord + ":\nSTR: " + stats[0] + "\nDEX: "
                    + stats[1] + "\nCON: " + stats[2] + "\nINT: " + stats[3] + "\nWIS: " + stats[4]
                    + "\nCHA: " + stats[5] + "\nTotal points: " + stats[6] + "`");
            }
        }
    }
}
