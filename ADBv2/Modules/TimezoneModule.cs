﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using GeoTimeZone;
using TimeZoneConverter;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using ADBv2.Services;
using ADBv2.Data;
using System.Data;
using System.Timers;
using System.IO;
//using Discord.Interactions;

namespace ADBv2.Modules
{
    [Name("Timezone")]
    [Discord.Commands.Summary("Get timezone information/Set timer.")]
    public class TimezoneModule : ModuleBase<SocketCommandContext>
    {
        [Command("time")]
        [Discord.Commands.Summary("Get current time in different zones.")]
        public async Task timeUS([Remainder] string input = null)
        {
            DateTime timeToConvert;
            // By default, if no specific time is requestd, do it for whatever time it is right now.
            timeToConvert = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now);

            // Modifyable list of US Timezones to check for
            List<string> tzc = TimeZoneCheckUS();
            List<TimeZoneInfo> zones = new List<TimeZoneInfo>();
            List<TimeZoneInfo> zUsed = new List<TimeZoneInfo>();
            // Put alllll the zones in a list.
            foreach (var z in TimeZoneInfo.GetSystemTimeZones())
            {
                zones.Add(z);
            }
            // Cross reference the list of desired zones and add the actual data to the list of zones we'll convert to.
            foreach (var tz in zones)
            {
                if (tzc.Contains(tz.Id)) { zUsed.Add(tz); }
            }

            //Modifyable list of US Timezone abbreviations
            Dictionary<string, string> tza = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
            tza = TimeZoneAbbreviationUS();
            if (input != null)
            {
                TimeSpan zoneReq = TimeSpan.Zero;
                // Base timezone requested
                string zoneRequest = input.Substring(input.LastIndexOf(' ') + 1);
                // Filter to proper DateTime name matching
                tza.TryGetValue(zoneRequest, out zoneRequest);

                // Remove the last word of input
                input = input.Remove(input.LastIndexOf(' ') + 1);
                // Convert the input to valid DateTime parameters
                bool validInput = DateTime.TryParse(input, out timeToConvert);
                if (!validInput) { await ReplyAsync("`Invalid input. Please see !help time`"); }
                // Find the valid timezone
                foreach (var tz in zUsed) { if (tz.Id.ToString().Contains(zoneRequest)) { zoneReq = tz.BaseUtcOffset; } }
                if (zoneReq == TimeSpan.Zero) { await ReplyAsync("`Invalid timezone. Valid ones are: hst, akst, pst, mst, cst, est`"); }
                // Shift the timezone offset from local time to the requested one
                zoneReq = TimeZoneInfo.Local.BaseUtcOffset - zoneReq;
                timeToConvert += zoneReq;
                // Convert to UTC for output method (which converts from UTC)
                timeToConvert = TimeZoneInfo.ConvertTimeToUtc(timeToConvert);
            }

            var embed = TimezoneBuilder(zUsed, timeToConvert);
            await ReplyAsync("", false, embed).ConfigureAwait(false);
        }

        [Command("timeintl")]
        [Discord.Commands.Summary("Get the times for international cities.")]
        public async Task timeIntl()
        {
            DateTime timeToConvert;
            timeToConvert = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now);

            List<TimeZoneInfo> zones = new List<TimeZoneInfo>();
            List<TimeZoneInfo> zUsed = new List<TimeZoneInfo>();
            List<string> zNames = new List<string>();
            foreach (var z in TimeZoneInfo.GetSystemTimeZones())
            {
                zones.Add(z);
            }
            foreach (var tz in zones)
            {
                // Since we're using city names, we don't pull the timezone name during the display building.
                // London, New York, Shanghai, Berlin, Tokyo, Mumbai, Singapore, Sydney, Moscow
                if (tz.Id == "W. Europe Standard Time") { zUsed.Add(tz); zNames.Add("London (UTC/DST+1)"); }
                else if (tz.Id == "Eastern Standard Time") { zUsed.Add(tz); zNames.Add("New York (UTC-5/DST-4)"); }
                else if (tz.Id == "China Standard Time") { zUsed.Add(tz); zNames.Add("Shanghai (UTC+8)"); }
                else if (tz.Id == "Central European Standard Time") { zUsed.Add(tz); zNames.Add("Berlin (UTC+2)"); }
                else if (tz.Id == "Tokyo Standard Time") { zUsed.Add(tz); zNames.Add("Tokyo (UTC+9)"); }
                else if (tz.Id == "India Standard Time") { zUsed.Add(tz); zNames.Add("Mumbai (UTC+5:30)"); }
                else if (tz.Id == "Singapore Standard Time") { zUsed.Add(tz); zNames.Add("Singapore (UTC+8)"); }
                else if (tz.Id == "E. Australia Standard Time") { zUsed.Add(tz); zNames.Add("Sydney (UTC+10)"); }
                else if (tz.Id == "Russian Standard Time") { zUsed.Add(tz); zNames.Add("Moscow (UTC+3)"); }
            }

            var embed = TimezoneBuilder(zUsed, timeToConvert, zNames);
            await ReplyAsync("", false, embed).ConfigureAwait(false);
        }

        [Command("timein")]
        [Discord.Commands.Summary("Find the current time in a city")]
        public async Task timeIn([Remainder] string city = null)
        {
            DateTime timeToConvert;
            timeToConvert = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now);

            LocationResult locResults = new LocationResult();
            var moreLocResults = locResults.GetLocation(city).Result;
            var result = moreLocResults.Features;

            double _latitude = Convert.ToDouble(result[0].Geometry.Coordinates[1]);
            double _longitude = Convert.ToDouble(result[0].Geometry.Coordinates[0]);

            var tzlookup = TimeZoneLookup.GetTimeZone(_latitude, _longitude).Result;
            var tz = TZConvert.IanaToWindows(tzlookup);

            //List<TimeZoneInfo> zones = new List<TimeZoneInfo>();
            List<TimeZoneInfo> zUsed = new List<TimeZoneInfo>();
            List<string> zNames = new List<string>();
            // Put alllll the zones in a list.
            foreach (var z in TimeZoneInfo.GetSystemTimeZones())
            {
                //zones.Add(z);
                if (z.Id == tz)
                {
                    zUsed.Add(z);
                    var utc = z.BaseUtcOffset.ToString();
                    utc = utc.Substring(0, utc.Length - 3);
                    if (!utc.Contains("-")) { utc = "+" + utc; }
                    zNames.Add($"{tz}, {tzlookup}\n(UTC{utc})");
                }
            }

            var embed = TimezoneBuilder(zUsed, timeToConvert, zNames);
            await ReplyAsync("", false, embed).ConfigureAwait(false);

        }

        /*
        [Command("timer"), Alias("alarm")]
        [Summary("Set an @here, @everyone, or @self timer.")]
        public async Task AnnounceTimer([Remainder]string time = null)
        {
            if (time == null) { await ReplyAsync("Whoops. Looks like you forgot... everything. Better luck next time? *[Error: null input]*"); }
            else
            {
                int delay = 0;
                int hours = 0;
                int minutes = 0;
                bool tooBig = false; // ;)
                string info = "";
                string infoAlert = "self";
                SocketRole role = Context.Guild.EveryoneRole;
                SocketUser user = Context.Message.Author;

                //List<string> inputSplit = new List<string>(input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));

                //foreach (var time in inputSplit)
                {
                    if (time.ToLower().Contains("self")) { infoAlert = "self"; }
                    if (time.ToLower().Contains("here")) { infoAlert = "here"; time = time.Replace("here", " ").Replace("Here", " "); }
                    if (time.ToLower().Contains("everyone")) { infoAlert = "everyone"; }
                    if (time.ToLower().Contains("h"))
                    {
                        var h = Regex.Replace(time, "[^0-9]", "");
                        bool convertInfo = Int32.TryParse(h, out hours);
                        if (convertInfo)
                        {
                            delay += (hours * 3600000);
                            info += (time + " ");
                        }
                        if (!convertInfo) { tooBig = true; }
                    }
                    if (time.ToLower().Contains("m"))
                    {
                        var m = Regex.Replace(time, "[^0-9]", "");
                        bool convertInfo = Int32.TryParse(m, out minutes);
                        if (convertInfo)
                        {
                            delay += (minutes * 60000);
                            info += (time + " ");
                        }
                        if (!convertInfo) { tooBig = true; }
                    }                    
                }
                if (delay == 0 && !tooBig) { await ReplyAsync("Unlike instant noodles, instant timers are kind of an oxymoron. *[Error: Time registered as 0]*"); }
                else if (tooBig) { await ReplyAsync("Max time is a week. *[Error: Time registed as too long]*"); }
                else
                {
                    await ReplyAsync("Timer set for " + info + "duration and to alert " + infoAlert + ".");
                    await Task.Delay(delay);

                    if (infoAlert.Equals("everyone")) { await ReplyAsync($"Alarm is up for {role}! Set by {Context.User.Mention} {info}ago!"); }
                    else if (infoAlert.Equals("here")) { await ReplyAsync($"Alarm is up for @here! Set by {Context.User.Mention} {info}ago!"); }
                    else if (infoAlert.Equals("self")) { await ReplyAsync($"Alarm is up for {user.Username}! Set by {Context.User.Mention} {info}ago!"); }
                }                
            }
        }
        */
        //[DefaultMemberPermissions(GuildPermission.ManageRoles)]
        [Command("pingroledisable"), Alias("prd")]
        [Discord.Commands.Summary("Add option to disable pinging listed rolls for an hour")]
        public async Task PingRoleDisable([Remainder] string input = "")
        {
            var msger = Context.User as SocketGuildUser;
            if (msger.GuildPermissions.ManageRoles == false)
            {
                await ReplyAsync("`Insufficent Permissions.`");
                return;
            }
            List<string> result = new();
            string outputDisplay = "";
            if (input.Contains("add "))
            {
                Console.WriteLine("Beginning add fuction of role ping...");
                //await ReplyAsync("`Command add fired.`");
                result = ReportFile.PingCooldown("add", Context);
                foreach (var item in result) { outputDisplay += "\n" + item; }
                if (outputDisplay == "\n") { outputDisplay = "Failure; Unknown error"; }
                await ReplyAsync($"`Command add result: {outputDisplay}`");
            }
            else if (input.Contains("remove "))
            {
                Console.WriteLine("Beginning remove fuction of role ping...");
                //await ReplyAsync("`Command remove fired.`");
                await ReplyAsync("`Reminder: Mention status is left as is. You need to manually change it if desired.`");
                result = ReportFile.PingCooldown("remove", Context);
                foreach (var item in result) { outputDisplay += "\n" + item; }
                if (outputDisplay == "\n") { outputDisplay = "Failure; Unknown error"; }
                await ReplyAsync($"`Command remove result: {outputDisplay}`");
            }
            else
            {
                await ReplyAsync("`Use \"!prd add @role / !prd remove @role\" to add or remove putting an hour cooldown on mentioning a role.`");
                await ReplyAsync("`This will not remember the current mention status of a role before adding, so if you remove the role, it is left as-is.`");
            }
        }

        //[Command("pingrole"), Alias("pr")]
        //[Summary("Ping a role and have an associated cooldown.")]
        public static async Task RolePing(SocketCommandContext context)
        {
            //Console.WriteLine("Interfacing with RolePing...");
            //This one checks to see if it should disable the role and set the timer.
            /*var cred = new ConfigService();
            if (context.Guild.Id.ToString() != cred.RolePingDisable) 
            { 
                Console.WriteLine("Guild is not applicable.");
                Console.WriteLine("Current guild ID: " + context.Guild.Id.ToString());
                Console.WriteLine("Requied guild ID: " + cred.RolePingDisable);
                return; 
            }
            */
            List<SocketRole> rolesToPing = new List<SocketRole>();
            //Need to check if the role is available to ping disable
            if (context.Message.MentionedRoles.Count < 1) { return; }
            Console.WriteLine("List of roles found in message: ");
            foreach (SocketRole role in context.Message.MentionedRoles)
            {
                string result = ReportFile.ValidRole(role.Id.ToString(), context.Guild.Id.ToString());
                if (result == "valid")
                {
                    rolesToPing.Add(role);
                    Console.WriteLine($"  Valid role: {role.Name} ({role.Id})");
                }
                else { Console.WriteLine($"Invalid role: {role.Name} ({role.Id})"); }
            }
            foreach (SocketRole rtp in rolesToPing)
            {
                //Console.WriteLine($"Checking ping timer status for role {rtp.Name}...");
                ReportFile.PingCooldown("", context); //Add the timer to the role
                if (rtp.IsMentionable == true)
                {
                    await DeMention(rtp);
                    Console.WriteLine("Setting role: " + rtp.Name + " in guild " + context.Guild.Name + " to unmentionable.");
                    //await context.Channel.SendMessageAsync("test");
                    //await rtp.ModifyAsync(x => { x.Mentionable = false; });
                }                
                //await ReplyAsync(rtp.Mention, false).ConfigureAwait(false);
            }
        }
        public static async Task DeMention(SocketRole rtp)
            => await rtp.ModifyAsync(role =>  role.Mentionable = false);
        public static async Task CheckPingCooldown(DiscordSocketClient discord)
        {
            string toWrite = "";
            string fileName = "PingCooldown.txt";
            string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PingCooldown", fileName);
            Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PingCooldown"));

            if (File.Exists(destPath))
            {
                var doc = File.ReadAllText(destPath);
                char[] delimiters = { '|' };
                List<string> inputSplit = new List<string>(doc.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
                //fixed i hope
                for (int i = 0; i < inputSplit.Count; i++)
                {
                    //Both of these parsing means the third line is the time
                    if (!ulong.TryParse(inputSplit[i], out ulong guild)) { continue; }
                    if (!ulong.TryParse(inputSplit[i + 1], out ulong rtp)) { continue; }
                    var savedTime = DateTime.Parse(inputSplit[i + 2]);
                    var timeDiff = (DateTime.Now - savedTime).TotalMinutes;
                    Console.WriteLine($"[{inputSplit[i+3]}][{inputSplit[i+4]}]Time difference in minutes: {timeDiff}");
                    if (timeDiff > 57)
                    {
                        foreach (var inGuild in discord.Guilds)
                        {
                            if (inGuild.Id == guild)
                            {
                                foreach (var inRole in inGuild.Roles)
                                {
                                    //Console.WriteLine($"Checking if {inRole.Id} matches {rtp}.");
                                    if (inRole.Id == rtp)
                                    {
                                        //Console.WriteLine("Match found.");
                                        await inRole.ModifyAsync(x => { x.Mentionable = true; });
                                        Console.WriteLine($"Role {inRole.Name} in guild {inGuild.Name} is now mentionable again.");
                                        //now remove the timer since its served its purpose
                                        //inputSplit[0](message.Guild.Id.ToString());
                                        //inputSplit[1](roleName.Id.ToString());
                                        //inputSplit[2](DateTime.Now.ToString());
                                        //inputSplit[3](message.Guild.Name.ToString());
                                        //inputSplit[4](roleName.Name.ToString());
                                        inputSplit.RemoveAt(i + 4);
                                        inputSplit.RemoveAt(i + 3);
                                        inputSplit.RemoveAt(i + 2);
                                        inputSplit.RemoveAt(i + 1);
                                        inputSplit.RemoveAt(i);
                                        i--;
                                    }
                                }
                            }
                        }
                    }
                }
                foreach (var text in inputSplit) { toWrite += text + "|"; }
                File.WriteAllText(destPath, toWrite);
            }
        }

        private Embed TimezoneBuilder(List<TimeZoneInfo> zUsed, DateTime timeToConvert, List<string> zNames = null)
        {
            var builder = new EmbedBuilder()
                .WithColor(new Color(0x4ADABB));

            // If no city names are passed, then...
            if (zNames == null)
            {
                // Add timezone names and the time conversion requeted to the embed.
                foreach (var zone in zUsed)
                {
                    string s = (zone.StandardName.ToString());
                    // Remove "Standard Time" from each name. It gets a bit long and ugly using inline fields.
                    string zoneName = s.Substring(0, s.IndexOf(" Standard"));
                    builder.AddField(zoneName, (TimeZoneInfo.ConvertTimeFromUtc(timeToConvert, zone)).ToShortTimeString(), inline: true);
                }
            }
            else
            {
                // Input the city names and do the time conversions!
                for (int i = 0; i < zUsed.Count; i++)
                {
                    builder.AddField(zNames[i], (TimeZoneInfo.ConvertTimeFromUtc(timeToConvert, zUsed[i])).ToShortTimeString(), inline: true);
                }
            }

            var embed = builder.Build();
            return embed;
        }

        public Dictionary<string, string> TimeZoneAbbreviationUS()
        {
            Dictionary<string, string> TimeZoneAbbr = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

            TimeZoneAbbr.Add("akst", "Alaskan Standard Time");
            TimeZoneAbbr.Add("hst", "Hawaiian Standard Time");
            TimeZoneAbbr.Add("mst", "Mountain Standard Time");
            TimeZoneAbbr.Add("cst", "Central Standard Time");
            TimeZoneAbbr.Add("est", "Eastern Standard Time");
            TimeZoneAbbr.Add("pst", "Pacific Standard Time");

            return TimeZoneAbbr;
        }

        public List<string> TimeZoneCheckUS()
        {
            List<string> TimeZoneCheck = new List<string>();

            TimeZoneCheck.Add("Alaskan Standard Time");
            TimeZoneCheck.Add("Hawaiian Standard Time");
            TimeZoneCheck.Add("Mountain Standard Time");
            TimeZoneCheck.Add("Central Standard Time");
            TimeZoneCheck.Add("Eastern Standard Time");
            TimeZoneCheck.Add("Pacific Standard Time");

            return TimeZoneCheck;
        }

        /*
         * Currently useless since I use the names of cities, and those are stored... nowhere. Derp.
         * That means I need to either make a dictionary for this or... something else, idfk.
        public List<string> TimeZoneCheckIntl()
        {
            List<string> TimeZoneCheck = new List<string>();

            TimeZoneCheck.Add("W. Europe Standard Time");
            TimeZoneCheck.Add("Eastern Standard Time");
            TimeZoneCheck.Add("China Standard Time");
            TimeZoneCheck.Add("Central European Standard Time");
            TimeZoneCheck.Add("Tokyo Standard Time");
            TimeZoneCheck.Add("India Standard Time");
            TimeZoneCheck.Add("Singapore Standard Time");
            TimeZoneCheck.Add("E. Australia Standard Time");
            TimeZoneCheck.Add("Russian Standard Time");

            return TimeZoneCheck;
        }
        */
    }
}