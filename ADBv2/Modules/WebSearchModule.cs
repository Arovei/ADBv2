﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Discord;
using Discord.Commands;
using Interactivity;
using ADBv2.Services;
using ADBv2.Data;

namespace ADBv2.Modules
{
    public class WebSearchModule : ModuleBase<SocketCommandContext>
    {
        [Command("google")]
        [Summary("Do a google web search.")]
        public async Task WebSearchGoogle([Remainder]string input = null)
        {
            if (input == null) { await ReplyAsync("`Requires search query.`"); }
            else
            {
                await DoTheSearch(input, "google");
            }
        }

        [Command("ddg")]
        [Summary("Do a DuckDuckGo web search.")]
        public async Task WebSearchDdg([Remainder]string input = null)
        {
            if (input == null) { await ReplyAsync("`Requires search query.`"); }
            else
            {
                await DoTheSearch(input, "ddg");
            }
        }

        // Current engine options: ddg, google [needs API key]
        public async Task DoTheSearch(string query, string engine)
        {
            // Amount of reults to pull and display
            int amountOfResults = 5;
            string[] pages = new string[amountOfResults];
            WebSearchResult SearchResults = new WebSearchResult();
            if (engine == "google")
            {
                WebSearchObj results = SearchResults.QueryByTerm(query, engine).Result;
                if (results == null) { await ReplyAsync("`Google Search: invalid API or not set up.`"); return; }

                List<string> title = new List<string>();
                List<string> link = new List<string>();
                List<string> snippet = new List<string>();

                var builder = new EmbedBuilder()
                        .WithColor(new Color(0xC16BF2));
                for (int i = 0; i < amountOfResults; i++)
                {
                    if (results.Items[i].Title != "")
                    {
                        title.Add(results.Items[i].Title);
                        link.Add(results.Items[i].Link);
                        snippet.Add(results.Items[i].Snippet);
                        builder.AddField(title[i], link[i] + "\n" + snippet[i]);
                    }
                }
                var footer = new EmbedFooterBuilder();
                footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
                if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                //footer.Text = "Google results for " + (((IGuildUser)Context.Message.Author).Nickname);
                //if (footer.Text == "Google results for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                footer.IconUrl = "https://i.imgur.com/vZvdsOF.png";
                builder.Footer = footer;
                var embed = builder.Build();
                await ReplyAsync("", false, embed);
            }
            else if (engine == "ddg")
            {
                WebSearchObjDdg results = SearchResults.QueryByTermDdg(query).Result;

                List<string> title = new List<string>();
                List<string> link = new List<string>();
                var queryContent = string.Format(Uri.EscapeDataString(query));

                if (results.Heading == "") { await ReplyAsync($"`DuckDuckGo Instant Answer result unavailable for search term: {query}`\nActual DuckDuckGo search: https://duckduckgo.com/{queryContent}"); return; }

                title.Add(results.Heading);
                link.Add(results.AbstractUrl);

                var builder = new EmbedBuilder()
                        .WithTitle(title[0])
                        .WithDescription(link[0])
                        .WithColor(new Color(0xC16BF2));
                for (int i = 1; i < amountOfResults - 1; i++)
                {
                    if (results.RelatedTopics[i].Text != null)
                    {
                        title.Add(results.RelatedTopics[i].Text);
                        link.Add(results.RelatedTopics[i].FirstUrl);
                        builder.AddField(title[i], link[i]);
                    }                    
                }
                var footer = new EmbedFooterBuilder();
                footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
                if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                //footer.Text = "DuckDuckGo results for " + (((IGuildUser)Context.Message.Author).Nickname);
                //if (footer.Text == "DuckDuckGo results for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
                footer.IconUrl = "https://i.imgur.com/ZPhDMLq.png";
                builder.Footer = footer;
                var embed = builder.Build();
                await ReplyAsync($"Actual DuckDuckGo search: https://duckduckgo.com/{queryContent} \nInstant answers search below.", false, embed);
            }
            else { await ReplyAsync("`Invalid search engine call.`"); return; }
        }
    }
}