﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using System.Net.Http;
using ADBv2.Data;

namespace ADBv2.Modules
{
    [Name("Urban Dictionary")]
    [Summary("Get definition from Urban Dictionary.")]
    public class UrbanDictModule : ModuleBase<SocketCommandContext>
    {
        [Command("ud")]
        [Summary("Get definition from Urban Dictionary.")]
        public async Task UrbanDict([Remainder]string input = null)
        {
            if (input == null)
            {
                //This needs a smartass answer for a smartass request! Still gonna be a smartass if you forgot to put the word, idc.
                input = "null";
                //await message.Channel.SendMessageAsync((message.Author).Mention + ": What good is searching for nothing going to do you?");
            }

            List<string> definitions = new List<string>();
            List<string> examples = new List<string>();
            UrbanDefine scan = new UrbanDefine();
            bool def2 = true;
            bool def3 = true;
            var defUrl = "https://www.urbandictionary.com/define.php?term=";
            var result = scan.QueryByTerm(input).Result;
            Uri link = new Uri(defUrl + Uri.EscapeDataString(input));

            if (result.ItemList.Count == 0)
            {
                await ReplyAsync(Context.User.Mention + ": Not defined in Urban Dictionary.");
                return;
            }
            for (int i = 0; i < result.ItemList.Count; i++)
            {
                definitions.Add(result.ItemList[i].Definition);
                examples.Add(result.ItemList[i].Example);
            }
            if (definitions[0].Length >= 1020)
            {
                string linkstring = link.ToString().Replace(" ", "%20");
                definitions[0] = definitions[0].Substring(0, 1000) + "...";
                var builderError = new EmbedBuilder()
                    .WithTitle("Definition to long for Discord Message.")
                    .WithColor(new Color(0xC16BF2))
                    .WithThumbnailUrl("")
                    .WithDescription($"Please follow the link to the website for full definition:\n{linkstring}.")
                    .WithAuthor(author => {
                        author
                            .WithName("Urban Dictionary")
                            .WithUrl("https://www.urbandictionary.com/")
                            .WithIconUrl("https://i.imgur.com/71KJUzy.png");
                    })
                    .AddField("Partial 1:", definitions[0]);
                var embedError = builderError.Build();
                await ReplyAsync("", false, embedError)
                    .ConfigureAwait(false);
                return;
            }

            if (definitions.Count >= 3)
            {
                if (((definitions[0].Length + definitions[1].Length + definitions[2].Length) >= 2000) && (definitions[2].Length > 1000)) { def3 = false; }
            }
            if (definitions.Count == 2)
            {
                if (((definitions[0].Length + definitions[1].Length) >= 2000) && (definitions[1].Length > 1000)) { def2 = false; }
                definitions.Add(null);
            }
            if (definitions.Count == 1) { definitions.Add(null); definitions.Add(null); }

            // maybe use try;catch; with HttpException later?            
            var builder = new EmbedBuilder()
                    .WithTitle("Top three definitions for: " + input)
                    .WithColor(new Color(0xC16BF2))
                    .WithThumbnailUrl("https://i.imgur.com/XTq2SX1.png")
                    .WithAuthor(author => {
                        author
                            .WithName("Urban Dictionary")
                            .WithUrl("https://www.urbandictionary.com/define.php?term="+ Uri.EscapeDataString(input))
                            .WithIconUrl("https://i.imgur.com/71KJUzy.png");
                    })
                    .AddField("1:", definitions[0]);
            if ((definitions[1] != null) && (def2 == true)) builder.AddField("2:", definitions[1]);
            if ((definitions[2] != null) && (def3 == true)) builder.AddField("3:", definitions[2]);
            var footer = new EmbedFooterBuilder();
            footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
            if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username; }
            builder.Footer = footer;
            var embed = builder.Build();
            await ReplyAsync("", false, embed)
                .ConfigureAwait(false);
        }
    }
}
