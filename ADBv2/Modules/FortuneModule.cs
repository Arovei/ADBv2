﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Interactivity;
using ADBv2.Data;
/*
namespace ADBv2.Modules
{
    [Name("")]
    [Summary("Generates a random fortune cookie")]
    public class FortuneModule : ModuleBase<SocketCommandContext>
    {
        [Command("fortune")]
        [Summary("Call the fortune.")]
        public async Task Fortune([Remainder]string input = null)
        {
            FortuneResults fr = new FortuneResults();

            bool validInput = false;
            var validOpts = new List<string> { "all", "bible", "computers", "cookie", "definitions", "miscellaneous", "misc", "people", "platitudes", "politics", "science", "wisdom" };
            if (validOpts.Contains(input, StringComparer.OrdinalIgnoreCase))
            {
                validInput = true;
                if (input == "misc") { input = "miscellaneous"; }
                Console.WriteLine("Valid Fortune option: " + input);
            }
            else { input = "cookie"; }

            var result = fr.GetFortune(input);
            var fortune = result.Result.fortune;

            var embed = Builder(fortune, input, validInput);

            await ReplyAsync("", false, embed).ConfigureAwait(false);
        }

        private Embed Builder(string fortune, string request, bool valid)
        {
            var builder = new EmbedBuilder()
                .AddField($"Your {request} fortune!", fortune, inline: true)
                .WithColor(new Color(0x4ADABB));
            var footer = new EmbedFooterBuilder();
            footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username;
            if (footer.Text == "Result for ") { footer.Text = $"Result for {((IGuildUser)Context.Message.Author).Username} | From yerkee.com database."; }
            builder.Footer = footer;
            
            var embed = builder.Build();
            return embed;
        }
    }    
}*/
