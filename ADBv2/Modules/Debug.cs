﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Discord;
using Discord.Commands;
//using Discord.Interactions;
using ADBv2.Services;

namespace ADBv2.Modules
{
    [Name("Debug")]
    [Summary("Debug console info")]
    public class DebugModule : ModuleBase<SocketCommandContext>
    {
        [Command("debug")]
        [Summary("Debug console info.")]
        public async Task Debug()
        {
            //Credentials cred = new Credentials();
            ConfigService cred = new ConfigService();
            string user = Context.User.Id.ToString();
            if (user == cred.BotOwner)
            {
                Console.WriteLine(user + " matches authorized user.");
            }
            else
            {
                Console.WriteLine(user + " is not an authorized user.");
            }

            string nickname = ((IGuildUser)Context.Message.Author).Nickname;
            await ReplyAsync("`" + Context.User.Username + ": Debug launched. Check console.`");
            Console.WriteLine("User.ID = " + Context.User.Id);
            Console.WriteLine("User.Username = " + Context.User.Username);
            Console.WriteLine("User.ToString() = " + Context.User.ToString());
            Console.WriteLine("Nickname = " + nickname);
            Console.WriteLine("Context.Client.Guilds");
            Console.WriteLine("Currently in " + Context.Client.Guilds.Count + " guilds. They are: ");
            List<string> serverList = new List<string>();
            string serverDisplay = "";
            foreach (var guild in Context.Client.Guilds)
            {
                Console.WriteLine($"{guild.Id} - {guild}");
                
                if (user == cred.BotOwner)
                {
                    serverList.Add($"{guild.Id} - {guild}");
                }
            }
            if (user == cred.BotOwner)
            {
                foreach (var server in serverList)
                {
                    serverDisplay += (server + "\n");
                }
                await ReplyAsync(serverDisplay);
            }
        }
    }
}
