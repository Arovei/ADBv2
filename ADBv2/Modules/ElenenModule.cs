﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Interactivity;
using ADBv2.Data;
using ADBv2.Services;
using Interactivity.Selection;

/*
namespace ADBv2.Modules
{
    [Name("Elenen Commands")]
    [Summary("Commands for tabletop game world Elenen.")]
    public class ElenenModule : ModuleBase<SocketCommandContext>
    {
        private readonly InteractivityService _interactivity;
        public ElenenModule(InteractivityService interactivity)
        {
            _interactivity = interactivity;
        }
        /*
        [Command("select")]
        public async Task ExampleReactionSelectionAsync()
        {
            var builder = new ReactionSelectionBuilder<string>()
                .WithSelectables(new Dictionary<IEmote, string>()
                {
                    [new Emoji("\u2139")] = "Cyberpunk Red / Shadowrun",
                    [new Emoji("\u2194")] = "Mutants & Masterminds",
                    [new Emoji("\u2195")] = "Pathfinder / 5E",
                    [new Emoji("\u2196")] = "Starfinder / Star Trek / Star Wars",
                    [new Emoji("\u2197")] = "Wod / Exalted",
                    [new Emoji("\u2198")] = "Eclipse Phase",
                    [new Emoji("\u2199")] = "Legend of the Five Rings / Heroes at Ogre's Gate (Wuxia)",
                    [new Emoji("\u21A9")] = "Rifts / GURPS",
                    [new Emoji("\u21AA")] = "TriStat / BESM",
                    [new Emoji("\u25FB")] = "Lancer",
                    [new Emoji("\u25FC")] = "13th Age",
                    [new Emoji("\uD83D\uDD35")] = "Powered by the Apocalypse / Rules Light",
                    [new Emoji("\uD83D\uDD34")] = "Fantasy",
                    [new Emoji("\uD83D\uDD37")] = "Modern",
                    [new Emoji("\uD83D\uDD36")] = "Sci-Fi"
                })
                .WithTitle("Tabletop Game Options")
                .WithUsers(Context.User)
                .WithAllowCancel(false)
                .WithDeletion(DeletionOptions.Invalids);

            var result = await _interactivity.SendSelectionAsync(builder.Build(), Context.Channel, TimeSpan.FromMilliseconds(-1));

            if (result.IsSuccess)
            {
                await Context.Channel.SendMessageAsync(result.Value.ToString());
            }
        }
        */
        /*
        [Command("eh"), Alias("elenenhelp")]
        [Summary("Help for the Elenen commands.")]
        public async Task ElenenHelp()
        {
            await ReplyAsync("This help menu: `!eh` or `!elenenhelp`\n" +
                "Hex traveling information: `!travel <distance in feet>`\n" +
                "Crafting Information: `!ci` or `!craftinfo`\n" + 
                "Crafting times: `!c <tier> <# of crafters> [initial modifier]` or `!craft`");
        }

        [Command("c"), Alias("craft")]
        [Summary("Get crafting times.")]
        public async Task CraftingTime(string tier = null, string craftersInput = null, [Remainder] string modifierInput = null)
        {
            if (String.IsNullOrEmpty(tier) || String.IsNullOrEmpty(craftersInput))
            {
                await ReplyAsync("Usage: `!c <tier> <# of crafters> [initial modifier]`\nEx: `!c veryrare 5` or `!c legendary 10 4`");
                return;
            }
            if (!int.TryParse(craftersInput, out int crafters)) { await ReplyAsync("`Crafters must be a number.`"); }
            if (!int.TryParse(modifierInput, out int modifier) && modifierInput != null) { await ReplyAsync("`Modifier must be a number.`"); }
            string tierDesc = "";
            float weekTime = 0, hourTime = 0, dayTime = 0;

            switch (tier.ToLower())
            {
                case "common":
                    weekTime = 1;
                    dayTime = 7;
                    hourTime = 168;
                    tierDesc = "Common";
                    break;
                case "uncommon":
                    weekTime = 2;
                    dayTime = 14;
                    hourTime = 336;
                    tierDesc = "Uncommon";
                    break;
                case "rare":
                    weekTime = 10;
                    dayTime = 70;
                    hourTime = 1680;
                    tierDesc = "Rare";
                    break;
                case "veryrare":
                    weekTime = 25;
                    dayTime = 175;
                    hourTime = 4200;
                    tierDesc = "Very Rare";
                    break;
                case "legendary":
                    weekTime = 50;
                    dayTime = 350;
                    hourTime = 8400;
                    tierDesc = "Legendary";
                    break;
                default:
                    await ReplyAsync("Tier must be: `common, uncommon, rare, veryrare, legendary`");
                    return;
            }
            //If initial modifier exists (primary crafter cuts time by extra amount) then cut the time
            if (modifier > 0) { weekTime /= modifier; hourTime /= modifier; dayTime /= modifier; }
            else { modifier = 0; }
            //For each crafter cut the time in half [deprecated]
            //for (int i = 0; i < crafters; i++) { weekTime /= 2; hourTime /= 2; }
            //Divide crafting time by the amount of crafters
            weekTime /= crafters;
            hourTime /= crafters;
            dayTime /= crafters;
            var builder = new EmbedBuilder()
                .WithDescription("Base Crafting Time Reduction: " + modifier)
                .AddField("Rarity:", tierDesc, inline: true)
                .AddField("Crafters", crafters + " Standard", inline: true)
                .AddField("\u200B", "\u200B", inline: true)
                .AddField("Weeks:", weekTime.ToString("0.00"), inline: true)
                .AddField("Days:", dayTime.ToString("0.00"), inline: true)
                .AddField("Hours:", hourTime.ToString("0.00"), inline: true)
                .WithAuthor("Crafting Calculation") 
                .WithColor(new Color(0x344294))
                .WithFooter("Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username);
            var embed = builder.Build();
            await ReplyAsync(embed: embed)
                .ConfigureAwait(false);
        }

        [Command("ci"), Alias("craftinfo")]
        [Summary("Get crafting info/times.")]
        public Task CraftingInfo()
        {
            var builder = new EmbedBuilder()
                .WithDescription("Crafting Information")
                .WithColor(new Color(0x344294))
                .AddField("Crafting Times[Base]", "Common = 1 week\nUncommon = 2 weeks\nRare = 10 weeks\nVery Rare = 25 weeks\nLegendary = 50 weeks\n", inline: true)
                .AddField("Crafting Times[10 Followers]", "Common = .16 hours\nUncommon = .33 hours\nRare = 1.64 hours\nVery rare = 4.1 hours\nLegendary = 8.2 hours", inline: true)
                .AddField("\u200B", "\u200B", inline: true)
                .AddField("Material Enchantability", "CR 1 to 3: common\nCR 4 to 8: Uncommon\nCR 9 to 12: Rare\nCR 13 to 18: Very Rare\nCR 19 +: Legendary)", inline: true)
                .AddField("Spell Enchant Levels", "0 = Common\n1 - 2 = Uncommon\n3 - 5 = Rare\n6 - 8 = Very Rare\n9 = Legendary", inline: true)
                .AddField("\u200B", "\u200B", inline: true)
                .WithFooter("Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username);
            var embed = builder.Build();
            _interactivity.DelayedSendMessageAndDeleteAsync(Context.Channel, TimeSpan.FromMilliseconds(0), TimeSpan.FromMinutes(2), embed: embed);
            return null;
            //await ReplyAsync(embed: embed).ConfigureAwait(false);
            //var Items = await Context.Channel.GetMessagesAsync().FlattenAsync();
            //var usermessages = Items.Where(x => x.Author == Context.Guild.CurrentUser).Take(1);
            //var delmsg = usermessages.First();
            //await Context.Channel.DeleteMessageAsync(delmsg);*//*
        }/*

        [Command("travel")]
        [Summary("Find travel distance in hexes.")]
        public async Task TravelInfo(string input = null)
        {
            //Fool! You forgot the input!
            if (String.IsNullOrEmpty(input))
            {
                await ReplyAsync("`Requires movement speed input. Ex: !travel 30`");
                return;
            }
            //Basic info for the game, displayed for viewing pleasure
            string distInfo = "1 Hex = 6 miles wide\nMovement/10 = speed in MPH";

            //convert the string to a double to make sure its a NUMBAH
            if (!Double.TryParse(input, out double dist))
            {
                await ReplyAsync("`Requires numeric input. Ex: !travel 30`");
                return;
            }
            //Get the distance in 'mph'
            double mph = dist/10;

            //do the hex calc
            //mph >= 6 means you can traverse one hex per hour
            string display = $"{mph} MPH = ";
            if(mph >= 6)
            {
                dist = mph/6;
                display += $"{dist.ToString("0.00")} hex(es) per hour.";
            }
            else
            {
                dist = 6/mph;
                display += $"{dist.ToString("0.00")} hour(s) per hex.";
            }

            var builder = new EmbedBuilder()
                .WithDescription(distInfo)
                .WithAuthor("Distance Information")
                .WithColor(new Color(0x344294))
                .AddField("Conversion", display)
                .WithFooter("Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username);
            var embed = builder.Build();
            await ReplyAsync(embed: embed)
                .ConfigureAwait(false);
        }
    }
}
*/