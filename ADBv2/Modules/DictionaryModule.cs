﻿using System;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Discord;
using Discord.Commands;
using Interactivity;
using ADBv2.Services;
using ADBv2.Data;

namespace ADBv2.Modules
{
    public class DictionaryModule : ModuleBase<SocketCommandContext>
    {
        [Command("dict")]
        [Summary("Find the definition of a word.")]
        public async Task DictCall([Remainder]string input)
        {
            if (input == null) { await ReplyAsync("`Requires word to define.`"); return; }

            DictResult definitions = new DictResult();
            try
            {
                var findWord = definitions.GetWord(input).Result.Results;
                input = findWord[0].LexicalEntries[0].Text;
            }
            catch { await ReplyAsync("`Error returned from web request.`"); return; }
            
            /*foreach (var test in findWord)
            {
                foreach (var test2 in test.LexicalEntries)
                {
                    var test3 = test2.Entries[0].ToString();
                    foreach (var test4 in test3.Senses)
                    {
                        input = test4.Definitions[0];
                        break;
                    }
                    break;
                }
                break;
            }*/
            var defResults = definitions.GetDefinitions(input).Result.Results;

            if (defResults.Count == 0) { await ReplyAsync("`No results for selected word.`"); return; }

            List<string> wordDef = new List<string>();
            List<string> wordType = new List<string>();
            List<int> numDef = new List<int>();

            foreach (var entry in defResults)
            {
                foreach (var test in entry.LexicalEntries)
                {
                    int i = 0;
                    wordType.Add(test.LexicalCategory.Text);
                    foreach (var test2 in test.Entries)
                    {
                        foreach (var test3 in test2.Senses)
                        {
                            if (test3.Definitions != null)
                            {
                                foreach (var test4 in test3.Definitions)
                                {
                                    if (test4 != null)
                                    {
                                        wordDef.Add(test4);
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                    numDef.Add(i);
                }
            }
            var builder = new EmbedBuilder()
                    .WithTitle("Definitions for: " + input)
                    .WithColor(new Color(0xC16BF2))
                    .WithThumbnailUrl("https://i.imgur.com/rQ8aIRW.png")
                    .WithAuthor(author => {
                        author
                            .WithName("Oxford Dictionary")
                            .WithUrl("https://en.oxforddictionaries.com/");
                        //.WithIconUrl("https://i.imgur.com/71KJUzy.png");
                    });
            if (wordType.ElementAtOrDefault(0) != null)
            {
                string totalDef = "";
                for (int i = 0; i < numDef[0]; i++)
                {
                    totalDef += wordDef[i] + "\n";
                }
                builder.AddField(wordType[0], totalDef);
            }
            if (wordType.ElementAtOrDefault(1) != null)
            {
                string totalDef = "";
                for (int i = 0; i < numDef[1]; i++)
                {
                    totalDef += wordDef[i+numDef[0]] + "\n";
                }
                builder.AddField(wordType[1], totalDef);
            }
            if (wordType.ElementAtOrDefault(2) != null)
            {
                string totalDef = "";
                for (int i = 0; i < numDef[2]; i++)
                {
                    totalDef += wordDef[i+numDef[0]+numDef[1]] + "\n";
                }
                builder.AddField(wordType[2], totalDef);
            }
            if (wordType.ElementAtOrDefault(3) != null)
            {
                string totalDef = "";
                for (int i = 0; i < numDef[3]; i++)
                {
                    totalDef += wordDef[i + numDef[0] + numDef[1] + numDef[2]] + "\n";
                }
                builder.AddField(wordType[3], totalDef);
            }
            if (wordType.ElementAtOrDefault(4) != null)
            {
                string totalDef = "";
                for (int i = 0; i < numDef[4]; i++)
                {
                    totalDef += wordDef[i + numDef[0] + numDef[1] + numDef[2] + numDef[3]] + "\n";
                }
                builder.AddField(wordType[4], totalDef);
            }
            var footer = new EmbedFooterBuilder();
            footer.Text = "Result for " + (Context.User as IGuildUser)?.Nickname ?? Context.User.Username + " | Powered by Oxford Dictionary";
            if (footer.Text == "Result for ") { footer.Text = "Result for " + ((IGuildUser)Context.Message.Author).Username + " | Powered by Oxford Dictionary"; }
            builder.Footer = footer;
            var embed = builder.Build();
            await ReplyAsync("", false, embed)
                .ConfigureAwait(false);
        }
    }
}
