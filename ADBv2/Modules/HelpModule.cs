﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Discord.Commands;
using Interactivity;
using ADBv2.Services;
using Interactivity.Pagination;

namespace ADBv2.Modules
{
    public class HelpModule : ModuleBase<SocketCommandContext>
    {

        private readonly InteractivityService _interactivity;
        public HelpModule(InteractivityService interactivity)
        {
            _interactivity = interactivity;
        }

        [Command("help")]
        [Summary("Basic help commands.")]
        public async Task Help()
        {
            var builder = new EmbedBuilder()
                        .WithTitle("Commands: ")
                        .WithDescription("<> are required items | [] are optional items.")
                        .WithColor(new Color(0xC16BF2))
                        .AddField("Dice Systems", "Ways to roll dice. See !help roll")
                        .AddField("!rs | !rollstats <3d6, 4d6, d20>", "Rolls base stats. ***Examples***: !rs d20 | !rollstats 3d6\n4d6 takes the lowest number and discards.")
                        //.AddField("!ref", "Pulls up reference sheet options (cheat sheets) for tabletop games.")
                        //.AddField("!report", "Reports the statistics of D20 rolls since last wipe. [Deprecated, no longer adding to log.]")
                        //.AddField("!reportwipe", "Resets the report back to 0. Admin only.")
                        .AddField("!w <location> | !weather <location>", "Displays weather information for the specified location. Location can be a name or zip code.")
                        .AddField("!time", "Displays current time in US zones.")
                        .AddField("!timeintl", "Displays current time in international cities.")
                        //.AddField("!timer <#><h/m> [here/everyone/self] | !alarm", "Sets an alarm that pings @here or @everyone.\n***Examples***: !timer 2h everyone | !alarm 5m here\nDefaults to self if unspecified or unrecognized.")
                        .AddField("!flip [heads/tals]", "Flips a coin. Heads or tails. ***Examples***: !flip | !flip heads")
                        .AddField("!ud | !dict", "Gets the top three definitions from Urban Dictionary.\nGets definitions from the Oxford Dictionary.")
                        //.AddField("!boop | !flop | !hug", ":smile:")
                        .AddField("!google | !ddg", "Do a Google/DuckDuckGo search.")
                        //.AddField("!purge <#> [user]", "Deletes the last <#> of messages by [user]. Default is self.\nDeleting another users messages needs requisite permissions.")
                        //.AddField("!purgeb <#> | !purgea <#>", "b - Deletes the last <#> of messages by the bot.\nRequires Guild or Bot ownership.\na - Deletes the last <#> of messages in the channel.\nRequires Guild ownership.")
                        .AddField("!invite", "Provides a link to invite Ievora to a server you are an admin of.")
                        .AddField("Extra help menus:", "!help roll | !help time | !help sr | !help adv");
                        //.WithFooter("This message will auto-delete after 2 minutes.");
            // uncomment the next 3 lines and comment the line under them to re-enable auto-deletion.
            //if (input.Contains("perm")) { builder.WithFooter("Message set to not delete."); }
            var embed = builder.Build();
            //if (input.Contains("perm")) { await ReplyAsync("", false, embed).ConfigureAwait(false); }
            //else { await ReplyAndDeleteAsync("", false, embed, TimeSpan.FromMinutes(2)).ConfigureAwait(false); }
            await ReplyAsync("", false, embed).ConfigureAwait(false);
            var Items = await Context.Channel.GetMessagesAsync().FlattenAsync();
            var usermessages = Items.Where(x => x.Author == Context.Message.Author as SocketGuildUser).Take(1);
            var delmsg = usermessages.First();
            await Context.Channel.DeleteMessageAsync(delmsg);
        }

        [Command("help adv")]
        [Summary("Advanced help commands for higher privledges.")]
        public Task HelpAdv()
        {
            var builder = new EmbedBuilder()
                        .WithTitle("Higher Privledge Commands: ")
                        .WithDescription("<> are required items | [] are optional items.")
                        .WithColor(new Color(0xC16BF2))                        
                        .AddField("!purge <#> [user]", "Deletes the last <#> of messages by [user]. Default is self.\nDeleting another users messages needs requisite permissions.")
                        .AddField("!purgeb <#> | !purgea <#>", "b - Deletes the last <#> of messages by the bot.\nRequires Guild or Bot ownership.\na - Deletes the last <#> of messages in the channel.\nRequires Guild ownership.")
                        .AddField("!invite", "Provides a link to invite Ievora to a server you are an admin of.");
            //.WithFooter("This message will auto-delete after 2 minutes.");
            // uncomment the next 3 lines and comment the line under them to re-enable auto-deletion.
            //if (input.Contains("perm")) { builder.WithFooter("Message set to not delete."); }
            var embed = builder.Build();
            _interactivity.DelayedSendMessageAndDeleteAsync(Context.Channel, embed: embed, deleteDelay: TimeSpan.FromMinutes(2));
            return null;
        }

        [Command("help roll")]
        [Summary("Help for roll command.")]
        public Task HelpRoll()
        {
            var pages = new PageBuilder[] {
                new PageBuilder().WithTitle("**!roll | !r <Dice Amount>D<Dice sides>**")
                    .WithDescription("Rolls dice.\n\n***Examples***: !roll 2D6 | !roll 1D10\nCan also use !r. Ex: !r 2d10 | !r 5d10-3+6d5"),
                new PageBuilder().WithTitle("**!sr <Dice Amount> [Modifiers]**")
                    .WithDescription("Shadowrun guidelines dice.\n\n***Examples***: !sr 20 | !sr 8 x 2\n***Modifiers***: [x/edge/ptl], [vs/v], [l], [sc]\n**[x [#] , edge [#] , ptl [#]]** - Use edge for Push the Limit. " +
                        "Rolling a 6 adds a extra dice. Defaults to 1 edge dice. This is usable by itself by calling \"!sr <ptl/x/edge> <#>\"\n**[vs] , [v]** - Versus roll. Accepts edge use. Use to roll two sets of dice seperately." +
                        "\n**[l]** - Specify a limit.\n**[sc]** - Second Chance. Prompts if you want to expend an edge dice to reroll all non-hit rolls. Automatically carries over the glitches from the first roll." +
                        "**More Info** - Extended test info available at !help sr"),
                new PageBuilder().WithTitle("**!wod <Dice Amount> [Modifiers]**")
                    .WithDescription("World of Darkness guidlines dice. Defaults to 6 for difficulty.\n\n***Examples***: !wod 10 | !wod 8 4 | !wod 3 x 4" +
                "\n***Modifiers***: [x], [spec], [#]\n[x] - Exploding Tens. Rolling a 10 adds a free extra dice.\n[spec] - Specialization roll. Tens are worth two successes." +
                "\n[#] - Difficulty. Optional. Any number past the first is taken for DC."),
                new PageBuilder().WithTitle("**!ep <Target> [Modifiers] | !ep m <Dice Amount>**")
                    .WithDescription("Eclipse Phase guidelines dice. Two options, one rolls against a target level, the other rolls multidice." +
                "\n\n***Examples***: !ep m 5 | !ep 45 | !ep 34 -10 | !ep 56 simple\n***Modifiers***: [m], [+/-##], [difficulty], [severity]\n[m] - Enables multidice rolls. Must come directly after !ep. " +
                "Accepts no other modifiers.\n[+/-##] - Adds/Subtracts from the target number.\n[difficulty] - Options are: effortless(+30), simple(+20), easy(+10), difficult(-10), challenging(-20), hard(-30)." +
                "\n[severity] - Options are: minor(+/-10), moderate(+/-20), major(+/-30)")};
            var paginator = new StaticPaginatorBuilder()
                .WithUsers(Context.Message.Author)
                .WithPages(pages)
                .WithFooter(PaginatorFooter.PageNumber | PaginatorFooter.Users)
                .WithDefaultEmotes()
                .Build();
            return _interactivity.SendPaginatorAsync(paginator, Context.Channel, TimeSpan.FromMinutes(2));
        }

        [Command("help sr")]
        [Summary("Help for shadowrun command.")]
        public async Task HelpSrRoll(IMessageChannel info = null)
        {
            var builder = new EmbedBuilder()
                        .WithTitle("Special Shadowrun Commands: ")
                        .WithDescription("<> are required items | [] are optional items.")
                        .WithColor(new Color(0xC16BF2))
                        .AddField("Extended Test - !sr <#> et <keyword>", "Creates/Modifies an extended test roll. Rolled exactly like normal dice with ET and a keyword on the end.\nEx: !sr 3 et sprite | !sr 5 x et gunbuild")
                        .AddField("Extended Test Report - !sr report [keyword]", "Displays roll status of chosen keyword. If blank, lists all available keywords.\nEx: !sr report | !sr report sprite")
                        .AddField("Extended Test Delete - !sr del [keyword]", "Deletes extended test information. If blank, lists all available keywords. \nEx: !sr del | !sr del gunbuild")
                        .WithFooter("");
            var embed = builder.Build();
            if (info == null) { await ReplyAsync(embed: embed).ConfigureAwait(false); }
            else { await info.SendMessageAsync(embed: embed).ConfigureAwait(false); }
            //var Items = await Context.Channel.GetMessagesAsync().FlattenAsync();
            //var usermessages = Items.Where(x => x.Author == Context.Message.Author as SocketGuildUser).Take(1);
            //var delmsg = usermessages.First();
            //await Context.Channel.DeleteMessageAsync(delmsg);
        }

        [Command("help time")]
        [Summary("Help for time command. Works for both time and timeintl.")]
        public async Task HelpTime()
        {
            var builder = new EmbedBuilder()
                .WithTitle("Time Commands: ")
                .WithDescription("<> are required items | [] are optional items.")
                .WithColor(new Color(0xC16BF2))
                .AddField("!time", "Displays the current time in US timezones.")
                .AddField("!time <hour>[:minute] <pm/am> <timezone>", "Displays the requested time in US timezones.\n***Examples***: !time 2 pm pst | !time 3:45 am cst\nValid timezones: hst, akst, pst, mst, cst, est.")
                .AddField("!timeintl", "Displays the current time in major cities around the globe.")
                .AddField("!timeintl <hour>[:minute] <pm/am> <timezone>", "Displays the requested time in (certain) global timezones. This is not yet implemented.")
                .AddField("!timer <#><h/m> [here/everyone/self] | !alarm", "Sets an alarm that pings @here or @everyone.\n***Examples***: !timer 2h everyone | !alarm 5m here\nDefaults to here if unspecified or unrecognized.")
                .WithFooter("");
            var embed = builder.Build();
            await ReplyAsync("", false, embed).ConfigureAwait(false);
            var Items = await Context.Channel.GetMessagesAsync().FlattenAsync();
            var usermessages = Items.Where(x => x.Author == Context.Message.Author as SocketGuildUser).Take(1);
            var delmsg = usermessages.First();
            await Context.Channel.DeleteMessageAsync(delmsg);
        }

        [Command("help fortune")]
        [Summary("Help for the fortune command.")]
        public async Task HelpFortune()
        {
            /*
            var builder = new EmbedBuilder()
                .WithTitle("Fortune Options: ")
                //.WithDescription("<> are required items | [] are optional items.")
                .WithColor(new Color(0xC16BF2))
                .AddField("Must be fully typed out:", "all, bible, computers, cookie, definitions, misc, people, platitudes, politics, science, wisdom")
                .WithFooter("");
            var embed = builder.Build();
            await ReplyAsync("", false, embed).ConfigureAwait(false);
            var Items = await Context.Channel.GetMessagesAsync().FlattenAsync();
            var usermessages = Items.Where(x => x.Author == Context.Message.Author as SocketGuildUser).Take(1);
            var delmsg = usermessages.First();
            await Context.Channel.DeleteMessageAsync(delmsg);*/
            await ReplyAsync("Module is disabled.");
        }

        [Command("fullhelp")]
        [Summary("Every command option. Authorized users only.")]
        public async Task FullHelp()
        {
            ConfigService cred = new ConfigService();
            string user = Context.User.ToString();
            if (user == cred.BotOwner)
            {
                await ReplyAsync(
                "`Authorized User.`\n" +
                "```Full Command List:```" +
                "`!roll, !r, !sr, !wod, !ep\n" +
                "!sr # [mods] et <name> | !sr report <name> !sr del <name>\n" +
                "!rs, !rollstats\n" +
                //"!report, !reportwipe\n" +
                "!flip, !flip heads/tails\n" +
                "!ud\n" +
                "!time, !timeintl\n" +
                "!boop, !boop @user, !boop user\n" +
                "!timer\n" +
                "!help, !fullhelp, !help roll, !help time\n" +
                "!invite\n" +
                //"!ping\n" +
                //"!play, !join\n" +
                "!debug\n" +
                "!scree`"
                );
                var Items = await Context.Channel.GetMessagesAsync().FlattenAsync();
                var usermessages = Items.Where(x => x.Author == Context.Message.Author as SocketGuildUser).Take(1);
                var delmsg = usermessages.First();
                await Context.Channel.DeleteMessageAsync(delmsg);
            }
            else
            {
                await ReplyAsync("`Must be bot owner.`");
            }
        }

        [Command("invite")]
        [Summary("Display invite code for the bot to add to guilds.")]
        public async Task InviteMe()
        {
            var builder = new EmbedBuilder()
                .WithTitle("Invite Me!")
                .WithColor(new Color(0xC16BF2))
                .WithDescription("Use this code to invite Ievora to a server you are an admin of!\n" +
                "Uncheck whatever permissions you want, but that might break some functionality.\n" +
                "[Invite Link!](https://discord.com/oauth2/authorize?client_id=269953683217842178&scope=bot&permissions=68165391875825)");
            var embed = builder.Build();
            await ReplyAsync("", false, embed)
                .ConfigureAwait(false);
            var Items = await Context.Channel.GetMessagesAsync().FlattenAsync();
            var usermessages = Items.Where(x => x.Author == Context.Message.Author as SocketGuildUser).Take(1);
            var delmsg = usermessages.First();
            await Context.Channel.DeleteMessageAsync(delmsg);
        }
    }
}
